package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMotionWordSoftAssignment;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.CategoryTableXYDataset;
import org.jfree.data.xy.DefaultXYDataset;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.experiments.KnnPrecision;
import xyz.matj1.bp.experiments.PairwiseCorrelation;
import xyz.matj1.bp.themes.SmoothXyGraphTheme;
import xyz.matj1.bp.themes.SuperimpositionXYGraphTheme;
import xyz.matj1.bp.types.CategoryPrecisions;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.Pair;
import xyz.matj1.bp.types.PointSet;
import xyz.matj1.bp.types.Vector2;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.DoubleStream;

import static xyz.matj1.bp.Utils.loadData;
import static xyz.matj1.bp.Utils.readCache;
import static xyz.matj1.bp.enums.LengthRelation.QUOTIENT;

/**
 * A class for the testing of how sequences of
 * motion words of similar motions of different lengths can be compared
 *
 * @author Matěj Bagar
 */
public class LengthTesting {
	public static void main(String[] args) throws IOException{
		ObjectMotionWordSoftAssignment.maxPartsToMatch = 6;

		var outputDir = new File(Config.outputDir(), "length testing");

		pairwiseTests(outputDir);
		pairwiseTestsSmoothed(new File(outputDir, "smoothed quotient pairwise graphs"));
		pairwiseTestsSmoothedWithQuantity(new File(outputDir, "smoothed quotient pairwise graphs with quantity"));
		pairwiseTestsSuperimposed(new File(outputDir, "superimposed quotient pairwise graphs"));
		knnTests(outputDir);
		knnTestsAveraged(new File(outputDir, "kNN averages/kNN averages.txt"));
		knnTestsAveragedToGraphs(new File(outputDir, "knn averages graphs"));
		knnTestsAveragedOrderedToGraphs(new File(outputDir, "knn averages ordered graphs"));
		knnTestsMocapTypePrecisionAverages();
		knnCompareMocapTypes(new File(Config.outputDir(), "kNN comparison of mocap types.pdf"));
	}

	/**
	 * Pairwise test for each category to graphs in a dedicated directory for each mocap type
	 *
	 * @param outputPath where the dedicated directories shall be written
	 */
	private static void pairwiseTests(File outputPath) throws IOException {
		ShowGraph showQuotientGraph = (dataPath, dataClass, outputDirPath) -> {
			var timer = Instant.now();
			PairwiseCorrelation.show(
				loadData(dataPath, dataClass),
				QUOTIENT,
				outputPath
			);
			System.err.printf(
				"Time to compute pairwise correlations for data class %s: %d s%n",
				dataClass.getSimpleName(),
				Duration.between(timer, Instant.now()).toSeconds()
			);
		};

		for (var type : MocapType.values()) {
			showQuotientGraph.show(
				dataFiles(type),
				type.dataClass(),
				new File(outputPath, type.adjective() + " quotient pairwise graphs")
			);
	}}

	/**
	 * For each mocap type, pairwise tests for each category
	 * with all data put into a single dataset and smoothed
	 *
	 * @param outputPath where the graphs shall be written
	 * @throws IOException if the reading or the writing fails
	 */
	private static void pairwiseTestsSmoothed(File outputPath) throws IOException {
		for (var type : MocapType.values()) {
			Utils.writeXYGraphAsPdf(
				PairwiseCorrelation.showCategory(
					PairwiseCorrelation.smooth(
						PairwiseCorrelation.compute(
							loadData(dataFiles(type), type.dataClass()),
							QUOTIENT
						),
						0.01d,
						.5d
					),
					QUOTIENT,
					Optional.empty()
				),
				new File(outputPath, type.noun() + ".pdf"),
				new Vector2<>(512, 512),
				SmoothXyGraphTheme.instance()
			);
	}}

	/**
	 * Like smoothed pairwise graphs, but with an other data series showing how many series is in the x-area of the graph (relatively)
	 *
	 * @param outputPath where the graphs shall be written
	 * @throws IOException if the reading or the writing fails
	 */
	public static void pairwiseTestsSmoothedWithQuantity(File outputPath) throws IOException {
		// Both data series have the same color.
		// This needs relabeling the y axis.

		for (var type : MocapType.values()) {
			var pairwiseCorrelation = PairwiseCorrelation.compute(
				loadData(dataFiles(type), type.dataClass()),
				QUOTIENT
			);

			var smoothCorrelation = PairwiseCorrelation.smooth(pairwiseCorrelation, 0.01, 0.5);
			var quantity = PairwiseCorrelation.quantity(pairwiseCorrelation, 0.01, 0.5);

			var quantityMaximum = quantity.stream().mapToDouble(Vector2::b).filter(Double::isFinite).max().orElseThrow();
			var sCMaximum = smoothCorrelation.stream().mapToDouble(Vector2::b).filter(Double::isFinite).max().orElseThrow();

			var quantityDataset = new DefaultXYDataset();

			// add quantity to the dataset
			double[] xQArray = quantity.stream().mapToDouble(Vector2::a).toArray();
			double[] yQArray = quantity.stream().mapToDouble(p -> p.b() * sCMaximum/quantityMaximum).toArray();
//			double[] yQArray = quantity.stream().mapToDouble(p -> p.b() / 1000).toArray();
			quantityDataset.addSeries(0, new double[][]{xQArray, yQArray});

			var chart = PairwiseCorrelation.showCategory(smoothCorrelation, QUOTIENT, Optional.empty());
			chart.getXYPlot().setDataset(1, quantityDataset);

			var theme = new ChartTheme() {
				@Override public void apply(JFreeChart chart) {
					SmoothXyGraphTheme.instance().apply(chart);
					var renderer = chart.getXYPlot().getRenderer();
					renderer.setSeriesPaint(1, Color.BLUE);
					renderer.setSeriesPaint(0, Color.GREEN);
			}};

			Utils.writeXYGraphAsPdf(chart, new File(outputPath, type.noun() + ".pdf"), new Vector2<>(512, 512), theme);
	}}

	/**
	 * Pairwise tests superimposed to one graph and written to a file for each mocap type
	 *
	 * @param outputPath where the graphs shall be written
	 * @throws IOException if the reading or the writing fails
	 */
	private static void pairwiseTestsSuperimposed(File outputPath) throws IOException {
		for (var type : MocapType.values()) {
			var data = loadData(dataFiles(type), type.dataClass());
			var correlationData = PairwiseCorrelation.compute(data, QUOTIENT);

			var charts = data.getCategories().parallelStream().map(
				category -> PairwiseCorrelation.showCategory(correlationData.get(category), QUOTIENT, Optional.of(category))
			).toList();

			var superimposed = Utils.superimpose(charts);

			Utils.writeXYGraphAsPdf(
				superimposed,
				new File(outputPath, type.noun() + ".pdf"),
				new Vector2<>(512, 512),
				SuperimpositionXYGraphTheme.instance()
			);
	}}

	/**
	 * kNN tests with a graph for each category in a directory dedicated for each type
	 *
	 * @param outputPath where the dedicated directories shall be
	 * @throws IOException if the reading or the writing fails
	 */
	private static void knnTests(File outputPath) throws IOException {
		outputPath.mkdirs();

		ShowGraph showKnnLengthGraph = (dataPath, dataClass, outputDirPath) -> {
			var timer = Instant.now();
			KnnPrecision.show(
				loadData(dataPath, dataClass),
				outputDirPath
			);
			System.err.printf(
				"Time to compute KNN for data class %s: %d s%n",
				dataClass.getSimpleName(),
				Duration.between(timer, Instant.now()).toSeconds()
			);
		};

		for (var type : MocapType.values()) {
			showKnnLengthGraph.show(
				dataFiles(type),
				type.dataClass(),
				new File(outputPath, type.adjective() + " kNN graphs")
			);
	}}

	/**
	 * kNN tests averaged and to a file for each mocap type
	 *
	 * @param outputPath where the result shall be written
	 * @throws IOException if the reading or the writing fails
	 */
	private static void knnTestsAveraged(File outputPath) throws IOException {
		var output = new PrintWriter(outputPath);

		for (var type : MocapType.values()) {
			output.printf("%n%s:%n", type.noun());
			Utils.write(
				KnnPrecision.averageFormat(
					KnnPrecision.average(KnnPrecision.compute(
						loadData(dataFiles(type), type.dataClass())
				))),
				output
			);
	}}

	/**
	 * kNN tests averaged for each category, to a data series for each mocap type and to a graph
	 *
	 * @param outputPath where the result shall be written
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestsAveragedToGraphs(File outputPath) throws IOException {
		var typeAverages = new HashMap<MocapType, Map<Category, Double>>();
		for (var type : MocapType.values()) {
			var averages = KnnPrecision.average(KnnPrecision.compute(
				loadData(dataFiles(type), type.dataClass())
			));

			typeAverages.put(type, averages);
		}

		var typeDataSeries = MyMap.of(typeAverages).mapValues(
			averages -> {
				var averagesDoubles = MyMap.of(averages).map(
					key -> Double.parseDouble(key.id),
					Function.identity()
				);

				var pointSet = new PointSet(
					averagesDoubles.entrySet().stream().map(Vector2::fromEntry).toList()
				); // for sorting the values

				return pointSet.toDataSeries();
		});

		var dataset = new DefaultXYDataset();
		typeDataSeries.forEach(dataset::addSeries);

		var chart = ChartFactory.createScatterPlot(
			"average precision for each category for each type",
			"category",
			"average precision",
			dataset,
			PlotOrientation.VERTICAL,
			true,
			false,
			false
		);

		Utils.writeXYGraphAsPdf(
			chart,
			new File(outputPath, "knn averages.pdf"),
			new Vector2<>(1024, 512),
			SmoothXyGraphTheme.instance()
		);
	}

	/**
	 * kNN tests averaged for each category, sorted and to a data series for each mocap type, and to a graph
	 *
	 * @param outputPath where the result shall be written
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestsAveragedOrderedToGraphs(File outputPath) throws IOException {
		var serialisedData = new File(Config.cacheDir(), "knnTestsAveragedOrderedToGraphs");

		// initialise type_Averages (load from cache or compute)
		var type_Averages = getType_Averages(serialisedData);

		// a collection of each [category with the [precisions for each mocap type]]
		// sorted by the precision for SKELETON mocap type
		var categories_Precisions = new TreeSet<>(
			Comparator.comparing(CategoryPrecisions::averagePrecision)
//			Comparator.comparing(o -> o.precision(MocapType.SKELETON))
		);
		for (var category : type_Averages.get(MocapType.SKELETON).keySet()) {
			categories_Precisions.add(
				new CategoryPrecisions(
					category,
					MyMap.of(type_Averages).mapValues(m -> m.get(category))
			));
		}

		var dataset = new CategoryTableXYDataset();
		var naturals = DoubleStream.iterate(0, i -> i+1).iterator();
		for (var item : categories_Precisions) {
			var xCoordinate = naturals.nextDouble();
			for (var type : MocapType.values()) {
				dataset.add(
					xCoordinate,
					item.precision(type),
					type.noun()
				);
		}}

		var chart = ChartFactory.createScatterPlot(
			"average precision for each category for each type, with points sorted by their height",
			"",
			"average precision",
			dataset,
			PlotOrientation.VERTICAL,
			true,
			false,
			false
		);

		Utils.writeXYGraphAsPdf(
			chart,
			new File(outputPath, "knn averages.pdf"),
			new Vector2<>(1024, 512),
			SmoothXyGraphTheme.instance()
		);
	}

	/**
	 * This gets a map from each mocap type to the average kNN precisions for each category.
	 * This either reads it from cache, or this computes it and tries to write it to the cache.
	 * The categories are represented by their <code>id</code>s parsed to integers.
	 *
	 * The cache is supposed to be in <code>serialisedData</code>.
	 *
	 * @param serialisedData supposed location of cache for this computation
	 * @return a map from each mocap type to the average kNN precisions for each category
	 * @throws IOException if loading input data for the computation fails
	 */
	private static Map<MocapType, Map<Integer, Double>> getType_Averages(File serialisedData) throws IOException {
		Map<MocapType, Map<Integer, Double>> type_Averages;
		try {
			type_Averages = readCache(serialisedData, Map.class);
		} catch (CacheIOException _e) {
			type_Averages = new HashMap<>();
			for (var type : MocapType.values()) {
				var averages = KnnPrecision.average(KnnPrecision.compute(
					loadData(dataFiles(type), type.dataClass())
				))
					.filter((category, precision) -> Double.isFinite(precision))
					.map(
						category -> Integer.parseInt(category.id),
						Function.identity()
					);

				type_Averages.put(type, averages);
			}
			for (var entry : type_Averages.entrySet()) { // check that all mocap types have the same categories
				assert entry.getValue().keySet().equals(type_Averages.get(MocapType.SKELETON).keySet());
			}

			try {
				Utils.writeCache(serialisedData, type_Averages);
			} catch (CacheIOException e) {
				e.printStackTrace();
			}
		}
		return type_Averages;
	}

	/**
	 * For each mocap type, this prints the average precision of all kNN searches with the mocap type.
	 */
	private static void knnTestsMocapTypePrecisionAverages() throws IOException {
		var knnMeasurements = getKnnMeasurements();

		knnMeasurements.forEach((type, value) -> {
			var precisions = value.entrySet().stream().collect(
				ArrayList<Double>::new,
				(accumulator, entry2) -> accumulator.addAll(entry2.getValue().stream().map(Pair::b).toList()),
				ArrayList::addAll
			);

			var averagePrecision = precisions.stream().mapToDouble(x -> x).filter(Double::isFinite).average().orElseThrow();

			System.out.printf("%s:%n", type.noun());
			System.out.println(averagePrecision);
		});
	}

	private static void knnCompareMocapTypes(File outputFile) throws IOException {
		class Category_Type_Precision extends MyMap<Category, EnumMap<MocapType, Double>> {
			public Category_Type_Precision putPrecision(Category category, MocapType type, Double precision) {
				super.putIfAbsent(category, new EnumMap<>(MocapType.class));
				super.get(category).put(type, precision);
				return this;
			}

			public Double getPrecision(Category category, MocapType type) {
				return super.get(category).get(type);
			}
		}

		var knnPrecisions = StretchedSequences.getKnnMeasurements();
		var originalPrecisions = knnPrecisions.mapValues(v -> v.get(SpeedMultiple._1));
		var category_Type_Precision = new Category_Type_Precision();
		for (var entry : originalPrecisions.entrySet()) {
			var type = entry.getKey();
			var category_Precisions = entry.getValue();
			for (var entry2 : category_Precisions.entrySet()) {
				var category = entry2.getKey();
				var averagePrecision = entry2.getValue().stream().mapToDouble(x -> x).average().orElseThrow();
				category_Type_Precision.putPrecision(category, type, averagePrecision);
		} }

		var dataset = new DefaultCategoryDataset();
		category_Type_Precision.entrySet().stream()
			.sorted(Comparator.comparing(e -> e.getValue().get(MocapType.SKELETON)))
			.forEach(entry -> {
				var category = entry.getKey();
				var type_Precision = entry.getValue();
				for (var entry2 : type_Precision.entrySet()) {
					var type = entry2.getKey();
					var precision = entry2.getValue();
					dataset.addValue(precision, type, category.id);
				}
			});

		var graph = ChartFactory.createLineChart(
			"",
			"category",
			"precision",
			dataset
		);

		Utils.writeXYGraphAsPdf(
			graph,
			outputFile,
			Config.graphDimensions()
		);
	}

	/**
	 * This retrieves the kNN measurements for each mocap type and puts them to a map.
	 * This tries to read the data from cache
	 * For each mocap type, it calls {@link KnnPrecision#compute(ObjectMgmt)}
	 * given the data relevant for the mocap type.
	 *
	 * @return a map from each mocap type to the kNN measurements for that mocap type
	 * @throws IOException if the data couldn't be read
	 */
	private static MyMap<MocapType, Map<Category, List<Pair<LocalAbstractObject, Double>>>> getKnnMeasurements() throws IOException {
		var serialisedData = new File(Config.cacheDir(), "knnMeasurements") ;

		MyMap<MocapType, Map<Category, List<Pair<LocalAbstractObject, Double>>>> knnMeasurements;

		try {
			knnMeasurements = readCache(serialisedData, MyMap.class);
		} catch (CacheIOException _e) {
			knnMeasurements = new MyMap<>();

			for (var type : MocapType.values()) {
				knnMeasurements.put(
					type,
					KnnPrecision.compute(loadData(dataFiles(type), type.dataClass()))
				);
			}

			try {
				Utils.writeCache(serialisedData, knnMeasurements);
			} catch (CacheIOException e) {
				e.printStackTrace();
		}}

		return knnMeasurements;
	}

	private interface ShowGraph {
		void show(File dataPath, Class<? extends LocalAbstractObject> dataClass, File outputPath) throws IOException;
	}

	public static File dataFiles(MocapType type) {
		return new File(
			Config.dataDir(),
			switch (type) {
				case SKELETON -> "skeleta/class130-actions-coords_normPOS.data";
				case HARD_MW -> "motion words/hdm05-annotations_specific-segment80_shift16-coords_normPOS-fps12-quantized-pivots-kmedoids-350.data";
				case SOFT_MW -> "motion words/kmedoids-350-softassign-D20K6.data";
				case MATCH_N_MW -> "motion words/multi-overlay_motion_words.data";
		});
	}
}
