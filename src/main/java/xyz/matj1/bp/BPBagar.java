/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMotionWord;
import mcdr.objects.utils.RankedSortedCollectionDistHashcode;
import mcdr.sequence.SequenceMotionWords;
import mcdr.sequence.impl.SequenceMotionWordsDTW;
import mcdr.test.utils.ObjectCategoryMgmt;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.AnswerType;
import messif.operations.query.KNNQueryOperation;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;

/**
 *
 * @author xkohout7
 */
public class BPBagar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {               
        /************** Working with MW data (hard MWs) *********************/
        // load  data
        final String mwDataFilepath = Path.of(
             Config.dataDir().getPath(),
             "motion words",
             "hard_motion_words.data"
        ).toString();

        final Class<? extends SequenceMotionWords<?>> mwClass = SequenceMotionWordsDTW.class;
        ObjectCategoryMgmt mwCategoryMgmt = new ObjectCategoryMgmt();
        ObjectMgmt mwSequenceMgmt = new ObjectMgmt(mwCategoryMgmt);
        mwSequenceMgmt.read(mwClass, mwDataFilepath);
        
        // evaluate distance=dissimilarity of two objects
        Iterator<LocalAbstractObject> mwSequenceIt = mwSequenceMgmt.getObjects().iterator();        
        SequenceMotionWordsDTW mwSequence1 = (SequenceMotionWordsDTW) mwSequenceIt.next();
        SequenceMotionWordsDTW mwSequence2 = (SequenceMotionWordsDTW) mwSequenceIt.next();
        float distance = mwSequence1.getDistance(mwSequence2);
        System.out.println("Distance between "+sequenceToString(mwSequence1)+" and "+sequenceToString(mwSequence2)+" is "+distance);
        System.out.println("The class of object "+sequenceToString(mwSequence1)+" is "+ ObjectMgmt.parseObjectCategoryId(mwSequence1));
        
        // evaluate kNN search - you probably won't be using this but it can be useful for some cross-checking
        SequenceMotionWordsDTW query = mwSequence1;
        Integer queryCategorySize = mwSequenceMgmt.getObjectCountInCategory(mwSequenceMgmt.getObjectCategories(query).get(0));
        KNNQueryOperation op = new KNNQueryOperation(query, queryCategorySize-1, false, AnswerType.ORIGINAL_OBJECTS, new RankedSortedCollectionDistHashcode());
        for (LocalAbstractObject dataObject : mwSequenceMgmt.getObjects()) {
            SequenceMotionWordsDTW mwObject = (SequenceMotionWordsDTW) dataObject;
            if (!mwObject.getLocatorURI().equals(query.getLocatorURI())) {
                op.addToAnswer(mwObject, query.getDistance(mwObject), null);
            }
        }
        System.out.println("Top "+(queryCategorySize-1)+" results for query "+sequenceToString(query)+": ");
        Iterator<RankedAbstractObject> answerIt = op.getAnswer();
        while (answerIt.hasNext()) {
            RankedAbstractObject resultObject = answerIt.next();
            System.out.println("  "+sequenceToString((SequenceMotionWords)resultObject.getObject())+" - distance "+resultObject.getDistance());
        }

        
        // a task: evaluate kNN search for all objects with k=size of the query object's category
        // compute average precision and compare to results in ECIR paper - it should be the same or at least very similar
        
        //b task: for each class, compute the average distance to all classses
        
        //third task: find the range of sizes of objects in individual classes       
        
        /************** Working with MoCap data *********************/
        // load  data      
//        final String mocapDataFilepath = "...\\class130-actions-coords.data";                       
//        final Class<? extends SequenceMocap<?>> mocapSeqClass = SequenceMocapPoseCoordsL2DTW.class;
//        ObjectCategoryMgmt mocapCategoryMgmt = new ObjectCategoryMgmt();
//        ObjectMgmt mocapMgmt = new ObjectMgmt(mocapCategoryMgmt);
//        mocapMgmt.read(mocapSeqClass, mocapDataFilepath); 
        
    }
    
    private static String sequenceToString(SequenceMotionWords seq) {
        String rtv = "";
        for (Object o : seq.getSequenceData()) {
            ObjectMotionWord mw = (ObjectMotionWord) o;
            if (rtv.length()>0) {
                rtv+=",";
            }            
            rtv+=mw.toString();
        }
        return seq.getLocatorURI()+" ["+rtv+"]";
    }
    
}
