/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMotionWord;
import mcdr.objects.impl.ObjectMotionWordNMatches;
import mcdr.objects.impl.ObjectMotionWordSoftAssignment;
import mcdr.objects.utils.RankedSortedCollectionDistHashcode;
import mcdr.sequence.SequenceMocap;
import mcdr.sequence.SequenceMotionWords;
import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import mcdr.sequence.impl.SequenceMotionWordsDTW;
import mcdr.sequence.impl.SequenceMotionWordsNMatchesDTW;
import mcdr.sequence.impl.SequenceMotionWordsSoftAssignmentDTW;
import mcdr.test.utils.ObjectCategoryMgmt;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.AnswerType;
import messif.operations.query.KNNQueryOperation;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;

/**
 *
 * @author xkohout7
 */
public class BPBagarNew {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {               
        /************** Working with MW data (hard MWs) *********************/
        // load  data

        final String mwDataFilepath;
        final Class<? extends SequenceMotionWords<?>> mwClass;
        ObjectMotionWordSoftAssignment.maxPartsToMatch = 6;
        ObjectMotionWordNMatches.nMatches = 1;
        ObjectMotionWordNMatches.maxPartsToMatch = 5;

        //hMWs
        {
            mwDataFilepath = Path.of(
                 Config.dataDir().getPath(),
                 "motion words",
                 "hard_motion_words.data"
            ).toString();
            mwClass = SequenceMotionWordsDTW.class;
        }

        //sMWs
        if (false) {
            mwClass = SequenceMotionWordsSoftAssignmentDTW.class;
            mwDataFilepath = Path.of(
                 Config.dataDir().getPath(),
                 "motion words",
                 "soft_motion_words.data"
            ).toString();
        }

        //moMWs
        if (false) {
            mwClass = SequenceMotionWordsNMatchesDTW.class;
            mwDataFilepath = Path.of(
                 Config.dataDir().getPath(),
                 "motion words",
                 "multiple-overlay_motion_words.data"
            ).toString();
        }

        // MoCap data     
//        final String mwDataFilepath = "y:/datasets/mocap/hdm05/class130-actions-coords_normPOS-fps12.data";
//        final Class<? extends SequenceMocap<?>> mwClass = SequenceMocapPoseCoordsL2DTW.class;
        final String mocapDataFilepath = Path.of(
             Config.dataDir().getPath(),
             "skeleta",
             "class130-actions-coords_normPOS.data"
        ).toString();
        final Class<? extends SequenceMocap<?>> mocapSeqClass = SequenceMocapPoseCoordsL2DTW.class;

        ObjectCategoryMgmt mocapCategoryMgmt = new ObjectCategoryMgmt();
        ObjectMgmt mocapMgmt = new ObjectMgmt(mocapCategoryMgmt);
        mocapMgmt.read(mocapSeqClass, mocapDataFilepath); 

        ObjectCategoryMgmt mwCategoryMgmt = new ObjectCategoryMgmt();
        ObjectMgmt mwSequenceMgmt = new ObjectMgmt(mwCategoryMgmt);
        mwSequenceMgmt.read(mwClass, mwDataFilepath);
        
        // evaluate distance=dissimilarity of two objects
//        Iterator<LocalAbstractObject> mwSequenceIt = mwSequenceMgmt.getObjects().iterator();
//        SequenceMotionWords mwSequence1 = (SequenceMotionWords) mwSequenceIt.next();
//        SequenceMotionWords mwSequence2 = (SequenceMotionWords) mwSequenceIt.next();
//        float distance = mwSequence1.getDistance(mwSequence2);
//        System.out.println("Distance between "+sequenceToString(mwSequence1)+" and "+sequenceToString(mwSequence2)+" is "+distance);
//        System.out.println("The class of object "+sequenceToString(mwSequence1)+" is "+mwSequenceMgmt.parseObjectCategoryId(mwSequence1));
//
//        // evaluate kNN search - you probably won't be using this but it can be useful for some cross-checking
//        SequenceMotionWords query = mwSequence1;
//        Integer queryCategorySize = mwSequenceMgmt.getObjectCountInCategory(mwSequenceMgmt.getObjectCategories(query).get(0));
//        KNNQueryOperation op = new KNNQueryOperation(query, queryCategorySize-1, false, AnswerType.ORIGINAL_OBJECTS, new RankedSortedCollectionDistHashcode());
//        for (LocalAbstractObject dataObject : mwSequenceMgmt.getObjects()) {
//            SequenceMotionWords mwObject = (SequenceMotionWords) dataObject;
//            if (!mwObject.getLocatorURI().equals(query.getLocatorURI())) {
//                op.addToAnswer(mwObject, query.getDistance(mwObject), null);
//            }
//        }
//        System.out.println("Top "+(queryCategorySize-1)+" results for query "+sequenceToString(query)+": ");
//        Iterator<RankedAbstractObject> answerIt = op.getAnswer();
//        while (answerIt.hasNext()) {
//            RankedAbstractObject resultObject = answerIt.next();
//            System.out.println("  "+sequenceToString((SequenceMotionWords)resultObject.getObject())+" - distance "+resultObject.getDistance());
//        }

        
        // first task: evaluate kNN search for all objects with k=size of the query object's category
        // compute average precision and compare to results in ECIR paper - it should be the same or at least very similar
        Iterator<LocalAbstractObject> mwSequenceIt = mwSequenceMgmt.getObjects().iterator();
        float sumOfPrecision = 0f;
        float sumOfResultSizes = 0f;
        float sumOfCorrectResults = 0f;
        LocalAbstractObject abstractQuery;
        while (mwSequenceIt.hasNext()) {
            abstractQuery = mwSequenceIt.next();
            String queryCategoryId = mwSequenceMgmt.getObjectCategories(abstractQuery).get(0).id;
            int queryCategorySize = mwSequenceMgmt.getObjectCountInCategory(mwSequenceMgmt.getObjectCategories(abstractQuery).get(0));
            if (queryCategorySize<2) {
                System.out.println("Query "+abstractQuery.getLocatorURI()+" has category size "+queryCategorySize);
                continue;
            }
            System.out.println("Processing query "+abstractQuery.getLocatorURI()+", category size "+queryCategorySize);
            KNNQueryOperation op = new KNNQueryOperation(
                 abstractQuery,
                 queryCategorySize-1,
                 false,
                 AnswerType.ORIGINAL_OBJECTS,
                 new RankedSortedCollectionDistHashcode()
            );
            for (LocalAbstractObject dataObject : mwSequenceMgmt.getObjects()) {
                if (!dataObject.getLocatorURI().equals(abstractQuery.getLocatorURI())) {
                    op.addToAnswer(dataObject, abstractQuery.getDistance(dataObject), null);
                }
            }

            Iterator<RankedAbstractObject> answerIt = op.getAnswer();
            float correctResultsCount = 0;
            System.out.print("  Answer: ");
            while (answerIt.hasNext()) {
                RankedAbstractObject resultObject = answerIt.next();
                System.out.print(resultObject.getObject().getLocatorURI()+"("+resultObject.getDistance()+") ");
                String resultCategoryId = mwSequenceMgmt.getObjectCategories((LocalAbstractObject)resultObject.getObject()).get(0).id;
                if (queryCategoryId.equals(resultCategoryId)) {
                    correctResultsCount++;
                }
            }

            System.out.println("\n Precision: "+correctResultsCount/(queryCategorySize-1));
            sumOfPrecision += correctResultsCount/(queryCategorySize-1); 
            sumOfCorrectResults += correctResultsCount;
            sumOfResultSizes += op.getAnswerCount();
        }
        System.out.println("Average precision: "+(sumOfPrecision/(mwSequenceMgmt.getObjects().size()-2)));
        System.out.println("Average precision - Honza style: "+sumOfCorrectResults/sumOfResultSizes);
        
        //second task: for each class, compute the average distance to all classses
        
        //third task: find the range of sizes of objects in individual classes               
        
    }
    
    private static String sequenceToString(SequenceMotionWords<ObjectMotionWord> seq) {
        String rtv = "";
        for (ObjectMotionWord mw : seq.getSequenceData()) {
            if (rtv.length()>0) {
                rtv+=",";
            }            
            rtv+=mw.toString();
        }
        return seq.getLocatorURI()+" ["+rtv+"]";
    }
}
