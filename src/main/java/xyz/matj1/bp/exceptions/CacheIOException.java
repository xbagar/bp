package xyz.matj1.bp.exceptions;

/**
 * This is thrown when there is a problem with writing or reading cached results
 */
public class CacheIOException extends Exception {
	public CacheIOException() {}

	public CacheIOException(String message) {
		super(message);
	}

	public CacheIOException(String message, Throwable cause) {
		super(message, cause);
	}

	public CacheIOException(Throwable cause) {
		super(cause);
	}

	public CacheIOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
