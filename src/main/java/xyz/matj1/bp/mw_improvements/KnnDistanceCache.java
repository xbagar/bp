package xyz.matj1.bp.mw_improvements;

import messif.objects.LocalAbstractObject;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.Vector2;

import java.util.Map;

class KnnDistanceCache {
	private final Map<Vector2<LocalAbstractObject>, Float> cache;
	private static final KnnDistanceCache instance = new KnnDistanceCache();

	public KnnDistanceCache() {
		this.cache = new MyMap<>();
	}

	public Float getDistance(LocalAbstractObject s1, LocalAbstractObject s2) {
		var comparison = 0 > CharSequence.compare(s1.getLocatorURI(), s2.getLocatorURI());
		var first = comparison ? s1 : s2;
		var second = comparison ? s2 : s1;

		return this.cache.computeIfAbsent(new Vector2<>(first, second), vec -> vec.a().getDistance(vec.b()));
	}

	public static KnnDistanceCache instance() {
		return KnnDistanceCache.instance;
	}
}
