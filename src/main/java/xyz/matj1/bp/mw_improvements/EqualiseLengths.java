package xyz.matj1.bp.mw_improvements;

import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import messif.objects.AbstractObject;
import messif.objects.keys.AbstractObjectKey;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.LengthTesting;
import xyz.matj1.bp.LinearTimeWarp;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.Pair;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * This is for creating a dataset from HDM05 where all motions have the same length.
 */
public class EqualiseLengths {
	public static void main(String[] args) throws IOException {
		var skeletonMgmt = Utils.loadData(LengthTesting.dataFiles(MocapType.SKELETON), MocapType.SKELETON.dataClass());

		// Change the uri of the sequence to make sure that the first 3 parts are unique
		skeletonMgmt.getObject("3417_38_6416_388").setObjectKey(new AbstractObjectKey("9001_38_6416_388"));

		var categoryObjects = LinearTimeWarp.convertMocap(skeletonMgmt.getCategoryObjects());
		var equalisedObjects = equaliseLengths(categoryObjects);


		var equalisedUris = equalisedObjects.values().stream().flatMap(Collection::stream).map(AbstractObject::getLocatorURI).toList();
		var uniqueAndDuplicateUris = equalisedUris.stream().collect(
			() -> new Pair<>(new HashSet<String>(), new ArrayList<String>()),
			(containers, element) -> {
				if (!containers.a().add(element)) {
					containers.b().add(element);
			} },
			(cont1, cont2) -> { cont1.a().addAll(cont2.a()); cont1.b().addAll(cont2.b()); }
		);
		if (!uniqueAndDuplicateUris.b().isEmpty()) {
			uniqueAndDuplicateUris.b().forEach(System.out::println);
		}


		var originalUris = equalisedObjects.values().stream().flatMap(Collection::stream).map(AbstractObject::getLocatorURI).toList();
		var originalUniqueAndDuplicateUris = originalUris.stream().collect(
			() -> new Pair<>(new HashSet<String>(), new ArrayList<String>()),
			(containers, element) -> {
				if (!containers.a().add(element)) {
					containers.b().add(element);
				} },
			(cont1, cont2) -> { cont1.a().addAll(cont2.a()); cont1.b().addAll(cont2.b()); }
		);
		if (!originalUniqueAndDuplicateUris.b().isEmpty()) {
			originalUniqueAndDuplicateUris.b().forEach(System.out::println);
		}


		categoryObjectsToFile(
			equalisedObjects,
			Utils.composeFile(Config.dataDir(), "skeleta", "class130-actions-coords_normPOS_length-equalised.data")
		);
	}

	/**
	 * This converts the given structure with skeleton motions to one where all skeleton motions
	 * are stretched so that their length is the average length of motions in the given structure.
	 *
	 * I chose the average length because it seems that it creates
	 * sequences not too big and with not too little data.
	 * The estimate of the average of lengths in HDM05 seems like a sensible length.
	 *
	 * @param skeletonMocap a structure containing skeleton motions
	 * @return the structure with all skeletons stretch to the same length
	 * @param <A> the type of the keys in of the map
	 */
	public static <A> MyMap<A, List<SequenceMocapPoseCoordsL2DTW>> equaliseLengths(MyMap<A, ? extends Collection<SequenceMocapPoseCoordsL2DTW>> skeletonMocap) {
		var averageLength = skeletonMocap.values().stream()
			.flatMap(Collection::stream)
			.map(Utils::sequenceLength)
			.mapToDouble(x -> x)
			.average().orElseThrow();

		var equalisedMocap = skeletonMocap.mapValues(
			list -> list.parallelStream().map(motion -> {
				var multiple = averageLength / Utils.sequenceLength(motion);
				return LinearTimeWarp.stretchSequence(motion, multiple);
			}).toList()
		);

		return equalisedMocap;
	}

	/**
	 * This is like {@link #equaliseLengths(MyMap)},
	 * but it sets the length to the double of the average.
	 *
	 * @param skeletonMocap a structure containing skeleton motions
	 * @return the structure with all skeletons stretch to the same length
	 * @param <A> the type of the keys in of the map
	 */
	public static <A> MyMap<A, List<SequenceMocapPoseCoordsL2DTW>> equaliseLengthsToDoubleAverage(MyMap<A, ? extends Collection<SequenceMocapPoseCoordsL2DTW>> skeletonMocap) {
		var doubleAverageLength = skeletonMocap.values().stream()
			.flatMap(Collection::stream)
			.map(Utils::sequenceLength)
			.mapToDouble(x -> x)
			.average().orElseThrow()
			* 2;

		var equalisedMocap = skeletonMocap.mapValues(
			list -> list.parallelStream().map(motion -> {
				var multiple = doubleAverageLength / Utils.sequenceLength(motion);
				return LinearTimeWarp.stretchSequence(motion, multiple);
			}).toList()
		);

		return equalisedMocap;
	}

	public static void categoryObjectsToFile(MyMap<Category, List<SequenceMocapPoseCoordsL2DTW>> data, File fileName) throws IOException {
		try(var output = new BufferedOutputStream(
			new FileOutputStream(fileName)
		)) {
			for (var entry : data.entrySet()) {
				for (var sequence : entry.getValue()) {
					sequence.write(output);
			} }
			output.flush();
	} }
}
