package xyz.matj1.bp.mw_improvements;

import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import org.jfree.chart.ChartFactory;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.LengthTesting;
import xyz.matj1.bp.StretchedSequences;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.experiments.KnnSearchTools;
import xyz.matj1.bp.types.Counter;
import xyz.matj1.bp.types.MyMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import static xyz.matj1.bp.Utils.loadData;
import static xyz.matj1.bp.Utils.readCache;
import static xyz.matj1.bp.Utils.writeCache;

/**
 * This is for experiments with dataset with equalised lengths and motion words with multiple segmentations.
 */
public class EqualLengthExperiments {
	public static void main(String[] args) throws IOException {
		var outputDir = new File(Config.outputDir(), "equalised lengths");
		outputDir.mkdirs();

		equalisedLengthsPrecisionShowBox(new File(outputDir, "precision box graph.pdf"));
		equalisedLengthsPrecisionInspect(new PrintWriter(System.out));
		eqLenCategoriesOfKnn(new PrintWriter(new FileOutputStream(new File(outputDir, "neighbor categories"))));
		originalCategoriesOfKnn(new PrintWriter(new FileOutputStream(new File(outputDir, "original neighbor categories"))));
	}

	/**
	 * This creates a box graph with precisions of searching in original sequences
	 * and the length-equalised ones for every mocap type.
	 *
	 * @param output where the graph shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void equalisedLengthsPrecisionShowBox(File output) throws IOException {
		output.getParentFile().mkdirs();
		var knnPrecisions = getKnnPrecisions();
		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (var type : MocapType.values()) {
			var category_List = knnPrecisions.get(type);
			var flatValues = category_List.values()
				.stream()
				.flatMap(Collection::stream)
				.toList();
			var originalPrecisions = originalPrecisions(type); // baseline for comparison
			dataset.add(originalPrecisions, "original", type);
			dataset.add(flatValues, "equalised to average", type);
		}

		var doubleKnnPrecisions = doubleKnnPrecisions();
		for (var type : MocapType.mwValues()) {
			var category_List = doubleKnnPrecisions.get(type);
			var flatValues = category_List.values()
				.stream()
				.flatMap(Collection::stream)
				.toList();
			dataset.add(flatValues, "equalised to double average", type);

		}

		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"precision of kNN search",
			dataset,
			true
		);
		Utils.writeXYGraphAsPdf(
			graph,
			output,
			Config.graphDimensions()
		);
	}

	/**
	 * This is for figuring out why precisions of equalised sequences are lower.
	 *
	 * @param output where the graph shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void equalisedLengthsPrecisionInspect(PrintWriter output) throws IOException {
		var eqKnnPrecisions = getKnnPrecisions();
		var origKnnPrecisions = StretchedSequences.getKnnMeasurements();

		for (var type : MocapType.values()) {
			output.println(type);
			var eqAveragePrecisions = eqKnnPrecisions.get(type)
				.mapValues(
					list -> list.stream().mapToDouble(x -> x)
						.average().orElseThrow());
			var origAveragePrecisions = origKnnPrecisions.get(type).get(SpeedMultiple._1)
				.mapValues(
					list -> list.stream().mapToDouble(x -> x)
						.average().orElseThrow());

			assert eqAveragePrecisions.keySet().equals(origAveragePrecisions.keySet());
			for (var category : eqAveragePrecisions.keySet()) {
				output.printf(
					"%s\t%s\t%s%n",
					category.id,
					origAveragePrecisions.get(category),
					eqAveragePrecisions.get(category));
			}
		}
		output.flush();
	}

	/**
	 * This does kNN searches and prints categories of the neighbors.
	 *
	 * @param output where the output shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void eqLenCategoriesOfKnn(PrintWriter output) throws IOException {
		var result = new MyMap<MocapType, MyMap<Category, Counter<Category>>>();

		for (var type : MocapType.values()) {
			var sequenceMgmt = loadData(dataFiles(type), type.dataClass());
			var knnResults = knnSearchResults(type, sequenceMgmt);

			result.put(type, categoriesOfKnn(sequenceMgmt, knnResults));
		}

		output.println(categoriesOfKnnToJson(result));
		output.flush();
	}

	/**
	 * This does kNN searches in the original data
	 * and prints categories of the neighbors.
	 *
	 * @param output where the output shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void originalCategoriesOfKnn(PrintWriter output) throws IOException {
		var result = new MyMap<MocapType, MyMap<Category, Counter<Category>>>();

		for (var type : MocapType.values()) {
			var sequenceMgmt = loadData(LengthTesting.dataFiles(type), type.dataClass());
			var knnResults = KnnSearchTools.knnSearchResultsWithCaching(
				type,
				sequenceMgmt,
				new File(Config.cacheDir(), "originalKnnResults"));

			result.put(type, categoriesOfKnn(sequenceMgmt, knnResults));
		}

		output.println(categoriesOfKnnToJson(result));
		output.flush();
	}

	static MyMap<Category, Counter<Category>> categoriesOfKnn(
		ObjectMgmt sequenceMgmt,
		MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnResults
	) {
		var result = new MyMap<Category, Counter<Category>>();

		// map from queries to counters of categories of neighbors
		var neighborCategories = knnResults.mapValues(
			mymap -> mymap
				.keySet().stream()
				.map(
					neighbor -> sequenceMgmt.getObjectCategories(neighbor).get(0))
				.collect(Counter.collector()));

		// map from each category A to a counter of categories of neighbors of all queries from A
		var aggregatedNeighborCategories = neighborCategories.map(
			sequence -> sequenceMgmt.getObjectCategories(sequence).get(0),
			Function.identity(),
			Counter::sum);

		for (var entry : aggregatedNeighborCategories.entrySet()) {
			var query = entry.getKey();
			var results = entry.getValue();

			result.put(query, results);
		}

		return result;
	}

	static String categoriesOfKnnToJson(MyMap<MocapType, MyMap<Category, Counter<Category>>> basis) {
		var result = new StringBuilder();
		var lineSep = System.lineSeparator();

		result.append("{" + lineSep);

		var typeValues = basis.keySet().stream().sorted().toList();
		for (var type : typeValues) {
			result.append(String.format("\"%s\": {%n", type));

			var entryList = basis.get(type)
				.entrySet().stream()
				.sorted(
					Entry.comparingByKey(
						Comparator.comparing(
							category -> Integer.parseInt(category.id))))
				.toList();
			for (var i = 0; i < entryList.size(); i++) {
				var query = entryList.get(i).getKey();
				var results = entryList.get(i).getValue();

				result.append(String.format(
					i == entryList.size() - 1
						? "\"%s\": %s%n"
						: "\"%s\": %s,%n",
					query.id,
					results.toJson(category -> category.id)));
			}

			if (type == typeValues.get(typeValues.size() - 1)) {
				result.append("}" + lineSep);
			} else {
				result.append("}," + lineSep);
			}
		}

		result.append("}");
		return result.toString();
	}

	static List<Double> originalPrecisions(MocapType type) throws IOException {
		var originalPrecisions = StretchedSequences
			.getKnnMeasurements()
			.get(type)
			.get(SpeedMultiple._1);
		return originalPrecisions.values()
			.stream()
			.flatMap(Collection::stream)
			.toList();
	}

	/**
	 * This gets, in the dataset of sequences with equalised lengths,
	 * for every mocap type, for every category, for every sequence in the category,
	 * the precision of kNN search of the sequence among all other sequences.
	 *
	 * This either computes it or loads it from cache.
	 *
	 * @return a map from every mocap type to a map from every category to a list of all kNN precisions of its sequences
	 * @throws IOException if the data for the computation can't be loaded
	 */
	public static MyMap<MocapType, MyMap<Category, List<Double>>> getKnnPrecisions() throws IOException {
		var knnPrecisions = new MyMap<MocapType, MyMap<Category, List<Double>>>();

		for(var type : MocapType.values()) {
			knnPrecisions.put(type, getKnnPrecisions(type));
		}

		return knnPrecisions;
	}

	/**
	 * This retrieves kNN precisions for every category of sequences of the given mocap type.
	 *
	 * @param type a mocap type on the sequences of which kNN search shall be done
	 * @return a map from every category to a list of precisions, one for each sequence
	 * @throws IOException if the data can't be loaded
	 */
	public static MyMap<Category, List<Double>> getKnnPrecisions(MocapType type) throws IOException {
		var cacheDir = new File(Config.cacheDir(), "eqLenKnnPrecision");
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s", type.noun()));

		try {
			return readCache(cachePath, MyMap.class);
		} catch(CacheIOException _e) {
			var sequenceMgmt = loadData(
				dataFiles(type),
				type.dataClass()
			);

			var result = KnnSearchTools.computeKnnPrecisions(
				knnSearchResults(type, sequenceMgmt),
				sequenceMgmt
			);

			try {
				writeCache(cachePath, result);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return result;
		}
	}

	public static MyMap<MocapType, MyMap<Category, List<Double>>> doubleKnnPrecisions() throws IOException {
		var knnPrecisions = new MyMap<MocapType, MyMap<Category, List<Double>>>();

		for(var type : MocapType.mwValues()) {
			knnPrecisions.put(type, doubleKnnPrecisions(type));
		}

		return knnPrecisions;
	}

	public static MyMap<Category, List<Double>> doubleKnnPrecisions(MocapType type) throws IOException {
		var cacheDir = new File(Config.cacheDir(), "eqLenDoubleKnnPrecision");
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s", type.noun()));

		try {
			return readCache(cachePath, MyMap.class);
		} catch(CacheIOException _e) {
			var sequenceMgmt = loadData(
				doubleDataFiles(type),
				type.dataClass()
			);

			var result = KnnSearchTools.computeKnnPrecisions(
				doubleKnnSearchResults(type, sequenceMgmt),
				sequenceMgmt
			);

			try {
				writeCache(cachePath, result);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return result;
		}
	}

	/**
	 * This retrieves kNN search results for every sequence of the given
	 * <code>type</code> among all other sequences of it
	 * by reading cache or by computing the results.
	 *
	 * This loads the dataset from disk. To avoid loading it, if there is one already,
	 * use {@link #knnSearchResults(MocapType, ObjectMgmt)}.
	 *
	 * @param type the type of the sequences for which kNN search shall be done
	 * @return a map from every sequence to a map of its nearest neighbors and their distances
	 */
	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnSearchResults(MocapType type) throws IOException {
		var sequenceMgmt = loadData(dataFiles(type), type.dataClass());
		return knnSearchResults(type, sequenceMgmt);
	}

	/**
	 * This retrieves kNN search results for every sequence of the given
	 * <code>sequenceMgmt</code> among all other sequences of it
	 * by reading cache or by computing the results.
	 *
	 * The given <code>type</code> has to correspond to the type of the sequences in <code>sequenceMgmt</code>.
	 *
	 * This is like {@link #knnSearchResults(MocapType)},
	 * but to avoid loading data if they are already loaded.
	 *
	 * @param type the type of the sequences
	 * @param sequenceMgmt the sequence management with the sequences for kNN
	 * @return a map from every sequence to a map of its nearest neighbors and their distances
	 */
	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnSearchResults(MocapType type, ObjectMgmt sequenceMgmt) {
		var cacheDir = new File(Config.cacheDir(), "eqLenKnnResults");
		return KnnSearchTools.knnSearchResultsWithCaching(type, sequenceMgmt, cacheDir);
	}

	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> doubleKnnSearchResults(MocapType type, ObjectMgmt sequenceMgmt) {
		var cacheDir = new File(Config.cacheDir(), "eqLenKnnResultsDouble");
		return KnnSearchTools.knnSearchResultsWithCaching(type, sequenceMgmt, cacheDir);
	}

	/**
	 * This returns a File pointing to the file with the dataset of the given <code>type</code>.
	 *
	 * @param type the type
	 * @return a File pointing to its dataset
	 */
	public static File dataFiles(MocapType type) {
		return new File(
			Config.dataDir(),
			switch (type) {
				case SKELETON -> "skeleta/class130-actions-coords_normPOS_length-equalised.data";
				case HARD_MW -> "motion words/hard_motion_words_length_equalised.data";
				case SOFT_MW -> "motion words/soft_motion_words_length_equalised.data";
				case MATCH_N_MW -> "motion words/multi-overlay_motion_words_length_equalised.data";
		});
	}

	public static File doubleDataFiles(MocapType type) {
		return new File(
			Config.dataDir(),
			switch (type) {
				case SKELETON -> throw new IllegalArgumentException("Skeleton sequences with lengths equalised to double of the average aren't available because there isn't time to do kNN search on them.");
				case HARD_MW -> "motion words/hard_motion_words_length_equalised_double.data";
				case SOFT_MW -> "motion words/soft_motion_words_length_equalised_double.data";
				case MATCH_N_MW -> "motion words/multi-overlay_motion_words_length_equalised_double.data";
			});

	}
}
