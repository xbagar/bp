package xyz.matj1.bp.mw_improvements;

import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import org.jfree.chart.ChartFactory;
import org.jfree.data.statistics.BoxAndWhiskerCalculator;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.StretchedSequences;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SegmentLength;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.experiments.KnnSearchTools;
import xyz.matj1.bp.types.MyMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static xyz.matj1.bp.Utils.entryCollector;
import static xyz.matj1.bp.Utils.loadData;
import static xyz.matj1.bp.Utils.readCache;
import static xyz.matj1.bp.Utils.writeCache;
import static xyz.matj1.bp.Utils.writeXYGraphAsPdf;
import static xyz.matj1.bp.enums.MocapType.SKELETON;
import static xyz.matj1.bp.enums.SegmentLength._80;
import static xyz.matj1.bp.enums.SpeedMultiple._1;
import static xyz.matj1.bp.experiments.KnnSearchTools.computeKnnPrecisions;

public class MultipleSegmentationExperiments {
	public static void main(String[] args) throws IOException {
		File outputDir = new File(Config.outputDir(), "multiple segmentations");
		outputDir.mkdirs();

		multiSegmentationPrecisionShowBox(new File(outputDir, "precision box graph.pdf"));
		dualSearchWithSkeletonsPrecisionShowBox(new File(outputDir, "dual-search precision box graph.pdf"));
		dualSearchWithCategoryDistancesPrecisionShowBox(new File(outputDir, "dual-search precision with perfect refinement.pdf"));
		dualSearchWithBothRefinementsShowBox(new File(outputDir, "dual-search precision with both refinements box graph.pdf"));
		candidateSetIntersectionsShowBox(new File(outputDir, "candidate set intersections.pdf"));
		segLenCombosPrecisionsPrint(new PrintWriter(new FileOutputStream(new File(outputDir, "segLen combos"))));
	}

	/**
	 * This creates a box graph with precisions of every segment length for every mocap type.
	 * This doesn't do searching for candidate sets and then refining.
	 *
	 * This is to chech precisions of single motion-word searches with certain segment lengths
	 *
	 * @param output where the graph shall be written
	 */
	private static void multiSegmentationPrecisionShowBox(File output) {
		output.getParentFile().mkdirs();
		var knnPrecisions = getKnnPrecisions();
		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (var type : MocapType.mwValues()) {
			for (var length : SegmentLength.values()) {
				var category_List = knnPrecisions.get(type).get(length);
				var flatValues = category_List.values().stream()
					.flatMap(Collection::stream)
					.toList();

				dataset.add(flatValues, length, type);
			}
		}
		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"precision of kNN search",
			dataset,
			true
		);
		writeXYGraphAsPdf(
			graph,
			output,
			Config.graphDimensions()
		);
	}

	/**
	 * For every mocap type this does multi-segment searches for candidate sets
	 * and refines it with searching on skeletons.
	 *
	 * This produces a box graph with the precisions of the searches.
	 *
	 * @param output where the data shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void dualSearchWithSkeletonsPrecisionShowBox(File output) throws IOException {
		output.getParentFile().mkdirs();

		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		var skeletonMgmt = loadData(
				StretchedSequences.dataFiles(SKELETON).get(_1),
				SKELETON.dataClass());

//		add baseline precisions to the dataset for comparison
		var flatOriginalValues = EqualLengthExperiments.originalPrecisions(SKELETON);
		dataset.add(flatOriginalValues, "original skeletons", "");


		for (var type : MocapType.mwValues()) {
			var precisions = doMultiSegmentationSearch(
				MultipleSegmentationExperiments::refineResultsOnSkeletons,
				skeletonMgmt,
				type
			)
				.values().stream()
				.sorted().toList();

			var originalPrecisions = EqualLengthExperiments.originalPrecisions(type);

			dataset.add(originalPrecisions, "original", type);
			dataset.add(precisions, "multiple segmentations", type);
		}

		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"precision",
			dataset,
			true);

		writeXYGraphAsPdf(graph, output, Config.graphDimensions());
	}

	/**
	 * For every mocap type this does multi-segment searches for candidate sets
	 * and refines it with searching nearest sequences according to categories
	 * (such that the distance is 0 if the sequences are in the same category, 1 otherwise).
	 *
	 * This produces a box graph with the precisions of the searches.
	 *
	 * This is to show the best case for multi-segment searching, if the refinement is perfect.
	 *
	 * @param output where the data shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void dualSearchWithCategoryDistancesPrecisionShowBox(File output) throws IOException {
		output.getParentFile().mkdirs();

		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		var skeletonMgmt = loadData(
			StretchedSequences.dataFiles(SKELETON).get(_1),
			SKELETON.dataClass());

//		add baseline precisions to the dataset for comparison
		var flatOriginalValues = EqualLengthExperiments.originalPrecisions(SKELETON);
		dataset.add(flatOriginalValues, "original skeletons", "");

		for (var type : MocapType.mwValues()) {
			MyMap<LocalAbstractObject, Double> precisions = doMultiSegmentationSearch(
				MultipleSegmentationExperiments::refineResultsByCategories,
				skeletonMgmt,
				type
			);

			dataset.add(new ArrayList<>(precisions.values()), type, "");
		}

		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"precision",
			dataset,
			true);

		writeXYGraphAsPdf(graph, output, Config.graphDimensions());
	}

	private static void candidateSetIntersectionsShowBox(File output) throws IOException {
		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (var type : MocapType.mwValues()) {
			var mwKnnResults = segmentLengthKnnResults(type);
			var transposedResultsWithUris = transposeResultsWithUris(
				mwKnnResults);
			var resultsWithUris = transposeMap(transposedResultsWithUris);
			var candidateSetsWithUris = transposedResultsWithUris
				.mapValues(
					length_Neighbor_Distance -> length_Neighbor_Distance
						.values().stream()
						.flatMap(
							map -> map.keySet().stream())
						.collect(Collectors.toSet()));

			dataset.add(
				candidateSetsWithUris
					.values().stream()
					.map(Collection::size).toList(),
				"size of candidate set",
				type
			);

			for (var length : SegmentLength.values()) {
				var thisLengthsCandidateSetPart = resultsWithUris.get(length).mapValues(ConcurrentHashMap::keySet);
				var intersections = candidateSetsWithUris.map(
					(q, r) -> q,
					(query, results) -> {
						var result = new HashSet<>(thisLengthsCandidateSetPart.get(query));
						results.retainAll(result);
						return result;
					});
				var sizes = intersections.values().stream().map(Collection::size).toList();

				dataset.add(
					sizes,
					length,
					type
				);
				sizes.stream().sorted().forEach(size -> {
					System.err.print(size); System.err.print(" ");
				});
				System.err.println();
		} }

		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"mocap type",
			"candidate set (intersection) size",
			dataset,
			true
		);
		writeXYGraphAsPdf(graph, output, Config.graphDimensions());
	}

	private static void dualSearchWithBothRefinementsShowBox(File output) throws IOException {
		output.getParentFile().mkdirs();

		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		var skeletonMgmt = loadData(
			StretchedSequences.dataFiles(SKELETON).get(_1),
			SKELETON.dataClass());

//		add baseline precisions to the dataset for comparison
		var flatOriginalValues = EqualLengthExperiments.originalPrecisions(SKELETON);
		dataset.add(flatOriginalValues, "original", SKELETON);


		for (var type : MocapType.mwValues()) {
			var skeletonPrecisions = doMultiSegmentationSearch(
				MultipleSegmentationExperiments::refineResultsOnSkeletons,
				skeletonMgmt,
				type
			)
				.values().stream()
				.sorted().toList();
			var idealPrecisions = doMultiSegmentationSearch(
				MultipleSegmentationExperiments::refineResultsByCategories,
				skeletonMgmt,
				type
			)
				.values().stream()
				.sorted().toList();
			var originalPrecisions = EqualLengthExperiments.originalPrecisions(type).stream()
				.sorted().toList();

			dataset.add(originalPrecisions, "original", type);
			dataset.add(skeletonPrecisions, "skeleton refinement", type);
			dataset.add(idealPrecisions, "ideal refinement", type);
		}

		var graph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"precision",
			dataset,
			true);

		writeXYGraphAsPdf(graph, output, Config.graphDimensions());
	}


	/**
	 * This prints precisions for dual search using only some combinations of segment lengths.
	 *
	 * @param output where the output shall be printed
	 */
	private static void segLenCombosPrecisionsPrint(PrintWriter output) throws IOException {
		var combos = Utils.subsetsWith(
			Arrays.asList(SegmentLength.values()),
			_80
		);

		output.println("{");
		for (var type : MocapType.mwValues()) {
			output.printf("\"%s\": [", type);

			var skeletonMgmt = loadData(
				StretchedSequences.dataFiles(SKELETON).get(_1),
				SKELETON.dataClass());


			for (var combo : combos) {
				output.println("{");

				output.print("\"lengths\": [");
				combo.forEach(element -> output.printf("%s,", element));
				output.println("],");

				var skeletonPrecisions = doMulSegSearchWithSomeSegLens(
					MultipleSegmentationExperiments::refineResultsOnSkeletons,
					skeletonMgmt,
					type,
					combo
				)
					.values().stream()
					.sorted().toList();
				var idealPrecisions = doMulSegSearchWithSomeSegLens(
					MultipleSegmentationExperiments::refineResultsByCategories,
					skeletonMgmt,
					type,
					combo
				)
					.values().stream()
					.sorted().toList();

				var skeletonStatistics = BoxAndWhiskerCalculator.calculateBoxAndWhiskerStatistics(skeletonPrecisions.stream().toList());
				var idealStatistics = BoxAndWhiskerCalculator.calculateBoxAndWhiskerStatistics(idealPrecisions.stream().toList());

				output.printf("\"skeleton\": %s,%n", skeletonStatistics);
				output.printf("\"ideal\": %s%n", idealStatistics);

				output.println("},");
			}
			output.println("],");
		}
		output.println("}");

		output.flush();
	}

	/**
	 * This does a multi-segmentation search for the motion words of the given type.
	 * The given refinement function is used to refine the candidate sets.
	 *
	 * @param refinement how to refine the candidate sets
	 * @param skeletonMgmt a default skeleton dataset, so it doesn't have to be loaded each time
	 * @param type the type of motion words used to create the candidate sets
	 * @return a map from each query to its precision
	 * @throws IOException if the data can't be loaded
	 */
	static MyMap<LocalAbstractObject, Double> doMultiSegmentationSearch(
		BiFunction<ObjectMgmt, MyMap<LocalAbstractObject, Set<LocalAbstractObject>>, MyMap<LocalAbstractObject, Set<LocalAbstractObject>>> refinement,
		ObjectMgmt skeletonMgmt,
		MocapType type
	) throws IOException {
		return doMulSegSearchWithSomeSegLens(
			refinement,
			skeletonMgmt,
			type,
			Arrays.asList(SegmentLength.values())
		);
	}

	/**
	 * This does multi-segmentation search for the motion words
	 * of the given type for the given segment lengths.
	 * The given refinement function is used to refine the candidate sets.
	 *
	 * @param refinement how to refine the candidate sets
	 * @param skeletonMgmt a default skeleton dataset, so it doesn't have to be loaded each time
	 * @param type the type of motion words used to create the candidate sets
	 * @param lengths searching with which lengths of segments shall be used to create the candidate sets
	 * @return a map from each query to its precision
	 */
	static MyMap<LocalAbstractObject, Double> doMulSegSearchWithSomeSegLens(
		BiFunction<ObjectMgmt, MyMap<LocalAbstractObject, Set<LocalAbstractObject>>, MyMap<LocalAbstractObject, Set<LocalAbstractObject>>> refinement,
		ObjectMgmt skeletonMgmt,
		MocapType type,
		Collection<SegmentLength> lengths
	) {
		var mwKnnResults = lengths.stream().collect(MyMap.keyCollector(
			length -> {
				try {
					return segmentLengthKnnResults(type, length);
				} catch (IOException e) {
					throw new RuntimeException(e);
		}}));

		var transposedResultsWithUris = transposeResultsWithUris(
			mwKnnResults);

		var candidateSetsWithUris = transposedResultsWithUris
			.mapValues(
				length_Neighbor_Distance -> length_Neighbor_Distance
					.values().stream()
					.flatMap(
						map -> map.keySet().stream())
					.collect(Collectors.toSet()));

		var candidateSets = candidateSetsWithUris.map(
			skeletonMgmt::getObject,
			candidates -> candidates.stream()
				.map(skeletonMgmt::getObject)
				.collect(Collectors.toSet()));

		var refinedResults = refinement.apply(
			skeletonMgmt,
			candidateSets
		);

		var precisions = refinedResults.map(
			(q, r) -> q,
			(query, results) -> {
				var queryCategory = skeletonMgmt.getObjectCategories(query).get(0);
				var kOfKnn = results.size();
				return results.stream()
					.filter(result -> skeletonMgmt.getObjectCategories(result).contains(queryCategory))
					.count() / (double)kOfKnn;
			});
		return precisions;
	}

	/**
	 * This takes a candidate set for each query and refines each set
	 * so than only k sequences closest to the query according to searching on skeletons remain.
	 * k = size of the category - 1.
	 * 
	 * @param skeletonMgmt the dataset on which the searching shall be done
	 * @param candidateSets a map from each query to set of candidates to the closest neighbors.
	 * @return a map from each query to a set of k nearest neighbors according to DTW on skeletons
	 */
	static MyMap<LocalAbstractObject, Set<LocalAbstractObject>> refineResultsOnSkeletons(
		ObjectMgmt skeletonMgmt,
		MyMap<LocalAbstractObject, Set<LocalAbstractObject>> candidateSets
	) {
		var distanceCache = KnnDistanceCache.instance();
		return candidateSets.map(
			(q, c) -> q,
			(query, candidates) -> {
				var queryCategorySize = skeletonMgmt.getObjectCountInCategory(
					skeletonMgmt.getObjectCategories(query).get(0));
				return candidates.stream()
					.sorted(Comparator.comparing(candidate -> distanceCache.getDistance(candidate, query)))
					.limit(queryCategorySize - 1)
					.collect(Collectors.toSet());
		});
	}

	/**
	 * This takes a candidate set for each query and refines each set
	 * to k best matches. k = size of the category - 1.
	 *
	 * The distance is according to categories, so, if the sequences are
	 * in the same category, the distance is 0, and it's 1 otherwise.
	 *
	 * @param sequenceMgmt the dataset to determine categories
	 * @param candidateSets a map from each query to set of candidates to the closest neighbors.
	 * @return a map from each query to a set of k nearest neighbors according to the categories
	 */

	static MyMap<LocalAbstractObject, Set<LocalAbstractObject>> refineResultsByCategories(
		ObjectMgmt sequenceMgmt,
		MyMap<LocalAbstractObject, Set<LocalAbstractObject>> candidateSets
	) {
		return candidateSets.map(
			(q, c) -> q,
			(query, candidates) -> {
				var queryCategorySize = sequenceMgmt.getObjectCountInCategory(
					sequenceMgmt.getObjectCategories(query).get(0));
				return candidates.stream()
					.sorted(Comparator.comparing(
						candidate -> sequenceMgmt.getObjectCategories(candidate)
							.contains(
								sequenceMgmt
									.getObjectCategories(query)
									.get(0))
							? 0
							: 1))
					.limit(queryCategorySize - 1)
					.collect(Collectors.toSet());
			});
	}

	/**
	 * This retrieves kNN results for every sequence of a given type for all segment lengths.
	 *
	 * @param type the motion representation on which kNN search shall be done
	 * @return a map from each segment length to a map from each sequence to a map from each of its near neighbors to the distance
	 * @throws IOException if the data can't be loaded
	 */
	static MyMap<SegmentLength, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>> segmentLengthKnnResults(
		MocapType type
	) throws IOException {
		var mwKnnResults = new MyMap<SegmentLength, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>>();
		for (var length : SegmentLength.values()) {
			mwKnnResults.put(length, segmentLengthKnnResults(type, length));
		}
		return mwKnnResults;
	}

	static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> segmentLengthKnnResults(
		MocapType type,
		SegmentLength length
	) throws IOException {
		var sequenceMgmt = loadData(dataFiles(type).get(length), type.dataClass());
		var results = knnSearchResults(type, length, sequenceMgmt);
		var kLimitedResults = results.map(
			(q, n) -> q,
			(query, neighbors) -> neighbors.entrySet().stream()
				.sorted(Entry.comparingByValue())
				.limit(
					sequenceMgmt.getObjectCountInCategory(
						sequenceMgmt.getObjectCategories(query).get(0)
					) - 1)
				.collect(entryCollector()));
		return kLimitedResults;
	}

	/**
	 * This transposes the given data structure and converts its LocalAbstractObjects to their URIs.
	 *
	 * The transposition is from a map from segment lengths to a map from query objects
	 * to a map from the query objects to a map from segment lengths.
	 *
	 * @param mwKnnResults the data structure to be transposed
	 * @return the transposed data structure
	 */
	static MyMap<String, MyMap<SegmentLength, MyMap<String, Float>>> transposeResultsWithUris(
		MyMap<SegmentLength, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>> mwKnnResults
	) {
		var transposedResultsWithUris = new MyMap<String, MyMap<SegmentLength, MyMap<String, Float>>>();
		for (var entry : mwKnnResults.entrySet()) {
			var length = entry.getKey();
			for (var entry2 : entry.getValue().entrySet()) {
				var query = entry2.getKey();
				var results = entry2.getValue();

				var resultsWithUris = results.map(
					AbstractObject::getLocatorURI,
					d -> d);

				transposedResultsWithUris.putIfAbsent(query.getLocatorURI(), new MyMap<>());
				transposedResultsWithUris.get(query.getLocatorURI()).put(length, resultsWithUris);
			}
		}
		return transposedResultsWithUris;
	}

	static <A, B, C> MyMap<B, MyMap<A, C>> transposeMap(MyMap<A, MyMap<B, C>> original) {
		var transposed = new MyMap<B, MyMap<A, C>>();
		for (var entry : original.entrySet()) {
			var a = entry.getKey();
			for (var entry2 : entry.getValue().entrySet()) {
				var b = entry2.getKey();
				var c = entry2.getValue();

				transposed.putIfAbsent(b, new MyMap<>());
				transposed.get(b).put(a, c);
		} }
		return transposed;
	}

	public static MyMap<MocapType, MyMap<SegmentLength, MyMap<Category, List<Double>>>> getKnnPrecisions() {
		var knnPrecisions = new MyMap<MocapType, MyMap<SegmentLength, MyMap<Category, List<Double>>>>();

		for(var type : MocapType.mwValues()) {
			knnPrecisions.put(type, getKnnPrecisions(type));
		}

		return knnPrecisions;
	}

	public static MyMap<SegmentLength, MyMap<Category, List<Double>>> getKnnPrecisions(MocapType type) {
		var cacheDir = new File(Config.cacheDir(), "mulSegKnnPrecision");
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s", type.noun()));


		try {
			return readCache(cachePath, MyMap.class);
		} catch(CacheIOException _e) {
			var results = dataFiles(type)
				.map(
					(length, file) -> length,
					(length, file) -> {
						var sequenceMgmt = loadDataUnchecked(file, type.dataClass());
						var knnResults = knnSearchResults(type, length, sequenceMgmt);
						return computeKnnPrecisions(knnResults, sequenceMgmt);
				});

			try {
				writeCache(cachePath, results);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return results;
		}
	}

	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnSearchResults(MocapType type, SegmentLength length, ObjectMgmt sequenceMgmt) {
		var cacheDir = new File(Config.cacheDir(), "mulSegKnnResults " + length);
		return KnnSearchTools.knnSearchResultsWithCaching(type, sequenceMgmt, cacheDir);
	}

	static ObjectMgmt loadDataUnchecked(File file, Class<? extends LocalAbstractObject> dataClass) {
		try {
			return Utils.loadData(file, dataClass);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static MyMap<SegmentLength, File> dataFiles(MocapType type) {
		var segmentLengths = Arrays.asList(SegmentLength.values());
		var mwDir = new File(Config.dataDir(), "motion words");
		var mocapTypeWord = switch (type) {
			case SKELETON -> throw new IllegalArgumentException("Skeleton sequences aren't segmented.");
			case HARD_MW -> "hard";
			case SOFT_MW -> "soft";
			case MATCH_N_MW -> "multi-overlay";
		};

		return segmentLengths.stream().collect(MyMap.keyCollector(
			length -> new File(
				mwDir,
				mocapTypeWord
					+ "_motion_words_segment"
					+ length
					+ ".data"
		)));
	}
}
