package xyz.matj1.bp.enums;

/**
 * A representation of a relation, used for pairs of sequences
 *
 * DIFFERENCE – difference of the lengths
 * QUOTIENT – quotient of the lengths
 */
public enum LengthRelation {
	DIFFERENCE,
	QUOTIENT,
}
