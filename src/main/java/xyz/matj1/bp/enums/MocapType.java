package xyz.matj1.bp.enums;

import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import mcdr.sequence.impl.SequenceMotionWordsDTW;
import mcdr.sequence.impl.SequenceMotionWordsNMatchesDTW;
import mcdr.sequence.impl.SequenceMotionWordsSoftAssignmentDTW;
import messif.objects.LocalAbstractObject;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * This represents types of motion-capture data.
 * Each value has attributes for its name, adjective and class used to contain the data.
 */
public enum MocapType {
	SKELETON("skeletons", "skeleton", SequenceMocapPoseCoordsL2DTW.class),
	HARD_MW("hard MW", "hard-MW", SequenceMotionWordsDTW.class),
	SOFT_MW("soft MW", "soft-MW", SequenceMotionWordsSoftAssignmentDTW.class),
	MATCH_N_MW("multiple-overlay MW", "multiple-overlay-MW", SequenceMotionWordsNMatchesDTW.class),
	;

	private final String name;
	private final String adjective;
	private final Class<? extends LocalAbstractObject> dataClass;

	MocapType(String name, String adjective, Class<? extends LocalAbstractObject> dataClass) {
		this.name = name;
		this.adjective = adjective;
		this.dataClass = dataClass;
	}

	public static List<MocapType> mwValues() {
		return Arrays.stream(MocapType.values())
			.filter(Predicate.isEqual(SKELETON).negate())
			.toList();
	}

	/**
	 * @return the name of the type of motion-capture data
	 */
	public String noun() {
		return name;
	}

	/**
	 * @return the adjective used for motion-capture data of that type
	 */
	public String adjective() {
		return adjective;
	}

	/**
	 * @return the class used for containing a sequence of data of that type
	 */
	public Class<? extends LocalAbstractObject> dataClass() {
		return dataClass;
	}

	@Override
	public String toString() {
		return this.noun();
	}
}
