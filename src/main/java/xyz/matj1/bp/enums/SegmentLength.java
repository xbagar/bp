package xyz.matj1.bp.enums;

/**
 * The values of this signify lengths of segments used for generating motion words.
 */
public enum SegmentLength {
	_40(40),
	_80(80),
	_120(120),
	_160(160),
	;

	private final Integer asInteger;

	SegmentLength(Integer asInteger) {
		this.asInteger = asInteger;
	}

	/**
	 * This returns the length of the segment signified by the enum value.
	 *
	 * @return the length of the segment
	 */
	public Integer asInteger() {
		return this.asInteger;
	}

	@Override
	public String toString() {
		return this.asInteger.toString();
	}

	/**
	 * This returns the default length of segments
	 * used for motion words in the majority of my bachelor's thesis.
	 *
	 * @return the default length of segments
	 */
	public static SegmentLength defaultLength() {
		return _80;
	}
}
