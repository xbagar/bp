package xyz.matj1.bp.enums;

/**
 * The constants of this represent multiples of speeds of motion-capture data used in this project.
 */
public enum SpeedMultiple {
	_1_4(1/4d),
	_1_2(1/2d),
	_2_3(2/3d),
	_1(1d),
	_3_2(3/2d),
	_2(2d),
	_3(3d),
//	_4(4d),
	_5(5d),
	;

	private final Double asDouble;

	SpeedMultiple(Double asDouble) {
		this.asDouble = asDouble;
	}

	/**
	 * @return the value represented by the constant
	 */
	public Double asDouble() {
		return asDouble;
	}

	/**
	 * This rounds the value to 2 decimal places and converts it to a string
	 * in a way that there are no trailing zeroes and “.” is used as the decimal separator.
	 *
	 * @return a text representation of the value represented by the constant
	 */
	public String asString() {
		return displayDouble(this.asDouble, 2);
	}

	@Override
	public String toString() {
		return this.asString();
	}

	/**
	 * This converts a Double to its string representation with <code>decimals</code> decimal places
	 * and ‘.’ as the separator of the whole part and the fractional part.
	 *
	 * @param input what shall be represented
	 * @param decimals decimal places of the representation
	 * @return a String representation of the given Double
	 */
	static String displayDouble(Double input, int decimals) {
		double multiplier = Math.pow(10, decimals);
		return ((Double)(Math.round(input * multiplier) / multiplier)) // round to `decimals` decimal places
			.toString().replaceAll("\\.?0*$", ""); // strip maybe ‘.’ and ‘0’s from the end
	}
}
