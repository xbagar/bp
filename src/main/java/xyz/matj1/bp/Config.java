package xyz.matj1.bp;

import xyz.matj1.bp.types.Vector2;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
	private static final Properties properties = properties();

	public static Theme graphTheme() {
		if (properties.getProperty("graphTheme", "").equals("dark")) {
			return Theme.DARK;
		} else {
			return Theme.LIGHT;
	}}

	public static File outputDir() {
		var outputDir = properties.getProperty("outputDir");
		return new File(outputDir == null ? "output" : outputDir);
	}

	public static File cacheDir() {
		var cacheDir = properties.getProperty("cacheDir");
		return new File(cacheDir == null ? "cache" : cacheDir);
	}

	public static File dataDir() {
		var dataDir = properties.getProperty("dataDir");
		return new File(dataDir == null ? "data" : dataDir);
	}

	public static Font graphFont() {
		var fontName = properties.getProperty("fontName");
		return new Font(fontName, Font.PLAIN, 0);
	}

	public static Vector2<Integer> graphDimensions() {
		var width = properties().getProperty("graphWidth");
		var height = properties().getProperty("graphHeight");
		try {
			return new Vector2<>(Integer.parseInt(width), Integer.parseInt(height));
		} catch (NumberFormatException e) {
			throw new ConfigException("A value of a property in {graphWidth, graphHeight} can't be parsed as an integer.", e);
		}
	}

	private static Properties properties() {
		var properties = new Properties();
		try (var propsFile = new FileInputStream("resources/config.properties")) {
			properties.load(propsFile);
		} catch (IOException e) {
			System.err.println("The configuration file wasn't loaded because of " + e);
		}

		return properties;
	}

	public enum Theme {
		LIGHT,
		DARK,
	}

	static class ConfigException extends RuntimeException {
		public ConfigException() {
			super();
		}

		public ConfigException(String message) {
			super(message);
		}

		public ConfigException(String message, Throwable cause) {
			super(message, cause);
		}

		public ConfigException(Throwable cause) {
			super(cause);
		}
	}
}
