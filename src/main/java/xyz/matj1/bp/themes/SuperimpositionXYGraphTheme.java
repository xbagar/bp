package xyz.matj1.bp.themes;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import java.awt.Color;

public class SuperimpositionXYGraphTheme extends XyGraphTheme {
	private static final SuperimpositionXYGraphTheme instance = new SuperimpositionXYGraphTheme();

	protected SuperimpositionXYGraphTheme() {}

	@Override
	public void apply(JFreeChart chart) {
		super.apply(chart);
		chart.getXYPlot().setRenderer(new XYLineAndShapeRenderer(true, false));

		var renderer = chart.getXYPlot().getRenderer();
		for (int i = 0; i < chart.getXYPlot().getDatasetCount(); i++) {
			renderer.setSeriesPaint(i, new Color(1f, 0f, 0f, 0.01f));
		}
	}

	public static SuperimpositionXYGraphTheme instance() {
		return instance;
	}
}
