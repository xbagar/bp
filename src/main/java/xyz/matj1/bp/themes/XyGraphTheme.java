package xyz.matj1.bp.themes;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.ui.RectangleInsets;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.Config.Theme;

import java.awt.Color;
import java.awt.Font;

public class XyGraphTheme implements ChartTheme {
	private static final XyGraphTheme instance = new XyGraphTheme();

	protected XyGraphTheme() {}

	@Override
	public void apply(JFreeChart chart) {
		var theme = (StandardChartTheme)((
			Config.graphTheme().equals(Theme.DARK)
				? StandardChartTheme.createDarknessTheme()
				: StandardChartTheme.createJFreeTheme()
		));
		theme.apply(chart);

		if (Config.graphTheme().equals(Theme.LIGHT)) {
			chart.getPlot().setBackgroundPaint(Color.white);
		}

		setFonts(Config.graphFont(), theme);

		chart.setPadding(RectangleInsets.ZERO_INSETS);
		chart.setBorderVisible(false);
	}

	/**
	 * This sets the font family of <code>theme</code>
	 * to the one of <code>font</code>.
	 * It keeps the original font styles and sizes.
	 *
	 * This changes the original style and returns it too.
	 *
	 * @param font the font to set
	 * @param theme in which theme the font shall be set
	 * @return the given theme
	 */
	public static StandardChartTheme setFonts(Font font, StandardChartTheme theme) {
		var oldExtraLargeFont = theme.getExtraLargeFont();
		var oldLargeFont = theme.getLargeFont();
		var oldRegularFont = theme.getRegularFont();
		var oldSmallFont = theme.getSmallFont();

		var extraLargeFont = font.deriveFont(oldExtraLargeFont.getStyle(), oldExtraLargeFont.getSize());
		var largeFont = font.deriveFont(oldLargeFont.getStyle(), oldLargeFont.getSize());
		var regularFont = font.deriveFont(oldRegularFont.getStyle(), oldRegularFont.getSize());
		var smallFont = font.deriveFont(oldSmallFont.getStyle(), oldSmallFont.getSize());

		theme.setExtraLargeFont(extraLargeFont);
		theme.setLargeFont(largeFont);
		theme.setRegularFont(regularFont);
		theme.setSmallFont(smallFont);

		return theme;
	}

	public static XyGraphTheme instance() {
		return instance;
	}
}
