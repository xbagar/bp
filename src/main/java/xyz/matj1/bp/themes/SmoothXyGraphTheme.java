package xyz.matj1.bp.themes;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

public class SmoothXyGraphTheme extends XyGraphTheme {
	private static final SmoothXyGraphTheme instance = new SmoothXyGraphTheme();

	protected SmoothXyGraphTheme() {}

	@Override
	public void apply(JFreeChart chart) {
		super.apply(chart);
		chart.getXYPlot().setRenderer(new XYLineAndShapeRenderer(true, false));
	}

	public static SmoothXyGraphTheme instance() {
		return instance;
	}
}
