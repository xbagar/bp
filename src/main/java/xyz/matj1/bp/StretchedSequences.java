package xyz.matj1.bp;

import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.xy.DefaultXYDataset;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.experiments.KnnPrecision;
import xyz.matj1.bp.experiments.KnnSearchTools;
import xyz.matj1.bp.types.Category_Multiple_Precision;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.PointSet;
import xyz.matj1.bp.types.Vector2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.regex.PatternSyntaxException;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static xyz.matj1.bp.Utils.average;
import static xyz.matj1.bp.Utils.loadData;
import static xyz.matj1.bp.Utils.readCache;
import static xyz.matj1.bp.Utils.writeCache;
import static xyz.matj1.bp.Utils.writeXYGraphAsPdf;
import static xyz.matj1.bp.enums.SpeedMultiple._1;

/**
 * A class for testing of how motion-word sequences
 * can be matched on artificially stretched sequences
 *
 * @author Matěj Bagar
 */
public class StretchedSequences {
	/**
	 * This runs tests on stretched sequences.
	 *
	 * @param args command-line arguments (ignored)
	 * @throws IOException if the data can't be loaded
	 */
	public static void main(String[] args) throws IOException {
		knnTest(new OutputStreamWriter(System.out));
		knnTestShow(new File(Config.outputDir(), "stretch kNN averages"));
		knnTestUnified(new OutputStreamWriter(System.out));
		knnTestUnified(new OutputStreamWriter(System.out), List.of(MocapType.HARD_MW, MocapType.SOFT_MW), Arrays.asList(SpeedMultiple.values()));
		knnTestUnifiedShow(Path.of(Config.outputDir().getPath(), "stretch kNN averages", "unified graph.pdf").toFile());
		knnTestUnifiedShowBox(Path.of(Config.outputDir().getPath(), "stretch kNN averages", "unified box graph.pdf").toFile());

		stretchingCheckWrite(new FileWriter(new File(Config.outputDir(), "stretchingCheckWrite")));
		stretchingCheckAverageWrite(new OutputStreamWriter(System.out));
		stretchingCheckAverageShow(new File(Config.outputDir(), "stretch check averages"));

		averageKnnDistancesPrint(10, new PrintWriter(System.out));
		averageKnnDistancesShowBox(10, new File(Config.outputDir(), "kNN distances box"));

		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types"), categoryOrderingByPrecision());
		knnCompareMocapTypesPrint(new PrintWriter(System.out), categoryOrderingByPrecision());
		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types ordered by length variance"), categoryOrderingByLengthVariance());
		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types ordered by length log variance"), categoryOrderingByLengthLogVariance());
		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types ordered by precision in bins by length log variance"), categoryOrderingByPrecisionInBinsByLengthLogVariance(5));
		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types ordered by precision in bins by category size"), categoryOrderingByPrecisionInBinsByCategorySize(5));
		knnCompareMocapTypesShow(new File(Config.outputDir(), "kNN comparison of mocap types ordered by relative precision"), categoryOrderingByRelativePrecision());
		knnCompareMocapTypesPrint(new PrintWriter(System.out), categoryOrderingByRelativePrecision());

		categoryOrderingPrint(new PrintWriter(System.out), categoryOrderingByLengthVariance());
		categoryOrderingPrint(new PrintWriter(System.out), categoryOrderingByPrecision());
		categoryOrderingPrint(new PrintWriter(System.out), categoryOrderingByLengthLogVariance());
	}

	/**
	 * This computes the average precision for each category, multiple, type.
	 *
	 * @param output where the output shall be written
	 * @throws IOException if the data can't be loaded
	 */
	private static void knnTest(Writer output) throws IOException {
		var knnMeasurements = getKnnMeasurements();

		for (var type : MocapType.values()) {
			output.write(String.format("%s:%n", type));
			for (var multiple : SpeedMultiple.values()) {
				output.write(String.format("%s:%n", multiple.asString()));
				Utils.write(
					KnnPrecision.averageFormat(knnPrecisionAverage(
						knnMeasurements.get(type).get(multiple)
					)),
					output
				);
			}
			output.write(System.lineSeparator());
	}}

	/**
	 * This computes the average precision for each category, multiple, type, and plots it to graphs.
	 *
	 * @param outputDir where the output files shall be put
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestShow(File outputDir) throws IOException {
		outputDir.mkdirs();
		var knnMeasurements = getKnnMeasurements();

		for (var type : MocapType.values()) {
			var precisions = new Category_Multiple_Precision();
			for (var multiple : SpeedMultiple.values()) {
				var category_List = knnMeasurements.get(type).get(multiple);

				for (var category : category_List.keySet()) {
					precisions.addPrecision(category, multiple, average(category_List.get(category)));
				}
			}

			var dataset = new DefaultCategoryDataset();
			var sortedCategoryEntries = precisions.entrySet().stream()
				.sorted(Comparator.comparing(e -> e.getValue().get(_1)))
				.toList();

			for (var entry : sortedCategoryEntries) {
				for (var multiple : SpeedMultiple.values()) {
					dataset.addValue(entry.getValue().get(multiple), multiple, entry.getKey().id);
				}
			}

			var graph = ChartFactory.createLineChart(
				String.format("Average kNN precision of each sequence in each multiple among the original sequences as %s", type),
				"category",
				"average precision",
				dataset,
				PlotOrientation.VERTICAL,
				true, true, true
			);
			writeXYGraphAsPdf(
				graph,
				new File(outputDir, String.format("%s.pdf", type)),
				new Vector2<>(1024, 512)
			);
		}
	}

	/**
	 * This outputs the average kNN precision for each speed multiple for each mocap type
	 * to the specified writer.
	 *
	 * @param output where the output shall be written
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestUnified(Writer output) throws IOException {
		var knnMeasurements = getKnnMeasurements();

		for (var type : MocapType.values()) {
			for (var multiple : SpeedMultiple.values()) {
				var category_List = knnMeasurements.get(type).get(multiple);
				var average = knnMeasurementsAverage(category_List);

				output.write(String.format(
					"%s, %s×: %s%n",
					type, multiple, average
				));
				output.flush();
			}
		}
	}

	/**
	 * This outputs the average kNN precision for each speed multiple for each mocap type
	 * to the specified writer.
	 *
	 * @param output where the result shall be written
	 * @param types which mocap types shall be included
	 * @param multiples which speed multiples shall be included
	 * @throws IOException if the input data can't be loaded
	 */
	private static void knnTestUnified(
		 OutputStreamWriter output,
		 List<MocapType> types,
		 List<SpeedMultiple> multiples
	) throws IOException {
		for (var type : types) {
			for (var multiple : multiples) {
				var knnMeasurements = getKnnMeasurements(type, multiple);
				
				DoubleStream flatValues = knnMeasurements.values().stream()
					.flatMapToDouble(
						x -> x.stream().mapToDouble(a -> a).filter(Double::isFinite)
					);
				var average = flatValues.average().orElseThrow();

				output.write(String.format(
					"%s, %s×: %s%n",
					type, multiple, average
				));
				output.flush();
			}
		}
	}

	/**
	 * This shows a bar graph with the average kNN precision for each speed multiple for each mocap type.
	 *
	 * @param outputFile where the output shall be written
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestUnifiedShow(File outputFile) throws IOException {
		outputFile.getParentFile().mkdirs();
		var knnMeasurements = getKnnMeasurements();
		var dataset = new DefaultCategoryDataset();

		for (var type : MocapType.values()) {
			for (var multiple : SpeedMultiple.values()) {
				var category_List = knnMeasurements.get(type).get(multiple);
				double average = knnMeasurementsAverage(category_List);

				dataset.addValue(average, multiple, type);
			}
		}
		var graph = ChartFactory.createBarChart(
			"Average kNN precision for each speed multiple among the original sequences for each mocap type",
			"type",
			"average kNN precision",
			dataset,
			PlotOrientation.VERTICAL,
			true, true, true
		);
		writeXYGraphAsPdf(
			graph,
			outputFile,
			new Vector2<>(1024, 512)
		);
	}

	/**
	 * This is to calculate the average of precision in kNN measurements
	 * for a specific type and multiple.
	 * The result is calculated as the average of the flattened input map.
	 *
	 * @param category_List a map from categories to lists of kNN precisions in each category
	 * @return the average precision of the measurements
	 */
	private static double knnMeasurementsAverage(MyMap<Category, List<Double>> category_List) {
		DoubleStream flatValues = category_List.values().stream()
			.flatMapToDouble(
				x -> x.stream().mapToDouble(a -> a)
			);
		return flatValues.average().orElseThrow();
	}


	/**
	 * This shows a box graph for each speed multiple for each mocap type.
	 *
	 * @param outputFile where the output shall be written
	 * @throws IOException if the reading or writing fails
	 */
	private static void knnTestUnifiedShowBox(File outputFile) throws IOException {
		outputFile.getParentFile().mkdirs();
		var knnMeasurements = getKnnMeasurements();
		var dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (var type : MocapType.values()) {
			for (var multiple : SpeedMultiple.values()) {
				var category_List = knnMeasurements.get(type).get(multiple);
				var flatValues = category_List.values().stream()
					.flatMap(Collection::stream)
					.toList();

				dataset.add(flatValues, multiple, type);
			}
		}
		var graph = ChartFactory.createBoxAndWhiskerChart(
//			"Average kNN precision for each speed multiple among the original sequences for each mocap type",
			"",
			"type",
			"precision of kNN search",
			dataset,
			true
		);
		writeXYGraphAsPdf(
			graph,
			outputFile,
			Config.graphDimensions()
		);

	}

	/**
	 * This computes the distance from each sequence to its original sequence.
	 * The distances are indexed by mocap type, speed multiple, then by category.
	 *
	 * @return the distance to it's original sequence
	 * @throws IOException if the data can't be loaded
	 */
	private static MyMap<MocapType, MyMap<SpeedMultiple, MyMap<Category, List<Double>>>> stretchingCheck() throws IOException {
		var cacheFile = new File(Config.cacheDir(), "stretchingCheck");
		try {
			return readCache(cacheFile, MyMap.class);
		} catch (CacheIOException _e) {
			var result = new MyMap<MocapType, MyMap<SpeedMultiple, MyMap<Category, List<Double>>>>();

			for (var type : MocapType.values()) {
				result.put(type, new MyMap<>());
				var typeMap = result.get(type);

				var category_OriginalSequences = loadData(dataFiles(type).get(_1), type.dataClass()).getCategoryObjects();
				for (var multiple : SpeedMultiple.values()) {
					typeMap.put(multiple, new MyMap<>());
					var multipleMap = typeMap.get(multiple);

					var stretchedSequenceMgmt = loadData(dataFiles(type).get(multiple), type.dataClass());
					var category_StretchedSequences = stretchedSequenceMgmt.getCategoryObjects();

					stretchedSequenceMgmt.getCategories().parallelStream().forEach(category -> {
						multipleMap.put(category, new ArrayList<>());
						var categoryList = multipleMap.get(category);

						category_StretchedSequences.get(category).parallelStream()
							.forEach(sequence -> {
								var originalSequence = category_OriginalSequences.get(category)
									.stream().filter(uriMatches(sequence)).findAny().orElseThrow();
								var distance = sequence.getDistance(originalSequence);
								categoryList.add((double)distance);
							});
					});
			}}

			try {
				writeCache(cacheFile, result);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cacheFile);
				e.printStackTrace();
			}

			return result;
		}
	}

	/**
	 * This is to check that every stretched sequence
	 * has a very small distance to its original sequence.
	 * This writes the length-compensed distance from each sequence to its original sequence
	 * to <code>output</code>.
	 *
	 * @param output where to write the results
	 * @throws IOException if loading the data fails
	 */
	private static void stretchingCheckWrite(Writer output) throws IOException {
		var result = stretchingCheck();

		for (var type : result.keySet()) {
			output.write(String.format("%s:%n", type));
			var multiple_Map = result.get(type);
			for (var multiple : multiple_Map.keySet()) {
				output.write(String.format("%s:%n", multiple));
				var category_Map = multiple_Map.get(multiple);
				for (var category : category_Map.keySet()) {
					output.write(String.format("Category: %s:%n", category.id));
					var distanceList = category_Map.get(category);
					for (var distance : distanceList) {
						output.write(String.format("%s%n", distance));
		}}}}
		output.flush();
		System.err.printf("Stretch checking results were written to %s.%n", output);
	}

	/**
	 * This is to check that every stretched sequence
	 * has a very small distance to its original sequence.
	 * This writes the length-compensed distance from each sequence to its original sequence
	 * to <code>output</code>.
	 *
	 * @param output where to write the results
	 * @throws IOException if loading the data fails
	 */
	private static void stretchingCheckAverageWrite(Writer output) throws IOException {
		var result = stretchingCheck();

		for (var type : result.keySet()) {
			output.write(String.format("%s:%n", type));
			var multiple_Map = result.get(type);
			for (var multiple : multiple_Map.keySet()) {
				output.write(String.format("%s:%n", multiple));
				var category_Map = multiple_Map.get(multiple);
				for (var category : category_Map.keySet()) {
					var averageDistance = average(category_Map.get(category));
					output.write(String.format("Category %s: %s%n", category.id, averageDistance));
		}}}
		output.flush();
		System.err.printf("Stretch checking averages were written to %s.%n", output);
	}

	/**
	 * This is to check that every stretched sequence
	 * has a very small distance to its original sequence.
	 *
	 * @param outputDir where to write the results
	 * @throws IOException if loading the data or writing the results fails
	 */
	private static void stretchingCheckAverageShow(File outputDir) throws IOException {
		outputDir.mkdirs();
		var result = stretchingCheck();

		for (var type : result.keySet()) {
			var multiple_Map = result.get(type);
			for (var multiple : multiple_Map.keySet()) {
				var category_List = multiple_Map.get(multiple);
				var pointSet = new PointSet();

				for (var category : category_List.keySet()) {
					var averageDistance = average(category_List.get(category));
					pointSet.add(Double.parseDouble(category.id), averageDistance);
				}

				var dataset = new DefaultXYDataset();
				dataset.addSeries(0, pointSet.toDataSeries());
				var graph = ChartFactory.createScatterPlot(
					String.format("Average distance of each sequence stretched %s× to its original sequence as %s", multiple, type),
					"category",
					"average distance",
					 dataset,
					PlotOrientation.VERTICAL,
					false, true, true
				);
				writeXYGraphAsPdf(
					graph,
					new File(outputDir, String.format("%s – %s×.pdf", type, multiple)),
					new Vector2<>(1024, 512)
				);
	}}}

	/**
	 * This prints the average distances to the found neighbors in kNN search
	 * for every speed multiple for every mocap type.
	 *
	 * This uses a fixed k in the kNN, not variable like in knnTest.
	 *
	 * @param output where the output shall be written
	 * @param k how many neighbors shall be found in the kNN search, at least 1 and at most 20
	 * @throws IOException if the data for kNN search can't be loaded
	 */
	private static void averageKnnDistancesPrint(
		Integer k,
		PrintWriter output
	) throws IOException {
		var distances = averageKnnDistances(k);

		for (var type : distances.keySet()) {
			var multiple_AverageDistance = distances.get(type);
			output.println(type);
			for (var multiple : multiple_AverageDistance.keySet()) {
				var averageDistance = multiple_AverageDistance.get(multiple);
				output.printf("%s:\t%s%n", multiple, averageDistance);
				output.println();
			}
		}
		output.flush();
	}

	/**
	 * This creates box graphs of the distances to the found neighbors in kNN search
	 * for every speed multiple for every mocap type.
	 * One graph is for only skeletons, The other graph is for all motion words.
	 * That is because the distances for motion words are much smaller than those for skeletons.
	 *
	 * This uses a fixed k in the kNN, not variable like in knnTest.
	 *
	 * @param outputDir directory where the output shall be written
	 * @param k how many neighbors shall be found in the kNN search, at least 1 and at most 20
	 * @throws IOException if the data for kNN search can't be loaded
	 */
	private static void averageKnnDistancesShowBox(
		Integer k,
		File outputDir
	) throws IOException {
		outputDir.mkdirs();
		var knnResults = KnnSearchTools.stretchKnnResults();
		var mwDataset = new DefaultBoxAndWhiskerCategoryDataset();
		var skeletonDataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (var type : knnResults.keySet()) {
			var multiple_AverageDistance = knnResults.get(type);
			for (var multiple : multiple_AverageDistance.keySet().stream().sorted().toList()) {
				var query_Results = multiple_AverageDistance.get(multiple);

				var flatDistances = query_Results
					.values().stream()
					.flatMap(map -> map.values().stream().sorted().limit(k))
					.map(fl -> fl == 0 ? LogarithmicAxis.SMALL_LOG_VALUE : fl)
					.toList();

				var dataset = type.equals(MocapType.SKELETON)
					? skeletonDataset
					: mwDataset;

				dataset.add(flatDistances, multiple, type);
			}
		}

		var skeletonGraph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"",
			skeletonDataset,
			true
		);
		var mwGraph = ChartFactory.createBoxAndWhiskerChart(
			"",
			"type",
			"",
			mwDataset,
			true
		);

		var graphHeight = Config.graphDimensions().b();
		var graphWidth = Config.graphDimensions().a();
		writeXYGraphAsPdf(skeletonGraph, new File(outputDir, "skeletons.pdf"), new Vector2<>(graphWidth/4, graphHeight));
		writeXYGraphAsPdf(mwGraph, new File(outputDir, "motion words.pdf"), new Vector2<>(graphWidth*3/4, graphHeight));
	}

		/**
		 * This is like knnTest, but it returns average distances instead of precisions.
		 * For every mocap type, for every speed multiple, this computes the average distance
		 * in the of the neighbors found in the kNN search.
		 *
		 * This uses a fixed k in the kNN, not variable like in knnTest.
		 *
		 * @param k how many neighbors shall be found in the kNN search, at least 1 and at most 20
		 * @return for each mocap type, for each speed multiple, the average distance to the neighbors
		 * @throws IOException if the data for kNN search can't be loaded
		 */
	private static MyMap<MocapType, MyMap<SpeedMultiple, Double>> averageKnnDistances(
		Integer k
	) throws IOException {
		if (k < 1) {
			throw new IllegalArgumentException("k should be at least 1.");
		}

		var knnResults = KnnSearchTools.stretchKnnResults();

		var result = knnResults.entrySet().stream().map(entry -> {
			var type = entry.getKey();
			var multiple_Queries = entry.getValue();

			var multiple_AverageDistance = multiple_Queries.entrySet().stream().map(entry2 -> {
				var multiple = entry2.getKey();
				var query_Neighbors = entry2.getValue();

				var averageDistance = query_Neighbors.values().stream().mapToDouble(
					distanceMap -> distanceMap.values().stream()
						.collect(Utils.smallestK(k, Comparator.naturalOrder())).stream()
						.mapToDouble(x -> x)
						.average().orElseThrow()
				).average().orElseThrow();

				return Map.entry(multiple, averageDistance);
			}).collect(Utils.entryCollector());
			return Map.entry(type, multiple_AverageDistance);
		}).collect(Utils.entryCollector());

		return result;
	}

	/**
	 * This sets ordering of categories by precision of kNN search on skeletons
	 * and always orders categories according to that.
	 *
	 * This creates graphs, one for each speed multiple,
	 * where each category has kNN precision for each mocap type shown.
	 * Every mocap type has its own data series; they are superimposed.
	 *
	 * @param outputDirectory where the graphs shall be put
	 * @throws IOException if the data can't be loaded
	 */
	private static void knnCompareMocapTypesShow(File outputDirectory, List<Category> categoryOrdering) throws IOException {
		outputDirectory.mkdirs();

		var data = knnCompareMocapTypes(categoryOrdering);

		data.forEach((multiple, category_Types) -> {
			var dataset = new DefaultCategoryDataset();
			category_Types.forEach(
				(category, type_Precision) -> type_Precision.entrySet().stream().sorted(Entry.comparingByKey()).forEach(
					entry -> {
						var type = entry.getKey();
						var precision = entry.getValue();
						if (!precision.isNaN()) {
							dataset.addValue(precision, type, category.id);
			} } ) );

			var graph = ChartFactory.createLineChart(
				"",
				"category",
				"precision",
				dataset
			);

			Utils.writeXYGraphAsPdf(
				graph,
				new File(outputDirectory, String.format("%s×.pdf", multiple)),
				Config.graphDimensions()
			);
		});
	}

	/**
	 * This sets ordering of categories by precision of kNN search on skeletons
	 * and always orders categories according to that.
	 *
	 * This prints tables of precisions, one for each speed multiple,
	 * where each category has kNN precision for each mocap type shown.
	 *
	 * @param output where the output shall be printed
	 * @throws IOException if the data can't be loaded
	 */
	private static void knnCompareMocapTypesPrint(PrintWriter output, List<Category> categoryOrdering) throws IOException {
		var data = knnCompareMocapTypes(categoryOrdering);

		data.forEach((multiple, category_Types) -> {
			output.println();
			output.println(multiple);
			output.println();
			output.printf(
				"category\t%s\t%s\t%s\t%s%n",
				MocapType.SKELETON,
				MocapType.HARD_MW,
				MocapType.SOFT_MW,
				MocapType.MATCH_N_MW
			);
			category_Types.forEach((category, type_Precision) -> output.printf(
				"%s\t%s\t%s\t%s\t%s\t%n",
				category.id,
				type_Precision.get(MocapType.SKELETON),
				type_Precision.get(MocapType.HARD_MW),
				type_Precision.get(MocapType.SOFT_MW),
				type_Precision.get(MocapType.MATCH_N_MW)
			));
		});

		output.flush();
	}

	/**
	 * This sets ordering of categories by precision of kNN search on skeletons
	 * and always orders categories according to that.
	 *
	 * This returns a data structure, where, for each speed multiple,
	 * each category has kNN precision for each mocap type.
	 *
	 * All maps in the data structure are MyMap
	 * except for the one with Category as the type of keys;
	 * that is LinkedHashMap, because it preserves the insertion order,
	 * and categories need to be sorted.
	 *
	 *
	 * @param categoryOrdering a list of all categories determining the ordering of categories in the output
	 * @throws IOException if the data can't be loaded
	 * @return nested maps with data about precisions
	 */
	private static MyMap<SpeedMultiple, LinkedHashMap<Category, MyMap<MocapType, Double>>> knnCompareMocapTypes(List<Category> categoryOrdering) throws IOException {
		var knnPrecisions = StretchedSequences.getKnnMeasurements();

		var result = new MyMap<SpeedMultiple, LinkedHashMap<Category, MyMap<MocapType, Double>>>();

		class Category_Type_Precision extends MyMap<Category, MyMap<MocapType, Double>> {
			public Category_Type_Precision putPrecision(Category category, MocapType type, Double precision) {
				super.putIfAbsent(category, new MyMap<>());
				super.get(category).put(type, precision);
				return this;
		} }

		for (SpeedMultiple multiple : SpeedMultiple.values()) {
			var precisions = knnPrecisions.mapValues(v -> v.get(multiple));
			var category_Type_Precision = new Category_Type_Precision();
			precisions.forEach((type, category_Precisions) -> {
				for (var entry2 : category_Precisions.entrySet()) {
					var category = entry2.getKey();
					var averagePrecision = entry2.getValue().stream().mapToDouble(x -> x).average().orElseThrow();
					category_Type_Precision.putPrecision(category, type, averagePrecision);
				}
			});

			result.put(
				multiple,
				category_Type_Precision.entrySet().stream()
					.sorted(Comparator.comparingInt(a -> categoryOrdering.indexOf(a.getKey()))) // ordering by the index in categoryOrdering of the category
					.collect(Utils.entryCollectorToLinkedHashMap())
			);
		}

		return result;
	}

	/**
	 * This orders categories by precision on multiple 1 of skeletons.
	 *
	 * @return a list of all categories ordered by precision on multiple 1 of skeletons
	 * @throws IOException if the input data can't be loaded
	 */
	static List<Category> categoryOrderingByPrecision() throws IOException {
		var knnPrecisions = StretchedSequences.getKnnMeasurements();

		var originalPrecisions = knnPrecisions.mapValues(v -> v.get(_1));
		var skeletonCategory_Precisions = originalPrecisions.get(MocapType.SKELETON);
		var skeletonCategory_AveragePrecision = skeletonCategory_Precisions
			.mapValues(
				list -> list.stream()
					.mapToDouble(x -> x)
					.average().orElseThrow()
			);

		var categoryOrdering = skeletonCategory_AveragePrecision
			.entrySet().stream()
			.sorted(Entry.comparingByValue())
			.map(Entry::getKey).toList();

		return categoryOrdering;
	}

	/**
	 * This orders categories by the variance of the lengths of the sequences.
	 *
	 * @return a list of all categories ordered the variance of the lengths of the sequences
	 * @throws IOException if the data with categories can't be loaded
	 */
	static List<Category> categoryOrderingByLengthVariance() throws IOException {
		var objectMgmt = loadData(dataFiles(MocapType.SKELETON).get(_1), MocapType.SKELETON.dataClass());

		var category_Objects = new MyMap<>(objectMgmt.getCategoryObjects());
		var category_Lengths = category_Objects.mapValues(
			objects -> objects.stream().map(
				Utils::sequenceLength
			).toList()
		);
		var category_Variance = category_Lengths.mapValues(Utils::variance);
		var categoryOrdering = category_Variance.entrySet().stream()
			.sorted(Entry.comparingByValue())
			.map(Entry::getKey)
			.toList();

		return categoryOrdering;
	}

	/**
	 * This orders categories by the variance of the logarithms of lengths of the sequences.
	 *
	 * @return a list of all categories ordered the variance of the lengths of the sequences
	 * @throws IOException if the data with categories can't be loaded
	 */
	static List<Category> categoryOrderingByLengthLogVariance() throws IOException {
		var objectMgmt = loadData(dataFiles(MocapType.SKELETON).get(_1), MocapType.SKELETON.dataClass());

		var category_Objects = new MyMap<>(objectMgmt.getCategoryObjects());
		var category_LengthLogs = category_Objects.mapValues(
			objects -> objects.stream()
				.map(Utils::sequenceLength)
				.map(Math::log)
				.toList()
		);
		var category_Variance = category_LengthLogs.mapValues(Utils::variance);

		var categoryOrdering = category_Variance.entrySet().stream()
			.sorted(Entry.comparingByValue())
			.map(Entry::getKey)
			.toList();

		return categoryOrdering;
	}

	/**
	 * This sorts categories by the variance of the logarithms of lengths of the sequences to bins of equal size.
	 * Then, it orders the categories in each bin by the precision on original skeletons.
	 *
	 * @param binCount to how many bins the categories shall be sorted
	 * @return a list of all categories ordered the variance of the lengths of the sequences
	 * @throws IOException if the data with categories can't be loaded
	 */
	static List<Category> categoryOrderingByPrecisionInBinsByLengthLogVariance(Integer binCount) throws IOException {
		var categoryVarianceOrder = categoryOrderingByLengthLogVariance();
		var categoryPrecisionOrder = categoryOrderingByPrecision();

		var category_BinByVariance = new MyMap<Category, Integer>() {{
			var binStep = categoryVarianceOrder.size() / binCount;
			for (int i = 0; i < categoryVarianceOrder.size(); i++) {
				this.put(categoryVarianceOrder.get(i), i/binStep);
		} }};
		// Each category has assigned a bin of variance.
		// All categories with a lower bin have lower variance than all categories with a higher bin.

		return categoryPrecisionOrder.stream()
			.sorted(Comparator.comparingInt(category_BinByVariance::get)).toList();
	}

	/**
	 * This sorts categories by the variance of the logarithms of lengths of the sequences to bins of equal size.
	 * Then, it orders the categories in each bin by the precision on original skeletons.
	 *
	 * @param binCount to how many bins the categories shall be sorted
	 * @return a list of all categories ordered the variance of the lengths of the sequences
	 * @throws IOException if the data with categories can't be loaded
	 */
	static List<Category> categoryOrderingByPrecisionInBinsByCategorySize(Integer binCount) throws IOException {
		var categoryPrecisionOrder = categoryOrderingByPrecision();
		var categorySizeOrder = new MyMap<>(
			loadData(dataFiles(MocapType.HARD_MW).get(_1), MocapType.HARD_MW.dataClass())
				.getCategoryObjects()
		)
			.mapValues(List::size)
			.entrySet().stream()
			.sorted(Entry.comparingByValue())
			.map(Entry::getKey)
			.toList();

		var category_BinBySize = new MyMap<Category, Integer>() {{
			var binStep = categorySizeOrder.size() / binCount;
			for (int i = 0; i < categorySizeOrder.size(); i++) {
				this.put(categorySizeOrder.get(i), i/binStep);
			} }};
		// Each category has assigned a bin of size.
		// All categories with a lower bin have smaller size than all categories with a higher bin.

		return categoryPrecisionOrder.stream()
			.sorted(Comparator.comparingInt(category_BinBySize::get)).toList();
	}

	static List<Category> categoryOrderingByRelativePrecision() throws IOException {
		var knnPrecisions = StretchedSequences.getKnnMeasurements();

		var originalPrecisions = knnPrecisions.mapValues(v -> v.get(_1));
		var relativePrecisions = new MyMap<Category, Double>();
		var skeletonCategory_Precisions = originalPrecisions.get(MocapType.SKELETON);
		for (var category : skeletonCategory_Precisions.keySet()) {
			var averageMwPrecision = MocapType.mwValues().stream()
				.map(type -> originalPrecisions.get(type).get(category))
				.mapToDouble(list -> list.stream()
					.mapToDouble(x -> x)
					.average().orElseThrow())
				.average().orElseThrow();
			var skeletonPrecision = skeletonCategory_Precisions
				.get(category).stream().mapToDouble(x -> x)
				.average().orElseThrow();
			relativePrecisions.put(category, averageMwPrecision / skeletonPrecision);
		}

		return relativePrecisions
			.entrySet().stream()
			.sorted(Entry.comparingByValue())
			.map(Entry::getKey)
			.toList();
	}

	static void categoryOrderingPrint(PrintWriter output, List<Category> categoryOrdering) {
		categoryOrdering.forEach(
			category -> output.println(category.id)
		);
		output.flush();
	}

	/**
	 * This returns a {@link Predicate} that tells if its given {@link LocalAbstractObject}
	 * has a locator URI matching to the locator URI of <code>sequence</code>.
	 * Two LocalAbstractObjects have matching locator URIs iff
	 * the URIs are digit sequences separated by “_” and the first three of them are equal.
	 * This is supposed to be used to search for matching stretched sequences.
	 *
	 * @param sequence a sequence to whose URI the other's URI shall be matched
	 * @return a predicate that tells if its given object has a locator URI matching to that of <code>sequence</code>
	 */
	public static Predicate<LocalAbstractObject> uriMatches(LocalAbstractObject sequence) {
		return other -> {
			try {
				var uriArray = sequence.getLocatorURI().split("_");
				var otherUriArray = other.getLocatorURI().split("_");

				return IntStream.of(0, 1, 2).allMatch(
					x -> uriArray[x].equals(otherUriArray[x])
				);
			} catch (PatternSyntaxException | NumberFormatException | IndexOutOfBoundsException e) {
				return false;
		}};
	}

	/**
	 * This takes the output from {@link KnnPrecision#compute(ObjectMgmt, ObjectMgmt)}
	 * and creates an average of precision for each category.
	 *
	 * It's very similar to {@link KnnPrecision#average(Map)};
	 * the only difference is that that function takes data containing the sequences and ignores them,
	 * and this function takes data without the sequences.
	 *
	 * @param knnData an output from {@link KnnPrecision#compute(ObjectMgmt, ObjectMgmt)}
	 * @return a map from each category to the average precision in the category
	 */
	public static MyMap<Category, Double> knnPrecisionAverage(MyMap<Category, List<Double>> knnData) {
		return knnData
			.mapValues(
				value -> value.parallelStream().mapToDouble(x -> x).average().orElse(Double.NaN)
			);
	}

	/**
	 * This gets, for every mocap type, for every speed multiple, for every category,
	 * for every sequence in the category, the precision of kNN search
	 * of the sequence with the speed multiple among sequences with multiple 1.
	 *
	 * This either computes it or loads it from cache.
	 *
	 * @return a map of every mocap type to a map of every speed multiple to a map of every category to a list of all kNN precisions of its sequences
	 * @throws IOException if the data for the computation can't be loaded
	 */
	public static MyMap<MocapType, MyMap<SpeedMultiple, MyMap<Category, List<Double>>>> getKnnMeasurements() throws IOException {
		var knnMeasurements = new MyMap<MocapType, MyMap<SpeedMultiple, MyMap<Category, List<Double>>>>();

		for(var type : MocapType.values()) {
			var multiple_Measurements = new MyMap<SpeedMultiple, MyMap<Category, List<Double>>>();
			
			for(var multiple : SpeedMultiple.values()) {
				multiple_Measurements.put(multiple, getKnnMeasurements(type, multiple));
			}

			knnMeasurements.put(type, multiple_Measurements);
		}

		return knnMeasurements;
	}

	public static MyMap<Category, List<Double>> getKnnMeasurements(
		 MocapType type,
		 SpeedMultiple multiple
	) throws IOException {
		var cacheDir = new File(Config.cacheDir(), "stretchKnnPrecision");
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s – %s×", type.noun(), multiple.asString()));

		try {
			return readCache(cachePath, MyMap.class);
		} catch(CacheIOException _e) {
			var stretchedSequenceMgmt = loadData(
				 dataFiles(type).get(multiple),
				 type.dataClass()
			);
			var baseSequenceMgmt = loadData(
				 dataFiles(type).get(_1),
				 type.dataClass()
			);
			var result = KnnPrecision.compute(baseSequenceMgmt, stretchedSequenceMgmt);

			try {
				writeCache(cachePath, result);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return result;
		}
	}

	public static MyMap<SpeedMultiple, File> dataFiles(MocapType type) {
		var speedMultiples = Arrays.asList(SpeedMultiple.values());

		return switch (type) {
			case SKELETON -> speedMultiples.stream().collect(MyMap.keyCollector(
				mult -> new File(
					Config.dataDir(),
					"skeleta/class130-actions-coords_normPOS" + (mult.equals(_1) ? "" : "_stretched-" + mult.asString() + "×")
						+ ".data"
			)));

			case HARD_MW -> speedMultiples.stream().collect(MyMap.keyCollector(
				mult -> new File(
					Config.dataDir(),
					"motion words/hard_motion_words"
						+ (mult.equals(_1) ? "" : "_stretched_" + mult.asString() + "×")
						+ ".data"
			)));

			case SOFT_MW -> speedMultiples.stream().collect(MyMap.keyCollector(
				mult -> new File(
					Config.dataDir(),
					"motion words/soft_motion_words"
						+ (mult.equals(_1) ? "" : "_stretched_" + mult.asString() + "×")
						+ ".data"
			)));

			case MATCH_N_MW -> speedMultiples.stream().collect(MyMap.keyCollector(
				mult -> new File(
					Config.dataDir(),
					"motion words/multi-overlay_motion_words"
						+ (mult.equals(_1) ? "" : "_stretched_" + mult.asString() + "×")
						+ ".data"
			)));
		};
	}
}
