package xyz.matj1.bp.experiments;

import mcdr.objects.impl.ObjectMotionWordNMatches;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.enums.LengthRelation;
import xyz.matj1.bp.types.DoubleRange;
import xyz.matj1.bp.types.PointSet;
import xyz.matj1.bp.types.Vector2;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * This is for functions used for the experiment pairwise correlation.
 */
public class PairwiseCorrelation {
	/**
	 * This calculates the length relation and the distance for each pair of sequences in each category.
	 *
	 * @param mwMgmt ObjectMgmt object containing the sequences and their categories
	 * @param lengthRelation how the relation of the lengths shall be expressed
	 * @return a map of categories to maps of pairs of the sequences to the results
	 */
	public static PCData compute(
		ObjectMgmt mwMgmt,
		LengthRelation lengthRelation
	) {
		var categoryObjects = mwMgmt.getCategoryObjects();
		var accumulator = new PCData(LengthRelation.QUOTIENT);

		for (var entry : categoryObjects.entrySet()) {
			var category = entry.getKey();
			var sequences = entry.getValue();
			accumulator.put(category, new PointSet());

			for (int i1 = 0; i1 < sequences.size(); i1++) {
				for (int i2 = i1+1; i2 < sequences.size(); i2++) {
					var seq1 = sequences.get(i1);
					var seq2 = sequences.get(i2);

					if (category.id.equals("73") && Utils.normaliseQuotient((double)Utils.sequenceLengthFromUri(seq1) / Utils.sequenceLengthFromUri(seq2)) > 10) {
						continue;
					} // to skip anomalous data points caused by an error in the data set

					BiFunction<Double, Double, Double> relationOperation = switch (lengthRelation) {
						case DIFFERENCE -> (a, b) -> Math.abs(a - b);
						case QUOTIENT -> (a, b) -> Utils.normaliseQuotient(a / b);
					};

					accumulator.get(category).add(
						new Vector2<>(
							relationOperation.apply((double)Utils.sequenceLengthFromUri(seq1), (double)Utils.sequenceLengthFromUri(seq2)),
							(double)seq1.getDistance(seq2)
					)); // add the point to the data
		}}}

		return accumulator;
	}

	/**
	 * This calls {@link #show(ObjectMgmt, LengthRelation, File, Vector2)} with a default size of output 1024×1024.
	 *
	 * @param mwMgmt ObjectMgmt object containing the sequences and their categories
	 * @param lengthRelation how the relation of the lengths shall be expressed
	 * @param graphsPath path of the directory where the graphs shall be saved
	 * @throws IOException if there is an error in writing to a file in graphsPath
	 */
	public static void show(
		ObjectMgmt mwMgmt,
		LengthRelation lengthRelation,
		File graphsPath
	) throws IOException {
		var defaultSize = new Vector2<>(512d, 512d);

		show(
			mwMgmt,
			lengthRelation,
			graphsPath,
			defaultSize
	);}

	/**
	 * Creates an XY graph of the length relation and the distance for each pair of sequences in the category for each category.
	 * That means that the points of each category are shown in their own graph, separated from the points of other categories.
	 *
	 * @param mwMgmt ObjectMgmt object containing the sequences and their categories
	 * @param lengthRelation how the relation of the lengths shall be expressed
	 * @param graphsPath path of the directory where the graphs shall be saved
	 * @param size a pair of integers specifying the width and the height of the output image
	 */
	public static void show(
		ObjectMgmt mwMgmt,
		LengthRelation lengthRelation,
		File graphsPath,
		Vector2<Double> size
	) {
		ObjectMotionWordNMatches.maxPartsToMatch = 5;
		ObjectMotionWordNMatches.nMatches = 1; //another reasonable choice is 2

		if (!graphsPath.isDirectory()) {
			throw new IllegalArgumentException(graphsPath.getPath() + " isn't a directory.");
		}

		var correlationData = compute(mwMgmt, lengthRelation);

		for (var entry : correlationData.entrySet()) {
			var category = entry.getKey();
			var chart = showCategory(entry.getValue(), lengthRelation, Optional.of(category));
			Utils.writeXYGraphAsPdf(
				chart,
				new File(graphsPath, "graph" + category.id + ".pdf"),
				size.map(Number::intValue),
				StandardChartTheme.createDarknessTheme() // added for dark theme
			);
		}

		System.err.println("Pairwise correlations for " + mwMgmt + " written to " + graphsPath.getPath());
	}

	/**
	 * This creates an XY graph of pairwise correlations treating them as in a single category.
	 * Iff <code>categoryOpt</code> is empty, title is null, so it needs to be added later.
	 *
	 * @param pointData a set of points to be plotted to the graph
	 * @param lengthRelation the length relation of the sequences
	 * @param categoryOpt optional category of motions from which the data were created
	 * @return a graph with the data
	 */
	public static JFreeChart showCategory(PointSet pointData, LengthRelation lengthRelation, Optional<Category> categoryOpt) {
		var dataSeries = new XYSeries(categoryOpt.orElse(new Category(null, null, 0)).index);
		pointData.forEach(v -> dataSeries.add(v.a(), v.b()));
		var dataset = new XYSeriesCollection(dataSeries);

		return ChartFactory.createScatterPlot(
			categoryOpt.isEmpty() ? null : "The length relation and the distance of each two sequences in category " + categoryOpt.get().id,
			"the " + switch(lengthRelation) {
				case DIFFERENCE -> "difference";
				case QUOTIENT -> "quotient";
			} +" of the lengths",
			"the distance",
			dataset,
			PlotOrientation.VERTICAL,
			false,
			false,
			false
		);
	}

	/**
	 * This removes all sequence identifiers from the data,
	 * puts all points from all categories together,
	 * and smooths the points by calculating an average of all points
	 *
	 * @param pairData the data to be smoothed
	 * @param precision the frequency of average points
	 * @param averageRange points up to how far from the centre shall be counted in the average
	 * @return smoothed data sequence
	 */
	public static PointSet smooth(PCData pairData, Double precision, Double averageRange) {
		var flatData = new PointSet(); // pairData flattenened
		pairData.forEach(
			(category, point) -> flatData.addAll(point)
		);

		return smooth(flatData, precision, averageRange);
	}

	/**
	 * This smooths the given data by averaging all b values in the range of
	 * <code>averageRange</code> from each point.
	 * <code>precision</code> determines the frequency
	 * of the calculated average values on the a (x) axis.
	 *
	 * @param pairData the data to be smoothed
	 * @param precision the frequency of average points
	 * @param averageRange points up to how far from the centre shall be counted in the average
	 * @return smoothed data sequence
	 */
	public static PointSet smooth(PointSet pairData, Double precision, Double averageRange) {
		return new DoubleRange(
			pairData.first().a(),
			pairData.last().a(),
			precision
		).stream().map(
			value -> new Vector2<>(
				value,
				pairData.subSet(
					new Vector2<>(value - averageRange, 0d),
					new Vector2<>(value + averageRange, 0d)
				).stream().mapToDouble(Vector2::b).average().orElse(Double.NaN)
		)).collect(PointSet::new, PointSet::add, PointSet::addAll);
	}

	/**
	 * This counts the data points from <code>pairData</code> in the vicinity
	 * of the data point on the a axis and sets it as the b value of the point.
	 * The size of the vicinity is given by <code>range</code>,
	 * so the vicinity contains everything closer than <code>range</code> to the current point on the a axis.
	 * <code>precision</code> determines the frequency
	 * of the calculated values on the a axis.
	 *
	 * @param pairData the data for counting
	 * @param precision how many points shall be calculated
	 * @param range the radius of the vicinity
	 * @return a sequence of points
	 */
	public static PointSet quantity(PCData pairData, Double precision, Double range) {
		var flatData = new PointSet(); // pairData flattenened
		pairData.forEach(
			(category, point) -> flatData.addAll(point)
		);

		return quantity(flatData, precision, range);
	}

	/**
	 * This counts the data points from <code>pairData</code> in the vicinity
	 * of the data point on the a axis and sets it as the b value of the point.
	 * The size of the vicinity is given by <code>range</code>,
	 * so the vicinity contains everything closer than <code>range</code> to the current point on the a axis.
	 * <code>precision</code> determines the frequency
	 * of the calculated values on the a axis.
	 *
	 * @param pairData the data for counting
	 * @param precision how many points shall be calculated
	 * @param range the radius of the vicinity
	 * @return a sequence of points
	 */
	public static PointSet quantity(PointSet pairData, Double precision, Double range) {
		return new DoubleRange(
			pairData.first().a(),
			pairData.last().a(),
			precision
		).stream().map(
			value -> new Vector2<>(
				value,
				(double)pairData.subSet(
					new Vector2<>(value - range, 0d),
					new Vector2<>(value + range, 0d)
				).size()
		)).collect(PointSet::new, PointSet::add, PointSet::addAll);
	}

	/**
	 * This is a wrapper for the data type used as a result of {@link #compute(ObjectMgmt, LengthRelation)}
	 * also storing the length relation of the sequences (difference or quotient).
	 *
	 * That means that it is a {@link ConcurrentHashMap} from {@link Category} to {@link SortedSet}
	 * of {@link Vector2<Double>}, where the elements are sorted by a values first and then b values.
	 *
	 * Only 2 constructors are supported, the one with no parameters and the one taking a map for initialisation.
	 */
	public static class PCData extends ConcurrentHashMap<Category, PointSet> {
		private final LengthRelation lengthRelation;

		public PCData(LengthRelation lengthRelation) {
			super();
			this.lengthRelation = lengthRelation;
		}

		public PCData(
			LengthRelation lengthRelation,
			Map<Category, Collection<Vector2<Double>>> initMap
		) {
			super();
			super.putAll(
				initMap.entrySet().stream()
					.collect(Collectors.toMap(
						Entry::getKey,
						c -> new PointSet(c.getValue())
			)));
			this.lengthRelation = lengthRelation;
		}

		/**
		 * @return the length relation of the sequences in these data
		 */
		public LengthRelation lengthRelation() {
			return lengthRelation;
		}

		/**
		 * This converts the object to a {@link XYDataset} for creating {@link Plot}s for {@link JFreeChart}s.
		 * Each category has a data series identified by the index of the category.
		 *
		 * @return a dataset with the data in this object
		 */
		public XYDataset toDataset() {
			var data = this.entrySet().stream()
				.collect(Collectors.toMap(
					entry -> entry.getKey().index,
					entry -> entry.getValue().stream().toList()
				)); // stripIds but then replaces categories with their index numbers and converts the data series to lists

			var dataset =  new DefaultXYDataset();
			for (var category : data.keySet()) {
				double[] xArray = data.get(category).stream().mapToDouble(Vector2::a).toArray();
				double[] yArray = data.get(category).stream().mapToDouble(Vector2::b).toArray();
				dataset.addSeries(category, new double[][]{xArray, yArray});
			}

			return dataset;
		}
	}
}
