package xyz.matj1.bp.experiments;

import mcdr.objects.utils.RankedSortedCollectionDistHashcode;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import messif.operations.AnswerType;
import messif.operations.query.KNNQueryOperation;
import xyz.matj1.bp.StretchedSequences;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.types.MyMap;

import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;

/**
 * This is like KnnPrecision, but average distance instead of precision is calculated.
 */
public class KnnDistance {
	/**
	 * For each motion in <code>stretchedSequenceMgmt</code>, this does a kNN search
	 * around it among the motions from <code>baseSequenceMgmt</code>
	 * and calculates the average distance from it to the found objects.
	 *
	 * The result is returned as a map from each category to a list of doubles,
	 * the average distance for each motion in the category.
	 *
	 * @param baseSequenceMgmt {@link ObjectMgmt} of base motions, among which the kNN search will be done
	 * @param stretchedSequenceMgmt {@link ObjectMgmt} of stretched motions, which will be used as a bases for the kNN search
	 * @return a map from each category to a list of the average distance for each motion in the category
	 */
	public static MyMap<Category, List<OptionalDouble>> compute(
		ObjectMgmt baseSequenceMgmt,
		ObjectMgmt stretchedSequenceMgmt
	) {
		var stretchedCategoryObjects = Collections.unmodifiableMap(stretchedSequenceMgmt.getCategoryObjects());

		var baseObjects = Collections.unmodifiableSet(baseSequenceMgmt.getObjects());
		var results = new MyMap<Category, List<OptionalDouble>>();

		baseSequenceMgmt.getCategoryObjects()
			.entrySet()
			.parallelStream()
			.forEach(entry -> {
				var category = entry.getKey();
				var sequences = entry.getValue();

				var distances  = sequences.stream().map(sequence -> {
					var stretchedObject = stretchedCategoryObjects.get(category)
						.stream().filter(
							StretchedSequences.uriMatches(sequence)
						).findAny().orElseThrow();

					var knnSequences = KnnSearchTools.knnSearch(
						stretchedObject,
						baseObjects.stream()
							.filter(StretchedSequences.uriMatches(stretchedObject).negate())
							.toList(),
						sequences.size() - 1
					).keySet();

					return knnSequences.stream().mapToDouble(seq -> (seq.getDistance(stretchedObject))).average();
				}).toList();

				results.put(category, distances);
			});

		return results;
	}

	/**
	 * This is like {@link #compute(ObjectMgmt, ObjectMgmt)} but with a fixed k in the kNN search.
	 *
	 * @param baseSequenceMgmt {@link ObjectMgmt} of base motions, among which the kNN search will be done
	 * @param stretchedSequenceMgmt {@link ObjectMgmt} of stretched motions, which will be used as a bases for the kNN search
	 * @param k how many nighest sequences shall be found in the kNN search
	 * @return a map from each category to a list of the average distance for each motion in the category
	 */
	public static MyMap<Category, List<OptionalDouble>> computeWithFixedK(
		ObjectMgmt baseSequenceMgmt,
		ObjectMgmt stretchedSequenceMgmt,
		Integer k
	) {
		var stretchedCategoryObjects = Collections.unmodifiableMap(stretchedSequenceMgmt.getCategoryObjects());

		var baseObjects = Collections.unmodifiableSet(baseSequenceMgmt.getObjects());
		var results = new MyMap<Category, List<OptionalDouble>>();

		baseSequenceMgmt.getCategoryObjects()
			.entrySet()
			.parallelStream()
			.forEach(entry -> {
				var category = entry.getKey();
				var sequences = entry.getValue();

				var distances  = sequences.stream().map(sequence -> {
					var stretchedObject = stretchedCategoryObjects.get(category)
						.stream().filter(
							StretchedSequences.uriMatches(sequence)
						).findAny().orElseThrow();

					var operation = new KNNQueryOperation(
						stretchedObject,
						k,
						false,
						AnswerType.ORIGINAL_OBJECTS,
						new RankedSortedCollectionDistHashcode()
					);

					baseObjects.stream()
						.filter(StretchedSequences.uriMatches(stretchedObject).negate())
						.forEach(operation::addToAnswer);

					var queryCategory = stretchedSequenceMgmt.getObjectCategories(stretchedObject).get(0);

					return Utils.iteratorToStream(operation.getAnswer())
						.mapToDouble(seq -> ((LocalAbstractObject)seq.getObject()).getDistance(stretchedObject)).average();
				}).toList();

				results.put(category, distances);
			});

		return results;
	}

}
