package xyz.matj1.bp.experiments;

import mcdr.objects.utils.RankedSortedCollectionDistHashcode;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedSortedCollection;
import messif.operations.AnswerType;
import messif.operations.query.KNNQueryOperation;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.StretchedSequences;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.mw_improvements.EqualLengthExperiments;
import xyz.matj1.bp.types.MyMap;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import static xyz.matj1.bp.Utils.loadData;
import static xyz.matj1.bp.Utils.readCache;
import static xyz.matj1.bp.Utils.writeCache;
import static xyz.matj1.bp.enums.SpeedMultiple._1;

public class KnnSearchTools {
	public static MyMap<LocalAbstractObject, Float> knnSearch(
		LocalAbstractObject queryObject,
		Collection<LocalAbstractObject> searchSpace,
		Integer k
	) {
		return knnSearch(
			queryObject,
			searchSpace,
			k,
			false,
			AnswerType.ORIGINAL_OBJECTS,
			new RankedSortedCollectionDistHashcode()
		);
	}
	
	public static MyMap<LocalAbstractObject, Float> knnSearch(
		LocalAbstractObject queryObject,
		Collection<LocalAbstractObject> searchSpace,
		Integer k,
		boolean storeMetaDistances,
		AnswerType answerType,
		RankedSortedCollection answerCollection
	) {
		var operation = new KNNQueryOperation(
			queryObject,
			k,
			storeMetaDistances,
			answerType,
			answerCollection
		);

		searchSpace.forEach(
			operation::addToAnswer
		);

		var result = new MyMap<LocalAbstractObject, Float>();
		operation.forEach(a -> result.put((LocalAbstractObject)a.getObject(), a.getDistance()));

		return result;
	}

	/**
	 * For each mocap type, for each speed multiple, this returns a map from each sequence
	 * to the list of sequences found by kNN search around the sequence
	 * with k equal to the number of other sequences in the category of the sequence.
	 *
	 * This caches results for faster retrieval after the first call, unless the caches are deleted or invalid.
	 *
	 * @return a nested map for each mocap type for each speed multiple, for each sequence, a list of the sequences nighest to the sequence
	 */
	public static MyMap<MocapType, MyMap<SpeedMultiple, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>>> stretchKnnResults() throws IOException {
		var results = new MyMap<MocapType, MyMap<SpeedMultiple, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>>>();

		for(var type : MocapType.values()) {
			var multiple_Results = new MyMap<SpeedMultiple, MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>>();

			for(var multiple : SpeedMultiple.values()) {
				multiple_Results.put(multiple, stretchKnnResults(type, multiple));
			}

			results.put(type, multiple_Results);
		}

		return results;

	}

	/**
	 * This is like {@link #stretchKnnResults()}, but only for the given type and multiple.
	 * The result of this is a part of the result of {@link #stretchKnnResults()}.
	 *
	 * @param type the type
	 * @param multiple the multiple
	 * @return a map from each sequence to a map of its nearest neighbors and their distances
	 * @throws IOException if the data can't be loaded
	 */
	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> stretchKnnResults(
		MocapType type,
		SpeedMultiple multiple
	) throws IOException {
		var cacheDir = new File(Config.cacheDir(), "stretchKnnResultsUris");
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s – %s×", type.noun(), multiple.asString()));

		var stretchedSequenceMgmt = loadData(
			StretchedSequences.dataFiles(type).get(multiple),
			type.dataClass()
		);
		var baseSequenceMgmt = loadData(
			StretchedSequences.dataFiles(type).get(_1),
			type.dataClass()
		);

		try {
			MyMap<String, MyMap<String, Float>> cacheWithUris = readCache(cachePath, MyMap.class);
			return cacheWithUris.map(
				stretchedSequenceMgmt::getObject,
				innerResults -> innerResults.map(
					baseSequenceMgmt::getObject,
					Function.identity()));
		} catch (CacheIOException _e) {
			MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> results = stretchKnnResultsCompute(
				stretchedSequenceMgmt,
				baseSequenceMgmt
			);

			try {
				var resultsWithUris = results.map(
					AbstractObject::getLocatorURI,
					innerResults -> innerResults.map(
						AbstractObject::getLocatorURI,
						Function.identity()));
				writeCache(cachePath, resultsWithUris);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return results;
		}
	}

	/**
	 * This computes kNN search with every sequence from <code>stretchedSequenceMgmt</code>
	 * as the query object among the sequences form <code>baseSequenceMgmt</code>.
	 *
	 * This returns a map from each query object to a map of its nearest neighbors and their distances.
	 *
	 * @param stretchedSequenceMgmt ObjectMgmt for query objects
	 * @param baseSequenceMgmt ObjectMgmt with search space
	 * @return a map from each query object to a map of its nearest neighbors and their distances
	 */
	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> stretchKnnResultsCompute(
		ObjectMgmt stretchedSequenceMgmt,
		ObjectMgmt baseSequenceMgmt
	) {
		var baseObjects = Collections.unmodifiableSet(baseSequenceMgmt.getObjects());
		var baseCategory_Objects = Collections.unmodifiableMap(baseSequenceMgmt.getCategoryObjects());
		var stretchedObjects = Collections.unmodifiableSet(stretchedSequenceMgmt.getObjects());
		var results = new MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>>();

		baseCategory_Objects
			.entrySet()
			.parallelStream()
			.forEach(entry -> {
				var sequencesInCategory = entry.getValue();
				var categorySize = sequencesInCategory.size();

				sequencesInCategory.parallelStream().forEach(sequence -> {
					var stretchedObject = stretchedObjects
						.stream().filter(
							StretchedSequences.uriMatches(sequence)
						).findAny().orElseThrow();

					var knnSequence_Distance = knnSearch(
						stretchedObject,
						baseObjects.stream()
							.filter(StretchedSequences.uriMatches(stretchedObject).negate())
							.toList(),
						Math.max(20, categorySize - 1)
					);
					
					results.put(sequence, knnSequence_Distance);
				});
			});
		return results;
	}

	/**
	 * This retrieves kNN search results for every sequence of the given
	 * <code>sequenceMgmt</code> among all other sequences of it
	 * by reading cache or by computing the results.
	 *
	 * If the results can be read from the cache in the given directory, they are loaded.
	 * Otherwise, they are computed and saved to a file in the given directory.
	 *
	 * The given <code>type</code> has to correspond to the type of the sequences in <code>sequenceMgmt</code>.
	 *
	 * This is like {@link EqualLengthExperiments#knnSearchResults(MocapType)},
	 * but to avoid loading data if they are already loaded.
	 *
	 * @param type the type of the sequences
	 * @param sequenceMgmt the sequence management with the sequences for kNN
	 * @return a map from every sequence to a map of its nearest neighbors and their distances
	 */
	public static MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnSearchResultsWithCaching(
		MocapType type,
		ObjectMgmt sequenceMgmt,
		File cacheDir
	) {
		cacheDir.mkdirs();
		var cachePath = new File(cacheDir, String.format("%s", type.noun()));

		try {
			MyMap<String, MyMap<String, Float>> cached =  readCache(cachePath, MyMap.class);
			return cached.map(
				sequenceMgmt::getObject,
				mymap -> mymap.map(
					sequenceMgmt::getObject,
					Function.identity()
				)
			); // convert sequence URIs to the actual sequences
		} catch (CacheIOException _e) {
			MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> results = KnnSearchTools.stretchKnnResultsCompute(
				sequenceMgmt,
				sequenceMgmt
			);

			var resultsWithUris = results.map(
				AbstractObject::getLocatorURI,
				mymap -> mymap.map(
					AbstractObject::getLocatorURI,
					Function.identity()
				)
			);

			try {
				writeCache(cachePath, resultsWithUris);
			} catch (CacheIOException e) {
				System.err.printf("Writing cache to %s failed.%n", cachePath);
				e.printStackTrace();
			}

			return results;
		}
	}

	/**
	 * This computes a list of precisions for each category from <code>knnResults</code>,
	 * results of a kNN search (like from {@link EqualLengthExperiments#knnSearchResults(MocapType)}).
	 * If a category has too few sequences (1 or less), the precisions for it will be NaN.
	 *
	 * The given <code>sequenceMgmt</code> is for determining categories.
	 *
	 * @param knnResults results of a kNN search
	 * @param sequenceMgmt a sequence management for determining categories
	 * @return a map from each category to a list of precisions of kNN searches for its sequences
	 */
	public static MyMap<Category, List<Double>> computeKnnPrecisions(
		MyMap<LocalAbstractObject, MyMap<LocalAbstractObject, Float>> knnResults,
		ObjectMgmt sequenceMgmt
	) {
		var categoryObjects = new MyMap<>(sequenceMgmt.getCategoryObjects());
		var category_Precisions = categoryObjects
			.mapValues(sequences -> {
				var categorySize = sequences.size();
				return sequences.stream()
					.mapToDouble(
						sequence -> knnResults.get(sequence)
							.entrySet().stream()
							.sorted(Entry.comparingByValue())
							.limit(categorySize - 1)
							.filter(entry -> sequences.contains(entry.getKey()))
							.count() / (double)categorySize
					)
					.boxed().toList();
			});
		return category_Precisions;
	}
}
