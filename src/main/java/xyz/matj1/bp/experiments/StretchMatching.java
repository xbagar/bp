package xyz.matj1.bp.experiments;

import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.types.Pair;
import xyz.matj1.bp.types.Vector2;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static xyz.matj1.bp.StretchedSequences.uriMatches;

/**
 * This is for functions used for the experiment stretch matching.
 */
public class StretchMatching {
	/**
	 * This creates a graph from the results of {@link #compute(ObjectMgmt, ObjectMgmt)}
	 * and saves it to <code>outputFile</code>. <code>stretchMultiple</code> is just for labeling the graph.
	 *
	 * @param matchData data returned from {@link #compute(ObjectMgmt, ObjectMgmt)}
	 * @param stretchMultiple the multiple by which the stretched sequences are stretched
	 * @param outputFile where the graph shall be saved
	 */
	public static void show(
		Map<String, Pair<Category, Double>> matchData,
		double stretchMultiple,
		File outputFile
	) {
		var dataSeries = new ConcurrentHashMap<Category, XYSeries>();

		matchData.entrySet().parallelStream().forEach(entry -> {
			var length = Integer.parseInt(entry.getKey().split("_")[3]);
			var category = entry.getValue().a();
			var distance = entry.getValue().b();

			if (!dataSeries.containsKey(category)) {
				dataSeries.put(category, new XYSeries(category.index));
			}
			var series = dataSeries.get(category);
			series.add(length, distance);
		});

		var dataset = new XYSeriesCollection();
		dataSeries.values().forEach(dataset::addSeries);

		var chartPlot = ChartFactory.createScatterPlot(
			"The length of a sequence and the distance to its " + stretchMultiple + "× stretched version",
			"the length",
			"the distance",
			dataset,
			PlotOrientation.VERTICAL,
			true,
			false,
			false
		);

		Utils.writeXYGraphAsPdf(chartPlot, outputFile, new Vector2<>(2048, 1024));

		System.err.println("Stretch-matching graph written to " + outputFile.getPath());
	}

	/**
	 * This returns a {@link Map} of its locator URI and a
	 * {@link Pair} of its category and the distance to its stretched version
	 * for all sequences in <code>baseSequenceMgmt</code>.
	 * <code>stretchedSequenceMgmt</code> is supposed to contain the stretched versions
	 * of all sequences in <code>baseSequenceMgmt</code> in the same categories.
	 *
	 * @param baseSequenceMgmt a management of the base sequences
	 * @param stretchedSequenceMgmt a management of the stretched vesions of the sequences
	 * @return a {@link Map} of its locator URI and a {@link Pair} of its category
	 * 	and the distance to its stretched version for all sequences in <code>baseSequenceMgmt</code>
	 */
	public static Map<String, Pair<Category, Double>> compute(
		ObjectMgmt baseSequenceMgmt,
		ObjectMgmt stretchedSequenceMgmt
	) {
		var results = new ConcurrentHashMap<String, Pair<Category, Double>>();
		var stretchedCategoryObjects = stretchedSequenceMgmt.getCategoryObjects();

		baseSequenceMgmt.getCategoryObjects().entrySet().parallelStream().forEach(entry -> {
			var category = entry.getKey();
			var sequences = entry.getValue();
			sequences.parallelStream().forEach(sequence -> {
				var stretchedSequence = stretchedCategoryObjects.get(category).stream()
					.filter(uriMatches(sequence)).findFirst().orElseThrow();
				results.put(
					sequence.getLocatorURI(),
					new Pair<>(category, (double)sequence.getDistance(stretchedSequence))
				);
		}); });

		return results;
	}
}
