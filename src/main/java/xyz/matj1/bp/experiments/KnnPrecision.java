package xyz.matj1.bp.experiments;

import mcdr.objects.utils.RankedSortedCollectionDistHashcode;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import messif.operations.AnswerType;
import messif.operations.query.KNNQueryOperation;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import xyz.matj1.bp.Config;
import xyz.matj1.bp.StretchedSequences;
import xyz.matj1.bp.Utils;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.Pair;
import xyz.matj1.bp.types.Vector2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * This is for functions used for the experiment kNN precision.
 */
public class KnnPrecision {
	/**
	 * This searches for the closes sequences of each sequence
	 * and counts how many of them are from the same category.
	 *
	 * Tato funkce je pro testování motion words tak,
	 * že se vyhledávají nejpodobnější pohyby k danému pohybu s určitou délkou,
	 * a měří se, jak pravděpodobně jsou mezi těmi nejpodobnějšími pohyby
	 * různě dlouhé pohyby ze stejné kategorie. (starý popis této funkce)
	 *
	 * @param mwMgmt ObjectMgmt object containing the sequences and their categories
	 * @return a map of each category and a list of the precision for each motion in the category
	 */
	public static Map<Category, List<Pair<LocalAbstractObject, Double>>> compute(ObjectMgmt mwMgmt) {
		var categoryObjects = mwMgmt.getCategoryObjects();
		var accumulator = new ConcurrentHashMap<Category, List<Pair<LocalAbstractObject, Double>>>();

		var  dataStream = categoryObjects.entrySet().parallelStream();
		dataStream.forEach(entry -> {
			var category = entry.getKey();
			var sequences = entry.getValue();
			accumulator.put(category, new ArrayList<>());

			for (var sequence : sequences) {
				var operation = new KNNQueryOperation(
					sequence,
					sequences.size() - 1,
					false,
					AnswerType.ORIGINAL_OBJECTS,
					new RankedSortedCollectionDistHashcode()
				);

				mwMgmt.getObjects().forEach(object -> {
					if (!object.getLocatorURI().equals(sequence.getLocatorURI())) {
						operation.addToAnswer(object);
				}});

				var answer =
					Utils.iteratorToStream(operation.getAnswer())
						.map(o -> (LocalAbstractObject)(o.getObject())); // Proč KNNOperation bere LocalAbstractObject a vrací DistanceRankedObject<>?

				var answerScore = answer.mapToInt(o ->
					mwMgmt
						.getObjectCategories(o)
						.contains(category)
						? 1 : 0
				).sum() / (double)operation.getK();

				accumulator.get(category).add(new Pair<>(sequence, answerScore));
		}});

		return accumulator;
	}

	/**
	 * For each motion in <code>stretchedSequenceMgmt</code>, this does a kNN search around it
	 * and calculates among the motions from <code>baseSequenceMgmt</code>
	 * the precision of search within its category,
	 * that is how many found motions are from the same category per the number of all found motions.
	 *
	 * The result is returned as a map from each category to a list of doubles,
	 * the precisions of each motion in the category.
	 *
	 * @param baseSequenceMgmt {@link ObjectMgmt} of base motions, among which the kNN search will be done
	 * @param stretchedSequenceMgmt {@link ObjectMgmt} of stretched motions, which will be used as a bases for the kNN search
	 * @return a map from each category to a list of the precision for each motion in the category
	 */
	public static MyMap<Category, List<Double>> compute(
		ObjectMgmt baseSequenceMgmt,
		ObjectMgmt stretchedSequenceMgmt
	) {
		var cacheDir = new File(Config.cacheDir(), "stretchKnnResults");

		var stretchedCategoryObjects = new MyMap<>(stretchedSequenceMgmt.getCategoryObjects());

		var baseObjects = Collections.unmodifiableSet(baseSequenceMgmt.getObjects());
		var results = new MyMap<Category, List<Double>>();

		baseSequenceMgmt.getCategoryObjects()
			.entrySet()
			.parallelStream()
			.forEach(entry -> {
				var category = entry.getKey();
				var sequences = entry.getValue();

				var scores  = sequences.stream().map(sequence -> {
					var stretchedObject = stretchedCategoryObjects.get(category)
							.stream().filter(
								StretchedSequences.uriMatches(sequence)
							).findAny().orElseThrow();
					var operation = new KNNQueryOperation(
						stretchedObject,
						sequences.size() - 1,
						false,
						AnswerType.ORIGINAL_OBJECTS,
						new RankedSortedCollectionDistHashcode()
					);

					baseObjects.stream()
						 .filter(StretchedSequences.uriMatches(stretchedObject).negate())
						 .forEach(operation::addToAnswer);

					var queryCategory = stretchedSequenceMgmt.getObjectCategories(stretchedObject).get(0);
					var score = Utils.iteratorToStream(operation.getAnswer())
						.filter(
							answer -> baseSequenceMgmt.getObjectCategories((LocalAbstractObject)answer.getObject()).get(0).equals(queryCategory)
						).count()
						/ (double)operation.getK();

					if (Double.isInfinite(score)) {
						score = Double.NaN;
					}
					return score;
				}).toList();

				results.put(category, scores);
			});

		return results;
	}

	/**
	 * Creates an XY graph of the length and the KNN score for each sequence in the category for each category.
	 * That means that the points of each category are shown in their own graph, separated from the points of other categories.
	 *
	 * @param mwMgmt ObjectMgmt object containing the sequences and their categories
	 * @param graphsPath path of the directory where the graphs shall be saved
	 */
	public static void show(ObjectMgmt mwMgmt, File graphsPath) {
		var results = compute(mwMgmt);

		for (var entry : results.entrySet()) {
			var category = entry.getKey();
			var values = entry.getValue();

			var dataset = new XYSeriesCollection();
			var dataSeries = new XYSeries(category.index);

			values.forEach(value -> dataSeries.add(
				Utils.sequenceLengthFromUri(value.a()),
				value.b()
			));

			dataset.addSeries(dataSeries);

			var chart = ChartFactory.createScatterPlot(
				"The length and the KNN search score for each sequence in category " + category.id,
				"the length of the sequence",
				"the kNN precision",
				dataset,
				PlotOrientation.VERTICAL,
				false,
				false,
				false
			);
			chart.getXYPlot().getRangeAxis().setRange(0, 1);

			var graphFile = new File(graphsPath, "graph" + category.id + ".pdf");
			Utils.writeXYGraphAsPdf(chart, graphFile, new Vector2<>(512, 512));
		}

		System.err.println("KNN score / length graphs for " + mwMgmt + " written to " + graphsPath.getPath());
	}

	/**
	 * This takes the output from {@link #compute(ObjectMgmt)}
	 * and creates an average of precision for each category.
	 *
	 * @param knnData an output from {@link #compute(ObjectMgmt)}
	 * @return a map from each category to the average precision in the category
	 */
	public static MyMap<Category, Double> average(
		Map<Category, List<Pair<LocalAbstractObject, Double>>> knnData
	) {
		return MyMap.of(knnData)
			.mapValues(
				value -> value.stream().mapToDouble(Pair::b).average().orElse(Double.NaN)
			);
	}

	/**
	 * This takes the output from {@link #average(Map)}
	 * and formats it to a string: one entry per line; the key, a tab, and the value.
	 * The key is represented by its id number in the string representation.
	 *
	 * @param averageData output from <code>average</code> to be formatted
	 * @return a string representation of the input
	 */
	public static String averageFormat(Map<Category, Double> averageData) {
		return averageData.entrySet().stream()
			.sorted(Entry.comparingByKey(Comparator.comparingInt(c -> c.index)))
			.map(entry -> String.format("%s\t%f", entry.getKey().id, entry.getValue()))
			.collect(Collectors.joining(System.lineSeparator(), "", System.lineSeparator()));
	}
}
