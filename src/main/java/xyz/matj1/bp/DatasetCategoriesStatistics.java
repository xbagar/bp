package xyz.matj1.bp;

import mcdr.test.utils.ObjectMgmt;
import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Some small tasks that don't fit elsewhere
 */
public class DatasetCategoriesStatistics {
	final static String NEWLINE = System.lineSeparator();

	public static void main(String[] args) throws IOException {
		datasetCategoriesStatistics(new PrintWriter(System.out));
		printCategorySizes(new PrintWriter(System.out));
	}

	/**
	 * This prints statistics about the dataset HDM05 to <code>output</code>.
	 * The statistics are number, average size, minimum size, maximum size,
	 * and various quantiles of sizes of the categories and the individual sequences.
	 *
	 * @param output where the statistics shall be written
	 * @throws IOException if the dataset can't be loaded
	 */
	private static void datasetCategoriesStatistics(PrintWriter output) throws IOException {
		var skeletonMgmt = Utils.loadData(LengthTesting.dataFiles(MocapType.SKELETON), MocapType.SKELETON.dataClass());

		var category_Sequences = skeletonMgmt.getCategoryObjects();
		var categorySizes = category_Sequences.values().stream()
			 .map(List::size)
			 .sorted()
			 .toList();

		output.printf("number of categories: %d%n", category_Sequences.size());

		output.println("Quantiles of category sizes:");
		for (var value : List.of(1/32d, 1/16d, 1/8d, 1/4d, 1/2d, 3/4d, 7/8d, 15/16d, 31/32d)) {
			output.println(formatQuantile(categorySizes, value));
		}

		output.printf("Minimum category size: %s%n", categorySizes.stream().mapToInt(x -> x).min().orElseThrow());
		output.printf("Maximum category size: %s%n", categorySizes.stream().mapToInt(x -> x).max().orElseThrow());
		output.printf("Average category size: %s%n", categorySizes.stream().mapToInt(x -> x).average().orElseThrow());

		for (var multiple : Arrays.stream(SpeedMultiple.values()).filter(m -> m.asDouble() <= 1).toList()) {
			output.println(NEWLINE + NEWLINE + "# " + multiple);
			for (var type : MocapType.values()) {
				var dataset = switch (type) {
					case SKELETON -> skeletonMgmt;
					default -> Utils.loadData(StretchedSequences.dataFiles(type).get(multiple), type.dataClass());
				};
				output.println();
				output.println();
				output.println("## " + type.noun());
				output.println();
				output.print(datasetCategoriesStatistics(dataset));
			}
		}

		output.flush();
	}

	/**
	 * This returns text with statistics about <code>dataset</code>,
	 * the given collectionoid of motion sequences.
	 * The statistics are number, average size, minimum size, maximum size,
	 * and various quantiles of sizes of the sequences.
	 *
	 * @param dataset for what the statistics shall be made
	 * @return a text with the statistics
	 */
	public static String datasetCategoriesStatistics(ObjectMgmt dataset) {
		var output = new StringBuilder();

		var sequences = dataset.getObjects();
		var sequenceLengths = sequences.stream()
			 .map(Utils::sequenceLength)
			 .sorted()
			 .toList();

		output.append(String.format("number of sequences: %d%n", sequences.size()));

		output.append("Quantiles of sequence lengths:")
			 .append(NEWLINE);
		for (var value : List.of(1/32d, 1/16d, 1/8d, 1/4d, 1/2d, 3/4d, 7/8d, 15/16d, 31/32d)) {
			output.append(formatQuantile(sequenceLengths, value))
				 .append(NEWLINE);
		}

		output.append(String.format("Minimum sequence length: %s%n", sequenceLengths.stream().mapToInt(x -> x).min().orElseThrow()));
		output.append(String.format("Maximum sequence length: %s%n", sequenceLengths.stream().mapToInt(x -> x).max().orElseThrow()));
		output.append(String.format("Average sequence length: %s%n", sequenceLengths.stream().mapToInt(x -> x).average().orElseThrow()));

		return output.toString();
	}

	/**
	 * This computes the quantile value of the given sequence
	 * and formats it to a line of text without a line separator.
	 *
	 * @param sequence an ordered sequence among the elements of which the quantile value shall be found
	 * @param quantile the quantile, in ⟨0, 1⟩
	 * @param <A> the type of the elements of the sequence
	 * @return a line of text with the quantile value
	 */
	private static <A> String formatQuantile(List<A> sequence, Double quantile) {
		return String.format("\tquantile %s: %s", quantile, quantile(sequence, quantile));
	}

	/**
	 * This computes the quantile value of the given sequence using a default interpolation.
	 *
	 * @param sequence an ordered sequence among the elements of which the quantile value shall be found, not empty
	 * @param quantile the quantile, in ⟨0, 1⟩
	 * @param <A> the type of the elements of the sequence
	 * @return the quantile value
	 * @throws IllegalArgumentException if the sequence is empty or the quantile is not in ⟨0, 1⟩
	 */
	public static <A> A quantile(List<A> sequence, Double quantile) {
		return quantile(sequence, quantile, QuantileInterpolation.NIGHEST);
	}

	/**
	 * This computes the quantile value of the given sequence using the given interpolation.
	 *
	 * @param sequence an ordered sequence among the elements of which the quantile value shall be found, not empty
	 * @param quantile the quantile, in ⟨0, 1⟩
	 * @param interpolation the type of interpolation used for interpolating between the values
	 * @param <A> the type of the elements of the sequence
	 * @return the quantile value
	 * @throws IllegalArgumentException if the sequence is empty or the quantile is not in ⟨0, 1⟩
	 */
	public static <A> A quantile(List<A> sequence, Double quantile, QuantileInterpolation interpolation) {
		if (sequence.isEmpty()) {
			throw new IllegalArgumentException("The sequence is supposed to not be empty.");
		}
		if (quantile > 1 || quantile < 0) {
			throw new IllegalArgumentException("Only values in ⟨0, 1⟩ are allowed as quantiles.");
		}

		var length =  sequence.size();
		var index = quantile * (length - 1);
		return interpolation.apply(
			 sequence.get((int)Math.floor(index)),
			 sequence.get((int)Math.ceil(index)),
			 index
		);
	}

	/**
	 * Types of interpolation used in computing quantiles
	 *
	 * This offers only types of interpolation not using an arithmetic of the elements
	 * because there is no guarantee that the elements support an arithmetic.
	 */
	public enum QuantileInterpolation {
		LOWER,
		UPPER,
		NIGHEST,
		;

		/**
		 * This applies the interpolation. This calculates it from the given lower and upper values,
		 * which should be the closest ones, and can be the same.
		 *
		 * @param lower the highest lower value
		 * @param upper the lowest higher value
		 * @param index the index of the quantile calculated on the quantile and the length of the sequence
		 * @param <A> the type of the elements of the sequence
		 * @return the interpolated value
		 */
		<A> A apply(A lower, A upper, Double index) {
			return switch (this) {
				case LOWER -> lower;
				case UPPER -> upper;
				case NIGHEST -> {
					var fractional = index % 1;
					if (fractional < 0.5) {
						yield lower;
					} else {
						yield upper;
			} } };
	} }

	/**
	 * This prints to the output the size of each category.
	 *
	 * @param output where the output shall be written
	 * @throws IOException if the data can't be loaded
	 */
	static void printCategorySizes(PrintWriter output) throws IOException {
		var dataset = Utils.loadData(LengthTesting.dataFiles(MocapType.HARD_MW), MocapType.HARD_MW.dataClass());
		var categoryObjects = dataset.getCategoryObjects();

		categoryObjects.forEach(
			(category, sequences) -> output.printf("%s:\t%s%n", category.id, sequences.size())
		);
		output.flush();
	}
}
