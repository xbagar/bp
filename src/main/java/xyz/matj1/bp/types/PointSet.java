package xyz.matj1.bp.types;

import org.jfree.data.xy.DefaultXYDataset;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * This is a {@link SortedSet} of 2D points with coordinates as doubles,
 * and the points are sorted by their a coordinates first, then by their b coordinates.
 */
public class PointSet extends TreeSet<Vector2<Double>> {
	/**
	 * The comparator for this set, sorting by a values first, then by b values.
	 */
	private static final Comparator<Vector2<Double>> comparator = (o1, o2) -> {
		if (!o1.a().equals(o2.a())) {
			return o1.a().compareTo(o2.a());
		} else {
			return o1.b().compareTo(o2.b());
		}
	};

	/**
	 * This creates a empty PointSet.
	 */
	public PointSet() {
		super(comparator);
	}

	/**
	 * This creates a PointSet with all values in <code>c</code>.
	 *
	 * @param c a collection of initial values of the new Pointset
	 */
	public PointSet(Collection<? extends Vector2<Double>> c) {
		super(comparator);
		this.addAll(c);
	}

	/**
	 * This gives the comparator used to compare elements of PointSet.
	 * This is a static version of {@link SortedSet#comparator()}.
	 *
	 * @return the comparator for the elements of PointSet
	 */
	public static Comparator<Vector2<Double>> pointComparator() {
		return comparator;
	}

	/**
	 * This creates an array of two arrays, of which the first one
	 * contains the a-value of each element of the PointSet in their order,
	 * and the second one contains the b-value of each element of it in their order.
	 *
	 * This is for adding series to {@link DefaultXYDataset}
	 * with {@link DefaultXYDataset#addSeries(Comparable, double[][])}.
	 *
	 * @return data of this set as an array of two arrays, the first one with the a-values, the second one with the b-values
	 */
	public double[][] toDataSeries() {
		return new double[][] {
			this.stream().mapToDouble(Vector2::a).toArray(),
			this.stream().mapToDouble(Vector2::b).toArray()
		};
	}

	/**
	 * This adds a new point with the given coordinates to the set.
	 *
	 * @param a the a coordinate
	 * @param b the b coordinate
	 */
	public void add(Double a , Double b) {
		this.add(new Vector2<>(a, b));
	}
}
