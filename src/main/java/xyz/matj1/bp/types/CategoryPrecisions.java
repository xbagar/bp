package xyz.matj1.bp.types;

import xyz.matj1.bp.enums.MocapType;

import java.util.Map;

/**
 * This stores a map of precisions for each mocap type for a certain category.
 * The map of precisions should contain mapping from all mocap types.
 *
 * @param categoryId the id of the category parsed to an integer
 * @param precisions the map of from each mocap type to the precision of the category in that mocap type
 */
public record CategoryPrecisions(
	Integer categoryId,
	Map<MocapType, Double> precisions
) {
	/**
	 * This returns the precision of the category in the given mocap type.
	 *
	 * @param type the given mocap type
	 * @return the precision
	 */
	public Double precision(MocapType type) {
		return this.precisions.get(type);
	}

	/**
	 * This returns the average precision of this category in all mocap types.
	 *
	 * @return the average precision
	 */
	public Double averagePrecision() {
		return precisions
			.values().stream().mapToDouble(d -> d)
			.average().orElseThrow();
	}
}
