package xyz.matj1.bp.types;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;

/**
 * An extension of {@link ConcurrentHashMap} with added methods for mapping and filtering and some others
 *
 * @param <K> the type of the keys
 * @param <V> the type of the values
 */
public class MyMap<K, V> extends ConcurrentHashMap<K, V> {
	/**
	 * This creates a new empty MyMap.
	 */
	public MyMap() {
		super();
	}

	/**
	 * This creates a new empty MyMap with the given value as the initial capacity.
	 *
	 * @param initialCapacity the initial capacity of the map
	 * @throws IllegalArgumentException if the initial capacity of elements is negative
	 */
	public MyMap(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * This creates a new empty MyMap with the given values as the initial capacity and table density.
	 *
	 * @param initialCapacity the initial capacity of the map
	 * @param loadFactor the initial table density of the hash table
	 * @throws IllegalArgumentException if the initial capacity of elements is negative or the load factor is nonpositive
	 */
	public MyMap(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	/**
	 * This creates a new empty MyMap with the given values as the initial capacity,
	 * table density, and number of concurrently updating threads.
	 *
	 * @param initialCapacity the initial capacity of the map
	 * @param loadFactor the initial table density of the hash table
	 * @param concurrencyLevel the estimated number of concurrently updating threads
	 * @throws IllegalArgumentException if the initial capacity of elements is negative or the load factor is nonpositive
	 */
	public MyMap(int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
	}

	/**
	 * This creates a new MyMap with the content of the given map.
	 *
	 * @param originalMap the original map
	 */
	public MyMap(Map<? extends K, ? extends V> originalMap) {
		super(originalMap);
	}

	/**
	 * This creates a new MyMap with the content of the given map.
	 *
	 * @param originalMap the original map
	 */
	public static <K, V> MyMap<K, V> of(Map<? extends K, ? extends V> originalMap) {
		return new MyMap<>(originalMap);
	}

	/**
	 * This creates a new MyMap with the entries from the given collection.
	 * <code>valueMerger</code> is used to merge values with the same key.
	 *
	 * @param entries a collection of entries to be in the resulting MyMap
	 * @param valueMerger a function for merging values with the same key
	 * @return a new MyMap with the entries from the given collection
	 */
	public static <K, V> MyMap<K, V> ofEntries(Collection<Entry<? extends K, ? extends V>> entries, BinaryOperator<V> valueMerger) {
		var result = new MyMap<K, V>();

		entries.parallelStream().forEach(entry -> {
			var key = entry.getKey();
			var value = entry.getValue();
			if (result.containsKey(key)) {
				result.put(key, valueMerger.apply(result.get(key), value));
			} else {
				result.put(key, value);
		}});

		return result;
	}

	/**
	 * This creates a new MyMap with the entries from the given collection.
	 *
	 * @param entries a collection of entries to be in the resulting MyMap
	 * @return a new MyMap with the entries from the given collection
	 */
	public static <K, V> MyMap<K, V> ofEntries(Collection<Entry<? extends K, ? extends V>> entries) {
		return MyMap.ofEntries(entries, (a, b) -> b);
	}

	public static <K, V> Collector<K, MyMap<K, V>, MyMap<K, V>> keyCollector(Function<K, V> valueMapper) {
		return Collector.of(
			MyMap::new,
			(accumulator, key) -> accumulator.put(key, valueMapper.apply(key)),
			(accumulator, other) -> {
				accumulator.putAll(other);
				return accumulator;
			},
			Characteristics.CONCURRENT, Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED
		);
	}

	public <K2, V2> MyMap<K2, V2> map(
		Function<? super K, ? extends K2> keyMapper,
		Function<? super V,  ? extends V2> valueMapper,
		BinaryOperator<V2> valueMerger
	) {
		var result = new MyMap<K2, V2>();

		this.entrySet().stream().parallel().forEach(entry -> {
			var mapKey = keyMapper.apply(entry.getKey());
			var mapValue = valueMapper.apply(entry.getValue());

			if (result.containsKey(mapKey)) {
				mapValue = valueMerger.apply(result.get(mapKey), mapValue);
			}

			result.put(mapKey, mapValue);
		});

		return result;
	}

	public <K2, V2> MyMap<K2, V2> map(
		Function<? super K, ? extends K2> keyMapper,
		Function<? super V,  ? extends V2> valueMapper
	) {
		return this.map(keyMapper, valueMapper, (a, b) -> b);
	}

	public <K2, V2> MyMap<K2, V2> map(
		BiFunction<? super K, ? super V, ? extends K2> keyMapper,
		BiFunction<? super K, ? super V,  ? extends V2> valueMapper,
		BinaryOperator<V2> valueMerger
	) {
		var result = new MyMap<K2, V2>();

		this.entrySet().stream().parallel().forEach(entry -> {
			var mapKey = keyMapper.apply(entry.getKey(), entry.getValue());
			var mapValue = valueMapper.apply(entry.getKey(), entry.getValue());

			if (result.containsKey(mapKey)) {
				mapValue = valueMerger.apply(result.get(mapKey), mapValue);
			}

			result.put(mapKey, mapValue);
		});

		return result;
	}

	public <K2, V2> MyMap<K2, V2> map(
		BiFunction<? super K, ? super V, ? extends K2> keyMapper,
		BiFunction<? super K, ? super V,  ? extends V2> valueMapper
	) {
		return this.map(keyMapper, valueMapper, (a, b) -> b);
	}

	public <V2> MyMap<K, V2> mapValues(Function<? super V,  ? extends V2> valueMapper) {
		var result = new MyMap<K, V2>();
		this.entrySet().stream().parallel().forEach(
			entry -> result.put(entry.getKey(), valueMapper.apply(entry.getValue()))
		);
		return result;
	}

	public MyMap<K, V> filter(BiPredicate<K, V> predicate) {
		var result = new MyMap<K, V>();

		this.entrySet().stream().parallel().forEach(entry -> {
			if (predicate.test(entry.getKey(), entry.getValue())) {
				result.put(entry.getKey(), entry.getValue());
		}});

		return result;
	}
}
