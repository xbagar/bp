package xyz.matj1.bp.types;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This is a range of Doubles with given boundaries iterable with given step.
 *
 * The iteration starts at <code>from</code>,
 * each other value is greater than the preceding value by <code>step</code>,
 * and ends when the values would reach over <code>to</code>.
 * (So, if <code>step</code> is negative, the iteration will go from <code>from</code>
 * to lower numbers up to <code>to</code>)
 *
 * There is no guarantee of wheter <code>to</code> is a value over which it will be iterated.
 *
 * If <code>step</code> equals 0, the iteration returns <code>from</code> for ever.
 * If a parameter is NaN, the iteration is undefined.
 *
 * @param from the start of the range
 * @param to the end of the range
 * @param step the step of iteration over the range
 */
public record DoubleRange(Double from, Double to, Double step) implements Iterable<Double> {
	@Override
	public Iterator<Double> iterator() {
		return new Iterator<>() {
			private Double state = from;

			@Override
			public boolean hasNext() {
				return this.state <= to;
			}

			@Override
			public Double next() {
				if (!this.hasNext()) {
					throw new NoSuchElementException();
				}

				var result = this.state;
				this.state += step;
				return result;
	}};}

	/**
	 * This converts the range to a stream for easier work.
	 * I don't know if both the stream and the original range can be used,
	 * so I recommend not doing it.
	 *
	 * @return a stream with the values over which it would be iterated.
	 */
	public Stream<Double> stream() {
		return StreamSupport.stream(this.spliterator(), true);
	}
}
