package xyz.matj1.bp.types;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;
import java.util.stream.Stream;

/**
 * This is like a multiset.
 * This doesn't support removing elements.
 *
 * @param <A> the type of the elements
 */
public class Counter<A> implements Collection<A> {
	private MyMap<A, Integer> baseMap;

	public Counter() {
		this.baseMap = new MyMap<>();
	}

	public Counter(Counter<A> basis) {
		this.baseMap = basis.baseMap;
	}

	public Integer get(A key) {
		if (this.baseMap.containsKey(key)) {
			return this.baseMap.get(key);
		}
		return 0;
	}

	public Counter<A> union(Counter<A> other) {
		var result = new Counter<>(this);
		other.distinct().forEach(
			el -> result.baseMap.put(
				el,
				Math.max(
					this.get(el),
					other.get(el))));
		return result;
	}

	public Counter<A> intersection(Counter<A> other) {
		var result = new Counter<>(this);
		result.distinct().forEach(
			el -> result.baseMap.put(
				el,
				Math.min(
					this.get(el),
					other.get(el))));
		return result;
	}

	public Counter<A> sum(Counter<A> other) {
		var result = new Counter<>(this);
		result.addAll(other);
		return result;
	}

	public Counter<A> difference(Counter<A> other) {
		var result = new Counter<A>();
		this.distinct().forEach(
			el -> result.baseMap.put(
				el,
				Math.max(
					this.get(el) - other.get(el),
					0)));
		return result;
	}

	public List<A> items() {
		return this.stream().toList();
	}

	public Set<A> distinct() {
		return this.baseMap.keySet();
	}
	
	@Override
	public int size() {
		return this.baseMap.values().stream().mapToInt(x -> x).sum();
	}

	@Override
	public boolean isEmpty() {
		return this.baseMap.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return this.baseMap.containsKey(o);
	}

	@Override
	public Iterator<A> iterator() {
		return this.stream().iterator();
	}

	@Override
	public Object[] toArray() {
		return this.stream().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return (T[])this.toArray();
	}

	@Override
	public boolean add(A a) {
		this.baseMap.putIfAbsent(a, 0);
		this.baseMap.put(a, this.baseMap.get(a) + 1);
		return true;
	}

	public boolean add(A a, Integer count) {
		this.baseMap.putIfAbsent(a, 0);
		this.baseMap.put(a, this.baseMap.get(a) + count);
		return true;

	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return c.stream().allMatch(this::contains);
	}

	@Override
	public boolean addAll(Collection<? extends A> c) {
		for (var el : c) {
			this.add(el);
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		this.baseMap = new MyMap<>();
	}

	@Override
	public Stream<A> stream() {
		return this.baseMap
			.entrySet().stream()
			.flatMap(
				entry -> Stream.generate(entry::getKey)
					.limit(entry.getValue()));
	}

	/**
	 * This returns a collector to a counter.
	 *
	 * @return a collector of elements into a counter
	 * @param <A> the type of the elements
	 */
	public static <A> Collector<A, Counter<A>, Counter<A>> collector() {
		return Collector.of(
			Counter::new,
			Counter::add,
			(c1, c2) -> {
				c1.addAll(c2);
				return c1;
			},
			Characteristics.CONCURRENT, Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED);
	}

	@Override
	public String toString() {
		return this.toString(Object::toString);
	}

	public String toString(Function<A, String> elementToString) {
		return "Counter" + this.toJson(elementToString);
	}

	private static <K, V> String entryToString(Entry<K, Integer> entry, Function<K, String> elementToString) {
		return String.format(
			"\"%s\": %s",
			elementToString.apply(entry.getKey()),
			entry.getValue());
	}

	/**
	 * This expresses the object as JSON from elements to their counts.
	 * The entries are sorted by counts.
	 *
	 * @param elementToString how to convert elements to strings
	 * @return JSON of the object
	 */
	public String toJson(Function<A, String> elementToString) {
		var builder = new StringBuilder();
		var iterator = this.baseMap.entrySet().stream()
			.sorted(
				Entry.comparingByValue(Comparator.reverseOrder()))
			.toList()
			.iterator();

		builder.append("{");

		if (iterator.hasNext()) {
			builder.append(entryToString(iterator.next(), elementToString));
		}

		iterator.forEachRemaining(
			entry -> builder.append(", ")
				.append(entryToString(entry, elementToString)));

		builder.append("}");

		return builder.toString();
	}
}
