package xyz.matj1.bp.types;

import mcdr.test.utils.ObjectCategoryMgmt.Category;
import xyz.matj1.bp.enums.SpeedMultiple;

import java.util.EnumMap;

/**
 * This stores a map from {@link Category} to a map from {@link SpeedMultiple} to a precision as a {@link Double}.
 * It's intended as a container where the precisions of kNN search
 * in different speeds of the same category are together so they could be sorted.
 */
public class Category_Multiple_Precision extends MyMap<Category, EnumMap<SpeedMultiple, Double>> {
	public Category_Multiple_Precision addPrecision(Category category, SpeedMultiple multiple, Double precision) {
		super.putIfAbsent(category, new EnumMap<>(SpeedMultiple.class));
		super.get(category).put(multiple, precision);
		return this;
	}

	public Double getPrecision(Category category, SpeedMultiple multiple) {
		return super.get(category).get(multiple);
	}
}
