package xyz.matj1.bp.types;

import java.util.Map.Entry;
import java.util.function.Function;

/**
 * A pair of values of the same type
 *
 * @param a the first value
 * @param b the second value
 */
public record Vector2<T>(T a, T b) {
	public String toString() {
		return "(" + a.toString() + ", " + b.toString() + ")";
	}

	public <X> Vector2<X> map(Function<T, X> function) {
		return new Vector2<>(function.apply(a()), function.apply(b()));
	}

	public static <X> Vector2<X> fromEntry(Entry<X, X> entry) {
		return new Vector2<>(entry.getKey(), entry.getValue());
	}
}
