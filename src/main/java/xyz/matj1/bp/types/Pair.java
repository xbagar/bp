package xyz.matj1.bp.types;

import java.io.Serializable;
import java.util.Map.Entry;

/**
 * A pair of values
 *
 * @param a the first value
 * @param b the second value
 */
public record Pair<A, B>(A a, B b) implements Serializable {
	public String toString() {
		return "(" + a.toString() + ", " + b.toString() + ")";
	}

	public static <A, B> Pair<A, B> fromEntry(Entry<A, B> entry) {
		return new Pair<>(entry.getKey(), entry.getValue());
	}
}
