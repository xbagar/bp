package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMocapPoseCoordsL2;
import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import mcdr.test.utils.ObjectCategoryMgmt.Category;
import messif.objects.LocalAbstractObject;
import messif.objects.keys.AbstractObjectKey;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.types.MyMap;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;


/**
 * Třída pro měnění rychlosti motion-capture dat pro účely testování motion words
 *
 * @author Matěj Bagar
 */
public class LinearTimeWarp {
	public static void main(String[] args) throws IOException {
		// load data
		var mocapMgmt = Utils.loadData(
			"data/skeleta/class130-actions-coords_normPOS.data",
			SequenceMocapPoseCoordsL2DTW.class
		);
		var categoryObjects = convertMocap(mocapMgmt.getCategoryObjects());

		// stretching of all sequences to different multiples
		var multiples = Arrays.asList(SpeedMultiple.values());
		multiples.parallelStream().forEach(multiple -> {
			var fileName = String.format(
				"data/skeleta/class130-actions-coords_normPOS_stretched-%s×.data",
				multiple.asString()
			);

			try {
				stretchToFile(
					categoryObjects,
					multiple.asDouble(),
					fileName
				);
			} catch(IOException e) {
				System.err.printf("I can't write to file %s.", fileName);
		}});
	}

	/**
	 * This transforms <code>categoryObjects</code> with <code>stretchCategoryObjects</code>
	 * and writes the result to a file.
	 *
	 * @param categoryObjects a structure the sequences of which are to be stretched
	 * @param multiple how much should the sequences be stretched
	 * @param fileName where the result should be written
	 * @throws IOException if the writing to file fails
	 */
	public static void stretchToFile(
		Map<Category, List<SequenceMocapPoseCoordsL2DTW>> categoryObjects,
		Double multiple,
		String fileName
	) throws IOException {
		// stretching of all sequences
		var categoryStretchedObjects = stretchCategoryObjects(categoryObjects, multiple);

		// save to file
		try(var output = new BufferedOutputStream(
			new FileOutputStream(fileName)
		)) {
			for (var entry : categoryStretchedObjects.entrySet()) {
				for (var sequence : entry.getValue()) {
					sequence.write(output);
	}}}}

	/**
	 * Převede sequence typu LocalAbstractObject v dané struktuře do sequencí typu SequenceMocapPoseCoordsL2DTW.
	 * V podstatě je to casting, ale uzavřený v malé funkci, aby se snadno hledaly chyby.
	 *
	 * @param original původní struktura s pósami typu LocalAbstractObject
	 * @return nová struktura s pósami typu SequenceMocapPoseCoordsL2DTW
	 */
	public static MyMap<Category, List<SequenceMocapPoseCoordsL2DTW>> convertMocap(Map<Category, List<LocalAbstractObject>> original) {
		var convertedMap = new MyMap<Category, List<SequenceMocapPoseCoordsL2DTW>>();
		for (var entry: original.entrySet()) {
			var convertedList = new ArrayList<SequenceMocapPoseCoordsL2DTW>();
			for (var object : entry.getValue()) {
				convertedList.add((SequenceMocapPoseCoordsL2DTW) object);
			}
			convertedMap.put(entry.getKey(), convertedList);
		}
		return convertedMap;
	}

	/**
	 * This creates a sequence of poses, <code>multiple</code>× version of the given one,
	 * by leaving some poses or copying some poses multiple times.
	 *
	 * If <code>multiple</code> is smaller than 1, the result is shorter (quicker) than the original sequence.
	 * If <code>multiple</code> is greater than 1, the result is longer (slower) than the original sequence.
	 * But, if <code>multiple</code> is not [finite and greater than 0], this throws {@link IllegalArgumentException}.
	 *
	 * @param sequence the original sequence
	 * @param multiple how many times slower than the original sequence should the new sequence be
	 * @return stretched version of the original sequence
	 * @throws IllegalArgumentException if <code>multiple</code> is not [finite and greater than 0]
	 */
	public static SequenceMocapPoseCoordsL2DTW stretchSequence(
		SequenceMocapPoseCoordsL2DTW sequence,
		Double multiple
	) throws IllegalArgumentException {
		if (!Double.isFinite(multiple) || multiple <= 0) {
			throw new IllegalArgumentException("The multiple (" + multiple + ") is not both finite and greater than 0.");
		}

		var stretchedSequenceList = new ArrayList<ObjectMocapPoseCoordsL2>();
		var counter = 0.0;

		for (var item : sequence.getObjects()) {
			while (counter < multiple) {
				stretchedSequenceList.add(item);
				counter++;
			}
			counter -= multiple;
		}

		var locatorURIArray = sequence.getLocatorURI().split("_");
		locatorURIArray[3] = Integer.toString((int)Math.ceil(
				Integer.parseInt(locatorURIArray[3]) * multiple
		));
		var newSequence = new SequenceMocapPoseCoordsL2DTW(stretchedSequenceList);
		newSequence.setObjectKey(new AbstractObjectKey(String.join("_", locatorURIArray)));
		return newSequence;
	}

	/**
	 * Tato methoda vytvoří mapu kategorií a seznamů sequencí pós,
	 * kde každá pósa je multiple× delší než ta odpovídající v zadané mapě.
	 * Pokud multiple je menší než 1, výsledné sequence jsou kratší (rychlejší) než původní sequence.
	 * Pokud multiple je větší než 1, výsledné sequence jsou delší (pomalejší) než původní sequence.
	 * Ale pokud multiple je NaN nebo není větší než 0, tato methoda vyhodí IllegalArgumentException.
	 *
	 * @param categoryObjects původní mapa
	 * @param multiple kolikkrát delší než původní sequence má být každá sequence ve výsledné mapě
	 * @return mapa s nataženými sequencemi
	 * @throws IllegalArgumentException pokud multiple je NaN nebo není větší než 0
	 */
	public static Map<Category, List<SequenceMocapPoseCoordsL2DTW>> stretchCategoryObjects(
		Map<Category, List<SequenceMocapPoseCoordsL2DTW>> categoryObjects,
		Double multiple
	) throws IllegalArgumentException {
		return categoryObjects.entrySet().parallelStream().map(
			e -> new SimpleImmutableEntry<>(
				e.getKey(),
				e.getValue().parallelStream().map(
					l -> stretchSequence(l, multiple)
				).collect(Collectors.toList())
		)).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
	}
}
