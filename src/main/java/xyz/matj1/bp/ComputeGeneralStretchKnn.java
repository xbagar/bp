package xyz.matj1.bp;

import xyz.matj1.bp.enums.MocapType;
import xyz.matj1.bp.enums.SpeedMultiple;
import xyz.matj1.bp.experiments.KnnSearchTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ComputeGeneralStretchKnn {
	public static void main(String[] args) throws IOException {
		List<MocapType> types = Arrays.stream(args).collect(
			ArrayList::new,
			(container, item) -> {
				try {
					container.add(MocapType.valueOf(item));
				} catch (IllegalArgumentException ignored) {}
			},
			ArrayList::addAll
		);

		if (types.isEmpty()) {
			types = List.of(MocapType.values());
		}

		List<SpeedMultiple> multiples = Arrays.stream(args).collect(
			ArrayList::new,
			(container, item) -> {
				try {
					container.add(SpeedMultiple.valueOf(item));
				} catch (IllegalArgumentException ignored) {}
			},
			ArrayList::addAll
		);

		if (multiples.isEmpty()) {
			multiples = List.of(SpeedMultiple.values());
		}

		computeKnn(types, multiples);
	}

	static void computeKnn(Collection<MocapType> types, Collection<SpeedMultiple> multiples) throws IOException {
		for (var type : types) {
			for (var multiple : multiples) {
				KnnSearchTools.stretchKnnResults(type, multiple);
	} } }
}
