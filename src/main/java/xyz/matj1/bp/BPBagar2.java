/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMotionWordSoftAssignment;
import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import mcdr.sequence.impl.SequenceMotionWordsDTW;
import mcdr.sequence.impl.SequenceMotionWordsSoftAssignmentDTW;
import messif.objects.LocalAbstractObject;
import xyz.matj1.bp.enums.LengthRelation;
import xyz.matj1.bp.experiments.KnnPrecision;
import xyz.matj1.bp.experiments.PairwiseCorrelation;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

/**
 * @author Matěj Bagar
 */
public class BPBagar2 {
	/**
	 * The method in which other methods run.
	 * What it does exactly changes.
	 *
	 * At this time, I try to have several main functions
	 * so I can keep all functionality.
	 * So it seems that this function is obsolete.
	 *
	 * @param args the command line arguments (ignored)
	 */
	public static void main(String[] args) throws IOException {
		ObjectMotionWordSoftAssignment.maxPartsToMatch = 6;

		interface ShowGraph {
			void show(String dataPath, Class<? extends LocalAbstractObject> dataClass, String outputPath) throws IOException;
		}

		ShowGraph showQuotientGraph = (dataPath, dataClass, outputDirPath) -> {
			PairwiseCorrelation.show(
				Utils.loadData(dataPath, dataClass),
				LengthRelation.QUOTIENT,
				new File(outputDirPath)
			);
		};

		ShowGraph showKnnLengthGraph = (dataPath, dataClass, outputDirPath) -> {
			var timer = Instant.now();
			KnnPrecision.show(
				Utils.loadData(dataPath, dataClass),
				new File(outputDirPath)
			);
			System.err.printf(
				"Time to compute KNN for class %s: %d s%n",
				dataClass.getSimpleName(),
				Duration.between(timer, Instant.now()).toSeconds()
			);
		};

		showQuotientGraph.show(
			"data/hdm05-annotations_specific-segment80_shift16-coords_normPOS-fps12-quantized-pivots-kmedoids-350.data",
			SequenceMotionWordsDTW.class,
			"output/hard MW quotient pairwise graphs"
		);

		showQuotientGraph.show(
			"data/class130-actions-coords_normPOS-fps12.data",
			SequenceMocapPoseCoordsL2DTW.class,
			"output/skeleton quotient pairwise graphs"
		);

		showQuotientGraph.show(
			"data/kmedoids-350-softassign-D20K6.data",
			SequenceMotionWordsSoftAssignmentDTW.class,
			"output/soft MW quotient pairwise graphs"
		);

		System.exit(0);
		// The end of ran procedures
		// The rest is there to be available if it was needed later.

		showKnnLengthGraph.show(
			"data/hdm05-annotations_specific-segment80_shift16-coords_normPOS-fps12-quantized-pivots-kmedoids-350.data",
			SequenceMotionWordsDTW.class,
			"output/hard MW kNN graphs"

		);

		showKnnLengthGraph.show(
			"data/class130-actions-coords_normPOS-fps12.data",
			SequenceMocapPoseCoordsL2DTW.class,
			"output/skeleton kNN graphs"
		);

		showKnnLengthGraph.show(
			"data/kmedoids-350-softassign-D20K6.data",
			SequenceMotionWordsSoftAssignmentDTW.class,
			"output/soft MW kNN graphs"
		);
	}

}
