package xyz.matj1.bp;

import mcdr.objects.impl.ObjectMotionWord;
import mcdr.sequence.SequenceMotionWords;
import mcdr.test.utils.ObjectCategoryMgmt;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.MetaObjectArray;
import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.util.ExportUtils;
import xyz.matj1.bp.exceptions.CacheIOException;
import xyz.matj1.bp.themes.XyGraphTheme;
import xyz.matj1.bp.types.MyMap;
import xyz.matj1.bp.types.Vector2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {
	/**
	 * Loads data for testing motion words (and other sequences) from the file at the given path.
	 *
	 * @param path the given path
	 * @param class_ a class to which the sequences shall be castable
	 * @return an object with motion sequences and their categories
	 * @throws IOException if the file couldn't be read
	 */
	public static ObjectMgmt loadData(
		String path,
		Class<? extends LocalAbstractObject> class_
	) throws IOException {
		var mwCategoryMgmt = new ObjectCategoryMgmt();
		var sequenceMgmt = new ObjectMgmt(mwCategoryMgmt);
		sequenceMgmt.read(class_, path);

		return sequenceMgmt;
	}
	public static ObjectMgmt loadData(
		File path,
		Class<? extends LocalAbstractObject> class_
	) throws IOException {
		return loadData(path.getPath(), class_);
	}

	/**
	 * Returns a string representing the given ObjectMotionWord.
	 *
	 * @param seq the sequence castable to ObjectMotionWord
	 * @return a string representing the input sequence
	 */
	public static String sequenceToString(LocalAbstractObject seq) {
		StringBuilder rtv = new StringBuilder();
		for (ObjectMotionWord mw : ((SequenceMotionWords<? extends ObjectMotionWord>)seq).getSequenceData()) {
			if (rtv.length() > 0) {
				rtv.append(',');
			}
			rtv.append(mw.toString());
		}
		return seq.getLocatorURI() + " [" + rtv + "]";
	}

	/**
	 * Calculates the distance of two categories as the average
	 * of the distances of objects in each tuples from the Cartesian product of given collections.
	 *
	 * @param objectsInCategory1 a collection of the objects in the first category
	 * @param objectsInCategory2 a collection of the objects in the second category
	 * @return the distance of the categories
	 */
	public static Double distanceOfCategories(
		Collection<LocalAbstractObject> objectsInCategory1,
		Collection<LocalAbstractObject> objectsInCategory2
	) {
		ArrayList<Float> objectDistances = new ArrayList<>();
		for (LocalAbstractObject object1 : objectsInCategory1) {
			for (LocalAbstractObject object2 : objectsInCategory2) {
				objectDistances.add(object1.getDistance(object2));
			}
		}
		return averageOfFloats(objectDistances);
	}

	/**
	 * This returns the average of all the values in the input collection.
	 * If the collection is empty, this returns NaN.
	 *
	 * @param collection the input collection
	 * @return average of the values in the input collection
	 */
	public static Double average(Collection<Double> collection) {
		return collection.parallelStream().mapToDouble(x -> x).average().orElse(Double.NaN);
	}

	/**
	 * Returns the average of all the values in the input collection.
	 *
	 * @param collection the input collection
	 * @return average of the values in the input collection
	 */
	public static Double averageOfFloats(Collection<Float> collection) {
		return collection.parallelStream().mapToDouble(x -> (double)x).average().orElse(Double.NaN);
	}

	/**
	 * Converts the given {@link Iterator} to {@link Stream} by putting each object returned from the iterator to the stream.
	 *
	 * @param iterator the given iterator
	 * @return a stream with the values taken from the iterator
	 */
	public static <T> Stream<T> iteratorToStream(Iterator<T> iterator) {
		Stream.Builder<T> builder = Stream.builder();
		iterator.forEachRemaining(builder::add);
		return builder.build();
	}

	/**
	 * Returns the length in poses of the given motion words sequence.
	 *
	 * @param object a motion words sequence
	 * @return the length of object
	 * @throws IllegalArgumentException if the given object isn't a motion words sequence
	 */
	public static int sequenceLengthFromUri(LocalAbstractObject object) throws IllegalArgumentException {
		try {
			return Integer.parseInt(object.getLocatorURI().split("_")[3]);
		} catch (IndexOutOfBoundsException | NumberFormatException e) {
			throw new IllegalArgumentException("The given object probably isn't a motion words sequence", e);
		}
	}

	public static int sequenceLength(LocalAbstractObject object) {
		if (object instanceof MetaObjectArray array) {
			return array.getObjectCount();
		}

		throw new IllegalArgumentException(object + " isn't a sequence (instance of SequenceMocap or SequenceMotionWords).");
	}

	/**
	 * This normalises a <code>value</code> in a way that,
	 * if its absolute value is smaller than 1, this returns 1/<code>value</code>,
	 * otherwise it returns <code>value</code>.
	 *
	 * @param value a value to be normalised
	 * @return <code>value</code> normalised
	 */
	public static double normaliseQuotient(double value) {
		return (Math.abs(value) < 1)
			? 1 / value
			: value;
	}

	/**
	 * This prints given string to given writers.
	 *
	 * @param data what shall be printed
	 * @param outputs where it shall be printed
	 * @throws IOException if writing to a given writer throws this exception
	 */
	public static void write(String data, Writer... outputs) throws IOException {
		for (var output : outputs) {
			output.write(data);
			output.flush();
		}
	}

	/**
	 * This creates a scatter-plot graph with the default theme
	 * ({@link XyGraphTheme}) and given format.
	 * The format of the output is PDF.
	 * This may changes the given <code>chart</code>.
	 *
	 * @param chart a chart containing the data for the graph
	 * @param output where the graph shall be saved
	 * @param size the width and the height of the output (in pixels)
	 */
	public static void writeXYGraphAsPdf(
		JFreeChart chart,
		File output,
		Vector2<Integer> size
	) {
		writeXYGraphAsPdf(chart, output, size, XyGraphTheme.instance());
	}

	/**
	 * This creates a scatter-plot graph with a given theme and format.
	 * The format of the output is PDF.
	 * This changes the given <code>chart</code>.
	 *
	 * @param chart a chart containing the data for the graph
	 * @param output where the graph shall be saved
	 * @param size the width and the height of the output (in pixels)
	 */
	public static void writeXYGraphAsPdf(
		JFreeChart chart,
		File output,
		Vector2<Integer> size,
		ChartTheme theme
	) {
		theme.apply(chart);

		assert ExportUtils.isOrsonPDFAvailable();
		ExportUtils.writeAsPDF(chart, size.a(), size.b(), output);
	}

	/**
	 * This superimposes all datasets from the charts in <code>charts</code> to a single chart.
	 * So this adds all datasets from XYPlots of the charts to a single chart and returns the chart.
	 *
	 * The returned chart is a clone of the first chart from the iterator of <code>charts</code>.
	 * But, if it doesn't support cloning, the returned chart is the first chart from the iterator of <code>charts</code>.
	 * That means that the chart is changed
	 *
	 * @param charts a nonempty collection of charts
	 * @return a clone (or maybe original) of the first chart with added datasets of all other charts
	 * @throws IllegalArgumentException if <code>charts</code> is empty
	 */
	public static JFreeChart superimpose(Collection<JFreeChart> charts) {
		if (charts.isEmpty()) {
			throw new IllegalArgumentException("The collection of charts is empty.");
		}

		var chartIterator = charts.iterator();
		var firstChart = chartIterator.next();

		JFreeChart tempNewChart;
		try {
			tempNewChart = (JFreeChart)firstChart.clone();
		} catch (CloneNotSupportedException e) {
			tempNewChart = firstChart;
		}
		final var newChart = tempNewChart;

		var newPlot = newChart.getXYPlot();

		chartIterator.forEachRemaining(chart -> {
			var plot = chart.getXYPlot();
			for (int i = 0; i < plot.getDatasetCount(); i++) {
				newPlot.setDataset(newPlot.getDatasetCount(), plot.getDataset(i));
		}});

		return newChart;
	}

	/**
	 * This tries to read cache from the file on the given path and return them cast to <code>C</code>.
	 *
	 * @param cachePath a path to the file where to read from
	 * @param dataType the expected data type of read cache
	 * @param <C> the expected data type of read cache or its extension or implementation
	 * @return the data in the cache
	 * @throws CacheIOException if reading the file fails or if its data aren't serialisation of an object of type C
	 */
	public static <C> C readCache(String cachePath, Class<C> dataType) throws CacheIOException {
		return readCache(new File(cachePath), dataType);
	}

	/**
	 * This tries to read cache from the given file and return them cast to <code>C</code>.
	 *
	 * @param cacheFile where to read from
	 * @param dataType the expected data type of read cache
	 * @param <C> the expected data type of read cache or its extension or implementation
	 * @return the data in the cache
	 * @throws CacheIOException if reading the file fails or if its data aren't serialisation of an object of type C
	 */
	public static <C> C readCache(File cacheFile, Class<? extends C> dataType) throws CacheIOException {
		C data;
		try {
			var deserialiser = new ObjectInputStream(new FileInputStream(cacheFile));
			data = (C)deserialiser.readObject();
			deserialiser.close();
			System.err.printf("Cached data from %s were read successfully.%n", cacheFile);
			return data;
		} catch (IOException | ClassNotFoundException e) {
			System.err.printf(
				"There was an attempt to read cache from %s, but it wasn't read and an exception occurred:%n%s%n",
				cacheFile.getPath(), e
			);
			throw new CacheIOException(e);
	}}

	/**
	 * This writes serialises <code>data</code> and writes the result to the file at <code>cachePath</code>.
	 *
	 * @param cachePath a path to the file where to write the cache
	 * @param data the data to be cached
	 * @throws CacheIOException if the writing fails or the data can't be serialised
	 */
	public static void writeCache(String cachePath, Object data) throws CacheIOException {
		writeCache(new File(cachePath), data);
	}

	public static void writeCache(File cacheFile, Object data) throws CacheIOException {
		try {
			var serialiser = new ObjectOutputStream(new FileOutputStream(cacheFile));
			serialiser.writeObject(data);
			serialiser.close();
			System.err.printf("Data cache was written to %s.%n", cacheFile);
		} catch (IOException e) {
			System.err.printf(
				"There was an attempt to write cache to %s, but it wasn't written and an exception occurred:%n%s%n",
				cacheFile.getPath(), e
			);
			throw new CacheIOException(e);
		}
	}

	public static <T> Collector<T, PriorityQueue<T>,List<T>> smallestK(Integer k, Comparator<? super T> comparator) {
		return Collector.of(
			() -> new PriorityQueue<>(k + 1, comparator),
			(queue, element) -> {
				queue.add(element);
				if (queue.size() > k) {
					queue.poll();
			} },
			(queue1, queue2) -> {
				queue1.addAll(queue2);
				while (queue1.size() > k)
					queue1.poll();
				return queue1;
			},
			queue -> {
				var result = new ArrayList<T>(k);
				for (int i = 0; i < k; i++) {
					result.add(queue.poll());
				}
				return result;
			},
			Collector.Characteristics.UNORDERED);
	}

	/**
	 * This creates a collector for collecting entries into a LinkedHashMap.
	 * The ordering of the entries in the map is that of the entries in the original stream.
	 *
	 * @return a collector for collecting entries to a MyMap
	 * @param <K> the type of the keys
	 * @param <V> the type of the values
	 */
	public static <K, V> Collector<Entry<K, V>, MyMap<K, V>, MyMap<K, V>> entryCollector() {
		return Collector.of(
			MyMap::new,
			(collector, entry) -> collector.put(entry.getKey(), entry.getValue()),
			(collector, other) -> {
				collector.putAll(other);
				return collector;
			},
			Characteristics.CONCURRENT, Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED
		);
	}

	/**
	 * This creates a collector for collecting entries into a LinkedHashMap.
	 * The ordering of the entries in the map is that of the entries in the original stream.
	 *
 	 * @return a collector for collecting entries to a LinkedHashMap
	 * @param <K> the type of the keys
	 * @param <V> the type of the values
	 */
	public static <K, V> Collector<Entry<K, V>, LinkedHashMap<K, V>, LinkedHashMap<K, V>> entryCollectorToLinkedHashMap() {
		return Collector.of(
			LinkedHashMap::new,
			(collector, entry) -> collector.put(entry.getKey(), entry.getValue()),
			(collector, other) -> {
				collector.putAll(other);
				return collector;
			},
			Characteristics.IDENTITY_FINISH
		);
	}

	/**
	 * This calculates the variance of data in a collection.
	 *
	 * @param data the collection, with at least 1 element
	 * @return the variance of the given data
	 */
	public static Double variance(Collection<? extends Number> data) {
		if (data.size() < 1) {
			throw new IllegalArgumentException("The data have to contain at least 2 elements.");
		}

		var average = data.stream()
			.mapToDouble(Number::doubleValue)
			.average().orElseThrow();
		return data.stream()
			.mapToDouble(value -> Math.abs(value.doubleValue() - average))
			.map(x -> x*x)
			.average().orElseThrow();
	}

	public static File composeFile(File origin, String... children) {
		var others = Arrays.stream(children).map(Path::of).reduce(Path.of(""), Path::resolve);
		return origin.toPath().resolve(others).toFile();
	}

	/**
	 * This takes objects of type A which may be null, and returns the first object that is not null.
	 * But, if there is no given non-null object, it returns null.
	 *
	 * By the way, I realised too late that this still evaluates the parameters after the first non-null,
	 * so this isn't useful for what I imagined, that is taking the first valid result and ignore the others.
	 *
	 * @param values objects that may be null
	 * @return the first non-null object or null
	 * @param <A> the type of the objects
	 */
	@SafeVarargs
	public static <A> A or(A... values) {
		return Arrays.stream(values).filter(Objects::nonNull).findFirst().orElse(null);
	}

	/**
	 * This creates a set of all subsets of <code>input</code>
	 * the size of which is at least <code>minSize</code>.
	 *
	 * This is to be used only with small collections because it can result
	 * in a very large set with a large collection as <code>input</code>.
	 * The size is up to 2^(the size of the input)
	 *
	 * @param input the original collection
	 * @param minSize minimal size of the subcollections
	 * @return a collection of all subcollections
	 * @param <A> the type of the elements of the collection
	 */
	public static <A> Collection<? extends Collection<A>> subsets(Collection<A> input, Integer minSize) {
		var inputSize = input.size();
		var inputList = new ArrayList<>(input);
		var result = new ArrayList<ArrayList<A>>();

		for (int a = 0; a < Math.pow(2, inputSize); a++) {
			var indices = leftPad(Integer.toBinaryString(a), '0', inputSize)
				.chars().mapToObj(c -> c == '1').toList();
			if (indices.stream().filter(bool -> bool).count() < minSize) {
				continue;
			}

			var subset = new ArrayList<A>();
			for (int b = 0; b < inputSize; b++) {
				if (indices.get(b)) {
					subset.add(inputList.get(b));
			} }
			result.add(subset);
		}
		return result;
	}

	public static <A> Collection<? extends Collection<A>> subsetsWith(Collection<A> input, A required) {
		var inputSize = input.size();
		var inputList = new ArrayList<>(input);
		var result = new ArrayList<ArrayList<A>>();

		for (int a = 0; a < Math.pow(2, inputSize); a++) {
			var indices = leftPad(Integer.toBinaryString(a), '0', inputSize)
				.chars().mapToObj(c -> c == '1').toList();

			var subset = new ArrayList<A>();
			for (int b = 0; b < inputSize; b++) {
				if (indices.get(b)) {
					subset.add(inputList.get(b));
			} }

			if (subset.contains(required)) {
				result.add(subset);
			}
		}
		return result;
	}

	public static String leftPad(String original, Character padder, Integer length) {
		var originalLength = original.length();
		if (originalLength >= length) {
			return original;
		}

		var padding = Stream.generate(() -> padder.toString()).limit(length - originalLength).collect(Collectors.joining());
		return padding + original;
	}
}
