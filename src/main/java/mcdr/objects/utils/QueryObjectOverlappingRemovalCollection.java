package mcdr.objects.utils;

import mcdr.test.utils.ObjectMgmt;
import messif.objects.AbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.RankedSortedCollection;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class QueryObjectOverlappingRemovalCollection extends RankedSortedCollection implements InstantiableCollection {

    // query object parent sequence id
    private final String queryObjectParentSequenceId;
    // query object start index
    private final int queryObjectStartIndex;
    // query object end index
    private final int queryObjectEndIndex;

    //************ Constructors ************//
    /**
     * Creates a new instance of
     * {@link QueryObjectOverlappingRemovalCollection}.
     *
     * @param queryObject query object
     */
    public QueryObjectOverlappingRemovalCollection(AbstractObject queryObject) {
        this.queryObjectParentSequenceId = ObjectMgmt.parseObjectParentSequenceId(queryObject);
        this.queryObjectStartIndex = ObjectMgmt.parseObjectOffset(queryObject);
        this.queryObjectEndIndex = this.queryObjectStartIndex + ObjectMgmt.parseObjectLength(queryObject) - 1;
    }

    //************ Overrided class RankedSortedCollection ************//
    /**
     * Adds the object in case it does not overlap with the query object.
     *
     * @param e object to be added
     * @return true if the object is added, i.e., it is not a query object
     */
    @Override
    public boolean add(RankedAbstractObject e) {
        final String oParentSequenceId = ObjectMgmt.parseObjectParentSequenceId(e.getObject());
        final int oStartIndex = ObjectMgmt.parseObjectOffset(e.getObject());
        final int oEndIndex = oStartIndex + ObjectMgmt.parseObjectLength(e.getObject()) - 1;

        // If the query object overlaps with the object to be added, the object is not added
        if (queryObjectParentSequenceId.equals(oParentSequenceId)
                && Math.max(queryObjectStartIndex, oStartIndex) <= Math.min(queryObjectEndIndex, oEndIndex)) {
            return false;
        }

        return super.add(e);
    }

    //************ Overrided class RankedSortedCollection ************//
    @Override
    public QueryObjectOverlappingRemovalCollection instantiate(AbstractObject queryObject) {
        return new QueryObjectOverlappingRemovalCollection(queryObject);
    }
}
