package mcdr.objects.utils;

import java.util.Iterator;
import messif.objects.AbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.RankedSortedCollection;
import messif.operations.RankingMultiQueryOperation;
import messif.operations.RankingQueryOperation;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class QueryObjectRemovalCollection extends RankedSortedCollection {

    // query objects
    private final AbstractObject[] queryObjects;

    //************ Constructors ************//
    /**
     * Creates a new instance of {@link QueryObjectRemovalCollection}.
     *
     * @param queryObjectIterator iterator over query objects
     */
    public QueryObjectRemovalCollection(Iterator<? extends AbstractObject> queryObjectIterator) {
        this.queryObjects = RankingMultiQueryOperation.toObjectArray(queryObjectIterator);
    }

    //************ Static methods ************//
    /**
     * Sets a new collection that maintains the answer list of the ranking
     * query. Note that this method should be used only for changing the
     * re-ranking/filtering of the results.
     *
     * @param operation operation whose answer collection is being replaced
     * @param collection a new instance of answer collection
     * @return the ranking query with a replaced answer collection
     */
    public static RankingQueryOperation setAnswerCollection(RankingQueryOperation operation, RankedSortedCollection collection) {
        operation.setAnswerCollection(collection);
        return operation;
    }

    //************ Overrided class RankedSortedCollection ************//
    /**
     * Adds the object in case it is not a query object.
     *
     * @param e object to be added
     * @return true if the object is added, i.e., it is not a query object
     */
    @Override
    public boolean add(RankedAbstractObject e) {
        String locator = e.getObject().getLocatorURI();
        for (AbstractObject o : queryObjects) {
            if (locator.equals(o.getLocatorURI())) {
                return false;
            }
        }
        return super.add(e);
    }
}
