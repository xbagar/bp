package mcdr.objects.utils;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import mcdr.test.utils.ObjectMgmt;
import messif.objects.AbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.RankedSortedCollection;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class OverlappingAnnotationRemovalCollection extends RankedSortedCollection {

    //************ Constructors ************//
    /**
     * Creates a new instance of {@link OverlappingAnnotationRemovalCollection}.
     *
     */
    public OverlappingAnnotationRemovalCollection() {
    }

    //************ Overrided class RankedSortedCollection ************//
    /**
     * Adds the object in case it either is not overlapping with another object,
     * or has a smaller distance than all overlapping objects that are removed
     * from the answer.
     *
     * @param e object to be added
     * @return true if the object is added
     */
    @Override
    public boolean add(RankedAbstractObject e) {
        Set<String> overlappingObjectLocators = new HashSet<>();
        int eOffset = ObjectMgmt.parseObjectOffset(e.getObject());
        int eLength = ObjectMgmt.parseObjectLength(e.getObject());

        Iterator<RankedAbstractObject> objIt = iterator();
        while (objIt.hasNext()) {
            RankedAbstractObject rao = objIt.next();
            AbstractObject o = rao.getObject();

            int oOffset = ObjectMgmt.parseObjectOffset(o);
            int oLength = ObjectMgmt.parseObjectLength(o);

            if (Math.max(eOffset, oOffset) <= Math.min(eOffset + eLength - 1, oOffset + oLength - 1)) {
                if (rao.getDistance() <= e.getDistance()) {
                    return false;
                } else {
                    overlappingObjectLocators.add(o.getLocatorURI());
                }
            }
        }

        // Removes the overlapping objects
        if (!overlappingObjectLocators.isEmpty()) {
            objIt = iterator();
            while (objIt.hasNext()) {
                RankedAbstractObject rao = objIt.next();
                if (overlappingObjectLocators.contains(rao.getObject().getLocatorURI())) {
                    objIt.remove();
                }
            }
        }
        return super.add(e);
    }
}
