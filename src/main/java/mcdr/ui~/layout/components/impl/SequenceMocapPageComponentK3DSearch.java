package mcdr.ui.layout.components.impl;

import java.awt.Color;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.servlet.jsp.JspWriter;
import mcdr.objects.ObjectMocapPose;
import mcdr.sequence.SequenceMocap;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class SequenceMocapPageComponentK3DSearch extends SequenceMocapPageComponent {

    //************ Attributes ************//
    // indicates whether to support retrieval
    protected final boolean supportRetrieval;
    // indicates whether to support classification
    protected final boolean supportClassification;
    // map associating the sequence id with the subject id
    protected final Map<String, String> sequenceToSubjectIdMap;
    // map associating the sequence id with its class
    protected final Map<String, String> sequenceToClassMap;

    //************ Constructors ************//
    /**
     * Creates a new instance of {@link SequenceMocapPageComponentSearch}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param fps data sampling frequency (frames per second)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     */
    public SequenceMocapPageComponentK3DSearch(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap) {
        super(canvasWidth, canvasHeight, fps, numberOfSkippedFramesToDisplay, datasets, sequenceToDatasetMap);
        this.supportRetrieval = true;
        this.supportClassification = false;
        this.sequenceToSubjectIdMap = null;
        this.sequenceToClassMap = null;
    }

    /**
     * Creates a new instance of {@link SequenceMocapPageComponentSearch}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param fps data sampling frequency (frames per second)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     * @param sequenceToSubjectIdMap map associating sequence id with the
     * corresponding subject id
     */
    public SequenceMocapPageComponentK3DSearch(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap, Map<String, String> sequenceToSubjectIdMap) {
        this(canvasWidth, canvasHeight, fps, numberOfSkippedFramesToDisplay, datasets, sequenceToDatasetMap, true, false, sequenceToSubjectIdMap, null);
    }

    /**
     * Creates a new instance of {@link SequenceMocapPageComponentSearch}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param fps data sampling frequency (frames per second)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     * @param supportRetrieval indicates whether to support retrieval
     * @param supportClassification indicates whether to support classification
     * @param sequenceToSubjectIdMap map associating sequence id with the
     * corresponding subject id
     * @param sequenceToClassMap map associating the sequence id with its class
     */
    public SequenceMocapPageComponentK3DSearch(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap, boolean supportRetrieval, boolean supportClassification, Map<String, String> sequenceToSubjectIdMap, Map<String, String> sequenceToClassMap) {
        super(canvasWidth, canvasHeight, fps, numberOfSkippedFramesToDisplay, datasets, sequenceToDatasetMap);
        this.supportRetrieval = supportRetrieval;
        this.supportClassification = supportClassification;
        this.sequenceToSubjectIdMap = sequenceToSubjectIdMap;
        this.sequenceToClassMap = sequenceToClassMap;
    }

    //************ Methods ************//
    /**
     * Generates the sequence canvas and writes its to the JSP writer.
     *
     * @param out JSP writer
     * @param o sequence to be visualized
     * @param dist ranked sequence distance (can be <code>null</code>)
     * @throws IOException if there was an error writing to the page output
     */
    @Override
    protected void generateCanvas(JspWriter out, SequenceMocap<ObjectMocapPose> o, Float dist) throws IOException {
        final String locator = o.getOriginalSequenceLocator();
        final String objId = locator + "_" + Math.abs(o.hashCode());
        final String sequenceId = (o.getSequenceId() == null) ? UUID.randomUUID().toString() : o.getSequenceId();
        final boolean enableRetrieval = o.getOffset() != -1 || sequenceId.equals(locator);
        DatasetDefinition sequenceDataset = sequenceToDatasetMap.get(o.getSequenceId());
        if (sequenceDataset == null) {
            sequenceDataset = sequenceToDatasetMap.get(null);
        }

        // Distance
        if (dist != null) {
            out.write("        <div class=\"relevanceScore\">Similarity distance: " + dist + "</div>\n");
        }

        out.write("<div class=\"resultCanvas\">\n");
        out.write("    <canvas id=\"canvas" + objId + "\" width=\"" + canvasWidth + "\" height=\"" + canvasHeight + "\">HTML5 Canvas not supported</canvas>\n");
        out.write("    <script type=\"text/javascript\">\n");
        out.write("        fps = " + fps + ";\n");
        out.write("        numberOfSkippedFramesToDisplay = " + numberOfSkippedFramesToDisplay + ";\n");
        out.write("        var canvas" + objId + " = document.getElementById(\"canvas" + objId + "\");\n");
        out.write("        function onCanvasMouseMove" + objId + "(event) {\n");
        out.write("            event.preventDefault();\n");
        out.write("            var sliderId = \"#slider-range" + objId + "\";\n");
        out.write("            if (rotating && !$( sliderId ).slider( \"option\", \"animationInProgress\")) {\n");
        out.write("                var diffx = px - event.pageX;\n");
        out.write("                var diffy = py - event.pageY;\n");
        out.write("                px = event.pageX;\n");
        out.write("                py = event.pageY;\n");
        out.write("                for (key in frames" + objId + ") {\n");
        out.write("                    frames" + objId + "[key].ophi = frames" + objId + "[key].ophi + diffx;\n");
        out.write("                    frames" + objId + "[key].ogamma = frames" + objId + "[key].ogamma + diffy;\n");
        out.write("                }\n");
        out.write("                showFrame(k3d" + objId + ", frames" + objId + ", $(\"#slider-range" + objId + "\").slider(\"option\", \"value\"));\n");
        out.write("            }\n");
        out.write("        }\n");

        // K3D Controller is responsible for managing the K3D objects displayed within the canvas
        out.write("        var k3d" + objId + " = new K3D.Controller(canvas" + objId + ", true);\n");
        out.write("        var frames" + objId + " = new Array();\n");

        // Moves the lowest y-coordinate to the ground
        float lowestYCoord = Float.MAX_VALUE;
        for (int f = 0; f < o.getObjectCount(); f += numberOfSkippedFramesToDisplay + 1) {
            float[][] jointCoordinates = o.getObject(f).getJointCoordinates();
            for (float[] jointCoordinate : jointCoordinates) {
                if (lowestYCoord > jointCoordinate[1]) {
                    lowestYCoord = jointCoordinate[1];
                }
            }
        }
        lowestYCoord = -lowestYCoord - (canvasHeight / 2 - 10) / sequenceDataset.skeletonEnlargement;

        // Constructs skeletons for selected frames
        Color seqColor = getSequenceColor(sequenceId, o.getSequenceId() != null);
        String coordsPrecisionStr = Integer.toString(Math.max(1, Math.round(sequenceDataset.skeletonEnlargement * 0.04f)));
        for (int f = 0; f < o.getObjectCount(); f += numberOfSkippedFramesToDisplay + 1) {
            out.write("        frames" + objId + "[" + f + "] = new K3D.K3DObject();\n");

            // Normalizes the skeleton coordinates
            ObjectMocapPose obj = o.getObject(f);
            float[][] jointCoordinates = normalize(obj, lowestYCoord);

            // Visualizes the skeleton
            out.write("        with (frames" + objId + "[" + f + "]) {\n");
            out.write("            color = [" + seqColor.getRed() + "," + seqColor.getGreen() + "," + seqColor.getBlue() + "];\n");
            out.write("            drawmode = \"wireframe\";\n");
            out.write("            shademode = \"depthcue\";\n");
            out.write("            scale = " + String.format(Locale.US, "%.1f", sequenceDataset.skeletonEnlargement) + ";\n");
            out.write("            init(\n");

            // Individual landmarks
            out.write("                [");
            for (int i = 0; i < jointCoordinates.length; i++) {
                if (i > 0) {
                    out.write(",");
                }
                out.write(String.format(Locale.US, "{x:%1$." + coordsPrecisionStr + "f,y:%2$." + coordsPrecisionStr + "f,z:%3$." + coordsPrecisionStr + "f}", jointCoordinates[i][0], jointCoordinates[i][1], jointCoordinates[i][2]));
            }
            out.write("],\n");

            // Individual bones
            out.write("                " + sequenceDataset.skeletonModelName + ",\n");

            // Polygon faces of the cube
            out.write("                []\n");
            out.write("            );\n");
            out.write("        }\n");

            // Displayes the first frame
            if (f == 0) {
                out.write("        showFrame(k3d" + objId + ", frames" + objId + ", 0);\n");
            }
        }

        // Functions handling the skeleton rotation
        out.write("        $(\"#canvas" + objId + "\").mousedown(onCanvasMouseDown);\n");
        out.write("        $(\"#canvas" + objId + "\").mousemove(onCanvasMouseMove" + objId + ");\n");
        out.write("        $(\"#canvas" + objId + "\").mouseup(onCanvasMouseUp);\n");
        out.write("        $(\"#canvas" + objId + "\").mouseleave(onCanvasMouseUp);\n");
        out.write("    </script>\n");
        out.write("</div>\n");

        // Whole sequence play button
        int sequenceMaxframe = (o.getObjectCount() / (numberOfSkippedFramesToDisplay + 1)) * (numberOfSkippedFramesToDisplay + 1);

        // Actions
        generateActionCanvas(out, o, sequenceId, objId, locator, enableRetrieval, sequenceDataset);

        // Player
        out.write("<div class=\"sequencePlayer\">\n");
        out.write("    <div class=\"button play-sequence\" id=\"play" + objId + "\"></div>\n");
        out.write("    <div class=\"sliderWrapper\">\n");
        out.write("        <div id=\"slider-range" + objId + "\"  style=\"width: " + (sequenceMaxframe / fps * 60) + "px; margin: 10px 10px 0px 9px;\"></div>\n");
        out.write("        <div style=\"width: " + (sequenceMaxframe / fps * 60 + 60) + "px; margin: 0px 10px 0px 9px;\">\n");
        out.write("            <span class=\"timeSlot-firstSecond\">0:00</span>\n");
        for (int i = 1; i <= o.getObjectCount() / fps; i++) {
            out.write("            <span class=\"timeSlot-anotherSecond\">" + (i / 60) + ":" + String.format("%02d", i % 60) + "</span>\n");
        }
        out.write("        </div>\n");
        if (enableRetrieval) {
            out.write("        <script type=\"text/javascript\">\n$(function() { $( \"#slider-range" + objId + "\" ).slider({range: true, min: 0, max: " + sequenceMaxframe + ", step: " + (numberOfSkippedFramesToDisplay + 1) + ", minDiff: minDiff, lastDiff: minDiff, sequenceId: '" + objId + "', stopAnimation: false, animationInProgress: false, prePlayPosition: [0, minDiff], values: [0, minDiff], canvas: k3d" + objId + ", frames: frames" + objId + ", slide: slide}); });\n</script>\n");
        } else {
            out.write("        <script type=\"text/javascript\">\n$(function() { $( \"#slider-range" + objId + "\" ).slider({range: false, min: 0, max: " + sequenceMaxframe + ", step: " + (numberOfSkippedFramesToDisplay + 1) + ", sequenceId: '" + objId + "', stopAnimation: false, animationInProgress: false, prePlayPosition: 0, value: 0, canvas: k3d" + objId + ", frames: frames" + objId + ", slide: slideWithoutRange}); });\n</script>\n");
        }
        out.write("    </div>\n");
        out.write("</div>\n");
    }

    /**
     * Generates the action canvas and writes its to the JSP writer.
     *
     * @param out JSP writer
     * @param o visualized sequence object
     * @param sequenceId ID of the visualized sequence
     * @param objId internal ID of the visualized (sub)sequence
     * @param locator locator string of the visualized sequence
     * @param enableRetrieval indicates whether the search action is enabled
     * @param sequenceDataset
     * @throws IOException if there was an error writing to the page output
     */
    protected void generateActionCanvas(JspWriter out, SequenceMocap<ObjectMocapPose> o, String sequenceId, String objId, String locator, boolean enableRetrieval, DatasetDefinition sequenceDataset) throws IOException {
        out.write("<div class=\"resultAction\">\n");
        out.write("    <div class=\"resultSection\">\n");
        out.write("        <div class=\"boldFont\">Motion properties:</div>\n");

        // Sequence name
        String aTagParams = "<a href=\"sequence?sequenceLocator=" + sequenceId + "\" onclick=\"javascript:appendLinkParams(this);\" >" + sequenceId + "</a>";
        if (o.getOffset() == -1 && sequenceId.equals(locator)) {
            out.write("        <div class=\"resultMotionLink\">Sequence ID: " + aTagParams + "</div>\n");
        } else {
            String subMotionPosition;
            if (o.getOffset() == -1) {
                subMotionPosition = "unknown range";
            } else {
                subMotionPosition = o.getOffset() + "&ndash;" + (o.getOffset() + o.getSequenceLength());
            }
            out.write("        <div class=\"resultMotionLink\">Subseq.: " + subMotionPosition + " frames</div>\n");
            out.write("        <div class=\"resultMotionLink\">Original sequence ID: " + aTagParams + "</div>\n");
        }
        out.write("        <div class=\"resultMotionLink\">Dataset: " + sequenceDataset.name + "</div>\n");

        // Subject name
        out.write("        <div class=\"resultMotionLink\">Person ID: " + (sequenceToSubjectIdMap == null ? "&lt;unknown&gt;" : sequenceToSubjectIdMap.get(sequenceId)) + "</div>\n");

        // Class name
        if (supportClassification && sequenceToClassMap != null) {
            String sequenceClass = sequenceToClassMap.get(o.getLocatorURI());
            if (sequenceClass != null) {
                out.write("        <div class=\"resultMotionLink boldFont\">Class: " + (sequenceClass == null ? "&lt;unknown&gt;" : sequenceClass) + "</div>\n");
            }
        }
        out.write("    </div>\n");

        // Searching
        if (enableRetrieval) {

            // Query selection
            out.write("    <div class=\"resultSection\">\n");
            out.write("        <div class=\"boldFont\">Query selection:</div>\n");
            out.write("        <span class=\"button play-selection\" id=\"playSelection" + objId + "\"></span>\n");
            out.write("        <div class=\"queryRange\">\n");
            out.write("                <span id=\"rangeFrom" + objId + "\">0</span>&ndash;<span id=\"rangeTo" + objId + "\">100</span> frames\n");
            out.write("        </div>\n");

            // Retrieval link
            if (supportRetrieval) {
                out.write("        <div class=\"resultMotionLink\">\n");
                out.write("            <a id=\"searchLink" + objId + "\" href=\"#\" onclick=\"javascript:setSubsequenceLink('searchLink" + objId + "', " + locator + ", " + Math.max(0, o.getOffset()) + ", 'rangeFrom" + objId + "', 'rangeTo" + objId + "'); appendLinkParams(this);\">Search for similar subsequences!</a>\n");
                out.write("        </div>\n");
            }

            // Classification link
            if (supportClassification) {
                out.write("        <div class=\"resultMotionLink\">\n");
                out.write("            <a id=\"classifyLink" + objId + "\" href=\"#\" onclick=\"javascript:setSubsequenceLinkAction('classifyLink" + objId + "', " + locator + ", " + Math.max(0, o.getOffset()) + ", 'rangeFrom" + objId + "', 'rangeTo" + objId + "', 'classify'); appendLinkParams(this);\">Classify subsequence!</a>\n");
                out.write("        </div>\n");
            }

            out.write("    </div>\n");
        }
        out.write("</div>\n");
    }
}
