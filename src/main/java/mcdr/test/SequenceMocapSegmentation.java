package mcdr.test;

import mcdr.preprocessing.segmentation.impl.RegularSegmentConvertor;
import mcdr.preprocessing.transformation.impl.FPSConvertor;
import mcdr.sequence.SequenceMocap;
import mcdr.sequence.impl.SequenceMocapPoseCoordsL2DTW;
import messif.objects.LocalAbstractObject;
import messif.objects.keys.AbstractObjectKey;
import messif.objects.util.StreamGenericAbstractObjectIterator;

import java.io.FileOutputStream;
import java.util.List;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class SequenceMocapSegmentation {
    /**
     * @param args the command line arguments
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        // HDM05
        final int originalSegmentFPS = 120;
        final int convertedSegmentFPS = 12;
        final int segmentSize = 80;
        final float segmentShiftRatio = 0.2f;
        final int segmentShiftInitial = 0;

        // data params
        final Class<? extends SequenceMocap<?>> sequenceClass = SequenceMocapPoseCoordsL2DTW.class;

        // HDM05
        final String sequenceFile = "data/skeleta/class130-actions-coords_normPOS_length-equalised-double.data";
        final String outputFeatureFile = "data/segmented/class130-actions-length-equalised-double-segment" + segmentSize + "_shift" + (segmentSize * segmentShiftRatio) + "-coords_normPOS-fps12" + ".data";

        // Transforming the sequences
        FileOutputStream fos = new FileOutputStream(outputFeatureFile);
        StreamGenericAbstractObjectIterator sequenceIterator = new StreamGenericAbstractObjectIterator<>(sequenceClass, sequenceFile);
        int sequenceCount = 0;
        int segmentCount = 0;
        while (sequenceIterator.hasNext()) {
            SequenceMocap<?> sequence = (SequenceMocap<?>) sequenceIterator.next();

            FPSConvertor fpsConvertor = new FPSConvertor(sequenceClass, originalSegmentFPS, convertedSegmentFPS);
            RegularSegmentConvertor segmentProcessor = new RegularSegmentConvertor<>(sequenceClass, segmentSize, segmentShiftRatio, segmentShiftInitial, false);
            List<SequenceMocap<?>> segments = segmentProcessor.convert(sequence);

            // If the sequence is too short for segmentation, the whole sequence is considered as a single segment
            if (segments.isEmpty()) {
                segments.add(sequence);
            }

            segmentCount += segments.size();
            int seqSegmentNo = 0;
            for (SequenceMocap<?> segment : segments) {
                var locatorURI = sequence.getLocatorURI(); // for debugging
                AbstractObjectKey segmentObjectKey = new AbstractObjectKey(sequence.getLocatorURI() + "_" + seqSegmentNo);
                LocalAbstractObject segmentObjectToStore = segment;

                // Segment feature extraction
                if (originalSegmentFPS != convertedSegmentFPS) {
                    segmentObjectToStore = fpsConvertor.convert(segment.duplicate());
                }
                segmentObjectToStore.setObjectKey(segmentObjectKey);

                // Stores the segment
                segmentObjectToStore.write(fos);

                seqSegmentNo++;
            }
            sequenceCount++;
        }
        fos.close();
        System.out.println("Sequence count: " + sequenceCount + "; segment count: " + segmentCount);
    }
}
